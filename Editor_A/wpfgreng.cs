﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

using System.Xml;//for mtrlxml
using System.IO;//for mtrlxml

using System.Windows.Controls;//for sample class

namespace Editor_A
{

    class wpfgreng
    {  
        public static int djenerator()
        {

            mw.mw_opt.gen_new_opt();

            //amk_rgb* r = material_witrag(PSize, PSize);
            ////cl_mtrl_witrag* mt1 = new cl_mtrl_witrag(mw_opt.iSize,mw_opt.iSize);
            mw.mt1.material_witrag(((mw.mw_opt.bAnimOn) && (mw.mw_opt.idFN > 0)));
            //2 amk_rgb* r = material_crstll_marble_sky(PSize, PSize);

            Bitmap bm = new Bitmap(mw.mw_opt.iSize, mw.mw_opt.iSize, System.Drawing.Imaging.PixelFormat.Format32bppArgb);

            for (int y = 0; y < mw.mw_opt.iSize; y += 1)
                for (int x = 0; x < mw.mw_opt.iSize; x += 1)
                {

                    bm.SetPixel(y, mw.mw_opt.iSize - x -1, 
                        Color.FromArgb(
                        (char)Math.Min(255, 255),

                        /////////////////////////////////////////////
                        (char)Math.Min(255,
                        Math.Abs(1.0D+(int)(255 * mw.mt1.arr[3 * (x * (mw.mw_opt.iSize) + y) + 0]))
                        ),

                        /////////////////////////////////////////////
                        (char)Math.Min(255,
                        Math.Abs(1.0D + (int)(255 * mw.mt1.arr[3 * (x * (mw.mw_opt.iSize) + y) + 1]))
                        ),

                        /////////////////////////////////////////////
                        (char)Math.Min(255,
                        Math.Abs(1.0D + (int)(255 * mw.mt1.arr[3 * (x * (mw.mw_opt.iSize) + y) + 2]))
                        )
                        
                        )
                        );


                }

            mw.mw_opt.generateFN();            
            bm.Save(mw.mw_opt.wFN);
            return 0;
        }

    }

    public static class mtrlxml
    {
        public static Guid GenerateMaxTextureBitmap(XmlTextWriter w)
        {
            w.WriteStartElement("TextureMap");
            w.WriteAttributeString("name", "Map #1");
            Guid map_id = Guid.NewGuid();
            w.WriteAttributeString("id", "{"+map_id.ToString()+"}");

            w.WriteStartElement("DisplayMapInViewport");
            w.WriteValue(0);
            w.WriteEndElement();// </DisplayMapInViewport>

            w.WriteStartElement("BitmapMap");
            w.WriteStartElement("TexMapCoords2d");
            w.WriteStartElement("TextureCoords");
            w.WriteStartElement("ExplicitMapChannel");
            w.WriteAttributeString("CoordPlane","uv");

            w.WriteStartElement("MapChannel");
            w.WriteValue(1);
            w.WriteEndElement();// </MapChannel>

            w.WriteStartElement("UCoords");

            w.WriteStartElement("OffsetCoord");
            w.WriteValue(0);
            w.WriteEndElement();//</OffsetCoord>

            w.WriteStartElement("TilingCoord");
            w.WriteValue(0.020833334);
            w.WriteEndElement();//</TilingCoord>


            w.WriteStartElement("ScaleCoord");
            w.WriteValue(0);
            w.WriteEndElement();//</ScaleCoord>

            w.WriteStartElement("TextureTiling");
            w.WriteAttributeString("Tiling","tile");

            w.WriteEndElement();//</TextureTiling>
            w.WriteEndElement();//</UCoords>


            w.WriteStartElement("VCoords");

            w.WriteStartElement("OffsetCoord");
            w.WriteValue(0);
            w.WriteEndElement();//</OffsetCoord>


            w.WriteStartElement("TilingCoord");
            w.WriteValue(0.020833334);
            w.WriteEndElement();//</TilingCoord>


            w.WriteStartElement("ScaleCoord");
            w.WriteValue(0);
            w.WriteEndElement();//</ScaleCoord>


            w.WriteStartElement("TextureTiling");
            w.WriteAttributeString("Tiling","tile");
            w.WriteEndElement();//</TextureTiling>

            w.WriteEndElement();//</VCoords>

            w.WriteStartElement("Angles");

            
            w.WriteStartElement("UAngle");
            w.WriteValue(0);
            w.WriteEndElement();//<UAngle>

            
            w.WriteStartElement("VAngle");
            w.WriteValue(0);
            w.WriteEndElement();//<VAngle>

            
            w.WriteStartElement("WAngle");
            w.WriteValue(0);
            w.WriteEndElement();//<WAngle>

            w.WriteEndElement();//</Angles>

            
            w.WriteStartElement("Blur");
            w.WriteValue(1);
            w.WriteEndElement();//<Blur>

            
            w.WriteStartElement("BlurOffset");
            w.WriteValue(0);
            w.WriteEndElement();//<BlurOffset>

            
            w.WriteStartElement("UseRealWorldScale");
            w.WriteValue(1);
            w.WriteEndElement();//<UseRealWorldScale>



            w.WriteEndElement();//</ExplicitMapChannel>
            w.WriteEndElement();//</TextureCoords>

            w.WriteEndElement();//</TexMapCoords2d>

            w.WriteStartElement("TexMapNoise");
            w.WriteAttributeString("AnimateOn","0");
            w.WriteAttributeString("on","0");

            w.WriteStartElement("NoiseAmount");
            w.WriteValue(1);
            w.WriteEndElement();//<NoiseAmount>

            w.WriteStartElement("NoiseLevels");
            w.WriteValue(1);
            w.WriteEndElement();//<NoiseLevels>

            w.WriteStartElement("NoiseSize");
            w.WriteValue(1);
            w.WriteEndElement();//<NoiseSize>

            w.WriteStartElement("NoisePhase");
            w.WriteValue(0);
            w.WriteEndElement();//<NoisePhase>											

            w.WriteEndElement();//</TexMapNoise>

            w.WriteStartElement("BitmapParameters");

            w.WriteStartElement("BitmapFile");
            w.WriteAttributeString("filename","src256.bmp");
            w.WriteEndElement();//</BitmapFile>

            w.WriteStartElement("Cropping");
            w.WriteAttributeString("CropType","crop");
            w.WriteAttributeString("on","0");


            w.WriteStartElement("UCrop");
            w.WriteValue(0);
            w.WriteEndElement();//<UCrop>


            w.WriteStartElement("VCrop");
            w.WriteValue(0);
            w.WriteEndElement();//<VCrop>


            w.WriteStartElement("WCrop");
            w.WriteValue(1);
            w.WriteEndElement();//<WCrop>


            w.WriteStartElement("HCrop");
            w.WriteValue(1);
            w.WriteEndElement();//<HCrop>

            w.WriteStartElement("JitterPlacement");
            w.WriteAttributeString("on","0");
            w.WriteValue(1);
            w.WriteEndElement();//</JitterPlacement>
            w.WriteEndElement();//</Cropping>

			w.WriteStartElement("Filtering");
            
            w.WriteAttributeString("Type","pyramidal");
            w.WriteEndElement();//</Filtering>

            w.WriteStartElement("MonoChannelOutput");
            w.WriteAttributeString("Output","rgbIntensity");
            w.WriteEndElement();//</MonoChannelOutput>

            w.WriteStartElement("RGBChannelOutput");
            w.WriteAttributeString("Output","alphaAsGray");
            w.WriteEndElement();//</RGBChannelOutput>

            w.WriteStartElement("AlphaSource");
            w.WriteAttributeString("Source","none");
            w.WriteEndElement();//</AlphaSource>


            w.WriteStartElement("PremultipliedAlpha");
            w.WriteValue(1);
            w.WriteEndElement();//<PremultipliedAlpha>


            w.WriteStartElement("FitToObject");
            w.WriteValue(false);
            w.WriteEndElement();//<FitToObject>

            w.WriteEndElement();//</BitmapParameters>

           w.WriteStartElement("TexMapOutput");

            w.WriteStartElement("Invert");
            w.WriteValue(0);
            w.WriteEndElement();//<Invert>

            w.WriteStartElement("Clamp");
            w.WriteValue(0);
            w.WriteEndElement();//<Clamp>

            w.WriteStartElement("AlphaFromRGBIntensity");
            w.WriteValue(0);
            w.WriteEndElement();//<AlphaFromRGBIntensity>

            w.WriteStartElement("EnableColorMap");
            w.WriteValue(0);
            w.WriteEndElement();//<EnableColorMap>

            w.WriteStartElement("OutputAmount");
            w.WriteValue(1);
            w.WriteEndElement();//<OutputAmount>

            w.WriteStartElement("RGBOffset");
            w.WriteValue(0);
            w.WriteEndElement();//<RGBOffset>


            w.WriteStartElement("RGBLevel");
            w.WriteValue(1);
            w.WriteEndElement();//<RGBLevel>


            w.WriteStartElement("BumpAmount");
            w.WriteValue(1);
            w.WriteEndElement();//<BumpAmount>





            w.WriteEndElement();//</TexMapOutput>
            w.WriteEndElement();//</BitmapMap>
            w.WriteEndElement();//</TextureMap>
            return map_id;
        }

        public static int GenerateMaxMtrl()
        {

            StreamWriter writer = File.CreateText("material.xml");
            XmlTextWriter w = new XmlTextWriter(writer);
            w.Formatting = Formatting.Indented;
            w.WriteComment("xml version=\"1.0\"");



            w.WriteStartElement("Scene");
            w.WriteAttributeString("FileName", "55.max");
            Guid scene = Guid.NewGuid();
            w.WriteAttributeString("id", "{"+scene.ToString()+"}");

            w.WriteStartElement("ObjectModifiers");
            w.WriteStartElement("Materials");
            w.WriteStartElement("Material");
            Guid material = Guid.NewGuid();
            w.WriteAttributeString("id", "{" + material.ToString() + "}");
            w.WriteAttributeString("app", "Alchemy");
            w.WriteAttributeString("thumbnail", ".\\3722c2.png");
            w.WriteAttributeString("Background","0");
            w.WriteAttributeString("Backlight","1");
            w.WriteAttributeString("Sample","0");
            w.WriteAttributeString("Tiling","0");
            w.WriteAttributeString("VideoCheck", "0");





            w.WriteStartElement("LinearUnits");
            w.WriteAttributeString("unitType","undef");
            w.WriteAttributeString("unitScale","1");
            w.WriteEndElement();


            
            w.WriteStartElement("StandardMtl");
            w.WriteAttributeString("name","mkub");

            w.WriteStartElement("Wire");
            w.WriteValue(0);
            w.WriteEndElement();

            w.WriteStartElement("TwoSided");
            w.WriteValue(0);
            w.WriteEndElement();

            w.WriteStartElement("FaceMap");
            w.WriteValue(0);
            w.WriteEndElement();


            w.WriteStartElement("Faceted");
            w.WriteValue(0);
            w.WriteEndElement();


            w.WriteStartElement("DisplayMapInViewport");
            w.WriteValue(0);
            w.WriteEndElement();


            w.WriteStartElement("Phong");
            w.WriteStartElement("PhongBasicParameters");
            w.WriteStartElement("AmbientColor");

			w.WriteStartElement("RGBf");
            w.WriteAttributeString("r","1");
            w.WriteAttributeString("g","0");
            w.WriteAttributeString("b","0");

            w.WriteStartElement("ByObject");
            w.WriteValue(false);
            w.WriteEndElement();

			w.WriteEndElement();//</RGBf>
			w.WriteEndElement();//</AmbientColor>
            
            w.WriteStartElement("DiffuseColor");

			w.WriteStartElement("RGBf");
            w.WriteAttributeString("r","1");
            w.WriteAttributeString("g","0");
            w.WriteAttributeString("b","0");
            
            w.WriteStartElement("ByObject");
            w.WriteValue(false);
            w.WriteEndElement();

			w.WriteEndElement();//</RGBf>

			w.WriteEndElement();//</DiffuseColor>



            w.WriteStartElement("SpecularColor");

			w.WriteStartElement("RGBf");
            w.WriteAttributeString("r","0.8980393");
            w.WriteAttributeString("g","0.8980393");
            w.WriteAttributeString("b","0.8980393");
            
            w.WriteStartElement("ByObject");
            w.WriteValue(false);
            w.WriteEndElement();

			w.WriteEndElement();//</RGBf>
			w.WriteEndElement();//</SpecularColor>


            w.WriteStartElement("SelfIllumination");

            w.WriteStartElement("SelfIllumValue");
            w.WriteValue(0);
            w.WriteEndElement();

			w.WriteEndElement();//</SelfIllumination>

            w.WriteStartElement("Opacity");
            w.WriteValue(100);
            w.WriteEndElement();


            w.WriteStartElement("SpecularLevel");
            w.WriteValue(0);
            w.WriteEndElement();

            w.WriteStartElement("Glossiness");
            w.WriteValue(0);
            w.WriteEndElement();


            w.WriteStartElement("Soften");
            w.WriteValue(0.1);
            w.WriteEndElement();



			w.WriteEndElement();//</PhongBasicParameters>

			w.WriteStartElement("ExtendedParameters");
			w.WriteStartElement("AdvancedTransparency");
            w.WriteAttributeString("FalloffType","in");
            w.WriteAttributeString("TransparencyType","filter");

            
            w.WriteStartElement("FalloffAmount");
            w.WriteValue(0);
            w.WriteEndElement();

            w.WriteStartElement("FilterColor");
			w.WriteStartElement("RGBf");
            w.WriteAttributeString("r","0.58431375");
            w.WriteAttributeString("g","0.58431375");
            w.WriteAttributeString("b","0.58431375");
            
            w.WriteStartElement("ByObject");
            w.WriteValue(false);
            w.WriteEndElement();

			w.WriteEndElement();//</RGBf>
            w.WriteEndElement();//</FilterColor>

            w.WriteStartElement("IndexOfRefraction");
            w.WriteValue(1.5);
            w.WriteEndElement();

			w.WriteEndElement();//</AdvancedTransparency>

            w.WriteStartElement("WireSize");
            w.WriteAttributeString("WireSizeUnits", "pixels");
            w.WriteValue(1);
			w.WriteEndElement();//</WireSize>
            
			w.WriteStartElement("ReflectionDimming");
            w.WriteAttributeString("on","0");

            w.WriteStartElement("DimmingLevel");
            w.WriteValue(0);
            w.WriteEndElement();//</DimmingLevel>

            w.WriteStartElement("ReflectionLevel");
            w.WriteValue(3);
            w.WriteEndElement();//</ReflectionLevel>

			w.WriteEndElement();//</ReflectionDimming>

            w.WriteStartElement("Translucency");
            w.WriteValue(0);
            w.WriteEndElement();//</Translucency>

            w.WriteEndElement();//</ExtendedParameters>

            w.WriteStartElement("SuperSampling");
            w.WriteAttributeString("on","1");

            w.WriteStartElement("AdaptiveHalton");

            w.WriteStartElement("SuperSampleTexture");
            w.WriteValue(1);
            w.WriteEndElement();//<SuperSampleTexture>

            w.WriteStartElement("SuperSampleQuality");
            w.WriteValue(0.5);
            w.WriteEndElement();//<SuperSampleQuality>

            w.WriteStartElement("SuperSampleAdaptiveThresh");
            w.WriteAttributeString("on","1");
            w.WriteValue(0.1);
            w.WriteEndElement();//<SuperSampleAdaptiveThresh on="1">

            w.WriteEndElement();//</AdaptiveHalton>
            w.WriteEndElement();//</SuperSampling>

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            w.WriteStartElement("MapSet");
			w.WriteStartElement("MapLib");
            
            Guid g1 = GenerateMaxTextureBitmap(w);
            Guid g2 = GenerateMaxTextureBitmap(w);

            w.WriteEndElement();//<MapLib/>


//<Map TexMapChannelNum="1" TexMapChannel="Diffuse Color" Amount="100" TexMapRef="{A8A4E52C-B097-43A9-B7F2-9A1005E4B2A7}" on="1"/>
            w.WriteStartElement("Map");
            w.WriteAttributeString("TexMapChannelNum","1");
            w.WriteAttributeString("TexMapChannel", "Diffuse Color");
            w.WriteAttributeString("Amount", "100");
            w.WriteAttributeString("TexMapRef","{"+g1.ToString()+"}");
            w.WriteAttributeString("on", "1");
            w.WriteEndElement();//<Map/>

//<Map TexMapChannelNum="2" TexMapChannel="Specular Color" Amount="100" TexMapRef="{67F582C6-DEBE-490B-8185-6EA24D954646}" on="1"/>
            w.WriteStartElement("Map");
            w.WriteAttributeString("TexMapChannelNum", "2");
            w.WriteAttributeString("TexMapChannel", "Specular Color");
            w.WriteAttributeString("Amount", "100");
            w.WriteAttributeString("TexMapRef", "{" + g2.ToString() + "}");
            w.WriteAttributeString("on", "1");
            w.WriteEndElement();//<Map/>

            /*
							<Map TexMapChannelNum="2" TexMapChannel="Specular Color" Amount="100" TexMapRef="{67F582C6-DEBE-490B-8185-6EA24D954646}" on="1"/>
							<Map TexMapChannelNum="3" TexMapChannel="Specular Level" Amount="100" TexMapRef="{42C57E71-91CC-43CC-854D-5B7E5DC72FFD}" on="1"/>
							<Map TexMapChannelNum="4" TexMapChannel="Glossiness" Amount="100" TexMapRef="{CE11AD7C-4B4E-4EFB-A102-CF5F05C3E6E6}" on="1"/>
							<Map TexMapChannelNum="5" TexMapChannel="Self-Illumination" Amount="100" TexMapRef="{B8607932-8FCF-4BCC-AA29-1F24B1FAB360}" on="1"/>
            */

            w.WriteEndElement();//</MapSet>
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            w.WriteEndElement();//</Phong>
            w.WriteEndElement();//</StandardMtl>

            w.WriteStartElement("PreviewShape");
            w.WriteValue(0);
            w.WriteEndElement();//<PreviewShape>

            w.WriteStartElement("DiffuseAmbientLock");
            w.WriteValue(false);
            w.WriteEndElement();//</DiffuseAmbientLock>

            w.WriteStartElement("DiffuseSpecularLock");
            w.WriteValue(false);
            w.WriteEndElement();//</DiffuseSpecularLock>


            w.WriteStartElement("DiffuseMapChannelOn");
            w.WriteValue(false);
            w.WriteEndElement();//</DiffuseMapChannelOn>


            w.WriteStartElement("OpacityMapChannelOn");
            w.WriteValue(false);
            w.WriteEndElement();//</OpacityMapChannelOn>


            w.WriteStartElement("ReflectionMapChannelOn");
            w.WriteValue(false);
            w.WriteEndElement();//</ReflectionMapChannelOn>


            w.WriteStartElement("BumpMapChannelOn");
            w.WriteValue(false);
            w.WriteEndElement();//</BumpMapChannelOn>



            w.WriteEndElement();//</Material>
            w.WriteEndElement();//</Materials>
            w.WriteEndElement();//</ObjectModifiers>
            w.WriteEndElement();//</Scene>



            /*
            w.WriteStartElement("y", "item", "urn:1");
            w.WriteAttributeString("attr", "urn:1", "123");
            Double price = 19.95;
            w.WriteElementString("price", XmlConvert.ToString(price));
            w.WriteEndElement();
            w.WriteEndElement();
             */

            w.Close();
            return 0;

            /*
             <?xml version="1.0"?>
<!-- Autodesk VIZ Selective Scene Definition.
     Exported 11.08.2014 15:10:11 by XmlMtlExp. -->
<Scene Filename="55.max" id="{F41DAD38-3A09-4D26-9712-283158D441BD}"><ObjectModifiers><Materials><Material id="{5845F4AA-BFD0-4EBD-9B05-29D94F9AA823}" app="3DSVIZ" thumbnail=".\3722c2.png" Background="0" Backlight="1" Sample="0" Tiling="0" VideoCheck="0">
				<LinearUnits unitType="undef" unitScale="1"/>
				<StandardMtl name="mkub">
					<Wire>0</Wire>
					<TwoSided>0</TwoSided>
					<FaceMap>0</FaceMap>
					<Faceted>0</Faceted>
					<DisplayMapInViewport>0</DisplayMapInViewport>
					<Phong>
						<PhongBasicParameters>
							<AmbientColor>
								<RGBf r="1" g="0" b="0">
									<ByObject>false</ByObject>
								</RGBf>
							</AmbientColor>
							<DiffuseColor>
								<RGBf r="1" g="0" b="0">
									<ByObject>false</ByObject>
								</RGBf>
							</DiffuseColor>
							<SpecularColor>
								<RGBf r="0.8980393" g="0.8980393" b="0.8980393">
									<ByObject>false</ByObject>
								</RGBf>
							</SpecularColor>
							<SelfIllumination>
								<SelfIllumValue>0</SelfIllumValue>
							</SelfIllumination>
							<Opacity>100</Opacity>
							<SpecularLevel>0</SpecularLevel>
							<Glossiness>10</Glossiness>
							<Soften>0.1</Soften>
						</PhongBasicParameters>
						<ExtendedParameters>
							<AdvancedTransparency FalloffType="in" TransparencyType="filter">
								<FalloffAmount>0</FalloffAmount>
								<FilterColor>
									<RGBf r="0.58431375" g="0.58431375" b="0.58431375">
										<ByObject>false</ByObject>
									</RGBf>
								</FilterColor>
								<IndexOfRefraction>1.5</IndexOfRefraction>
							</AdvancedTransparency>
							<WireSize WireSizeUnits="pixels">1</WireSize>
							<ReflectionDimming on="0">
								<DimmingLevel>0</DimmingLevel>
								<ReflectionLevel>3</ReflectionLevel>
							</ReflectionDimming>
							<Translucency>0</Translucency>
						</ExtendedParameters>
						<SuperSampling on="1">
							<AdaptiveHalton>
								<SuperSampleTexture>1</SuperSampleTexture>
								<SuperSampleQuality>0.5</SuperSampleQuality>
								<SuperSampleAdaptiveThresh on="1">0.1</SuperSampleAdaptiveThresh>
							</AdaptiveHalton>
						</SuperSampling>
						<MapSet>
							<MapLib/>
						</MapSet>
					</Phong>
				</StandardMtl>
				<PreviewShape>0</PreviewShape>
				<DiffuseAmbientLock>false</DiffuseAmbientLock>
				<DiffuseSpecularLock>false</DiffuseSpecularLock>
				<DiffuseMapChannelOn>false</DiffuseMapChannelOn>
				<OpacityMapChannelOn>false</OpacityMapChannelOn>
				<ReflectionMapChannelOn>false</ReflectionMapChannelOn>
				<BumpMapChannelOn>false</BumpMapChannelOn>
			</Material></Materials></ObjectModifiers></Scene>
             */
        }
            
    }

    public class Sample
    {
        private const string filename = "material.xml";

        public static bool LoadMyXml(XmlNode x, TreeViewItem trv)
        {
            TreeViewItem temp;
            if (x.Attributes != null)
            for (int i = 0; i < x.Attributes.Count; i++)
            {
                temp = new TreeViewItem();
                temp.Header = x.Attributes[i].Name;
                if (x.Attributes[i].Value != null)
                    temp.Header += " | " + x.Attributes[i].Value.ToString();
                trv.Items.Add(temp);
            }

            if (x.HasChildNodes) 
                for (int i = 0; i < x.ChildNodes.Count; i++)            
                {
                    temp = new TreeViewItem();
                    temp.Header = x.ChildNodes[i].Name;//.Value.ToString();
                    LoadMyXml(x.ChildNodes[i], temp);
                    trv.Items.Add(temp);
                }

                
            return true;
        }

        public static void SMain(TextBlock tb, TreeView trv)
        {

            //XmlTextWriter writer = new XmlTextWriter(filename, null);


            //Lookup the prefix and then write the ISBN attribute. 
           // string prefix = writer.LookupPrefix("urn:samples");
           /// writer.WriteString("1-861003-78");


            //Read the file back in and parse to ensure well formed XML.
            XmlDocument doc = new XmlDocument();
            //Preserve white space for readability.
            doc.PreserveWhitespace = true;
            //Load the file
            doc.Load(filename);

            if (doc.Attributes!=null)
            for (int i = 0; i < doc.Attributes.Count; i++)
            {
                TreeViewItem temp = new TreeViewItem();
                temp.Header = doc.Attributes[i].Name;
                if (doc.Attributes[i].Value!=null)
                temp.Header +=" | " + doc.Attributes[i].Value.ToString();
                trv.Items.Add(temp);
            }

            if (doc.HasChildNodes)
                for (int i = 0; i < doc.ChildNodes.Count; i++)            
                {

                    TreeViewItem temp = new TreeViewItem();
                    temp.Header = doc.ChildNodes[i].Name;//.Value.ToString();
                    LoadMyXml(doc.ChildNodes[i], temp);
                    trv.Items.Add(temp);
                }

            
            
            //App.Current.Shutdown(-1);
           // tb.Text = doc.InnerXml;
            //Write the XML content to the console.
           // Console.Write(doc.InnerXml);

        }

    }

}
