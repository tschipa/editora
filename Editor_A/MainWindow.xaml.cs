﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Diagnostics;


//#using <mscorlib.dll>
using System.Runtime.InteropServices; 


    
namespace Editor_A
{

 
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {


        [DllImport("greng.dll", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.Cdecl)]        
        public static extern int djenerator(int x);

        [DllImport("greng.dll", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.Cdecl)]        
        public static extern void mw_opt_initialize(int x);

        [DllImport("greng.dll", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.Cdecl)]        
        public static extern int wfpamkparam(int w_render_tech, int w_sizex, int w_sizey, int w_sec, String w_OutAviFileName);


        [DllImport("greng.dll", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.Cdecl)]        
        public static extern int wfpamkparamchannel(
    double w_invMainCh0,
    double w_negMainCh0,
    double w_shftMainCh0,
    double w_pwrMainCh0,
    double w_deltaTxtr0Size,

    double w_invMainCh1,
    double w_negMainCh1,
    double w_shftMainCh1,
    double w_pwrMainCh1,
    double w_deltaTxtr1Size,

    double w_invMainCh2,
    double w_negMainCh2,
    double w_shftMainCh2,
    double w_pwrMainCh2,
    double w_deltaTxtr2Size);



        [DllImport("greng.dll", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.Cdecl)]        
        public static extern int wfpamkparamblur(
//Blur
int w_itrBlur,     //number of blur iterations of amountBlur pixels each
int w_amountBlur,  //amount of pixels in blur
int w_beforeBlur,  //amount of pixels in blur before iterated blur
//Blur0
int w_itrBlur0,     //number of blur iterations of amountBlur pixels each
int w_amountBlur0,  //amount of pixels in blur
int w_beforeBlur0,  //amount of pixels in blur before iterated blur
//Blur1
int w_itrBlur1,     //number of blur iterations of amountBlur pixels each
int w_amountBlur1,  //amount of pixels in blur
int w_beforeBlur1,  //amount of pixels in blur before iterated blur
//Blur2
int w_itrBlur2,     //number of blur iterations of amountBlur pixels each
int w_amountBlur2,  //amount of pixels in blur
int w_beforeBlur2  //amount of pixels in blur before iterated blur
                                                   );


        [DllImport("greng.dll", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.Cdecl)]        
        public static extern int  wfpamkparammtrxmainch(
	//e1
	int el000,
	int el001,

	int el010,
	int el011,

	int el020,
	int el021,
	//e2
	int el100,
	int el101,

	int el110,
	int el111,

	int el120,
	int el121,
	//e3
	int el200,
	int el201,

	int el210,
	int el211,

	int el220,
	int el221
	);

        // Use DllImport to import the Win32 MessageBox function.
        [DllImport("user32.dll", CharSet = CharSet.Unicode)]        
        public static extern int MessageBox(IntPtr hWnd, String text, String caption, uint type);



        public MainWindow()
        {
            InitializeComponent();
           



            
            GroupBox gbx22 = wpfamkparam.CrtMgcCrstlls();
            DockPanel.SetDock(gbx22, Dock.Top);
            DckPnl_CmmndPnl.Children.Add(gbx22);//[0]

            Expander xpx23 = wpfamkparam.MnTxtrChnnls();
            DockPanel.SetDock(xpx23, Dock.Top);
            DckPnl_CmmndPnl.Children.Add(xpx23);//[1]

            Expander xpx24 = wpfamkparam.CrtBlurs();
            DockPanel.SetDock(xpx24, Dock.Top);
            DckPnl_CmmndPnl.Children.Add(xpx24);//[2]

            /*
            int[, , ,] IntParses = new int[3, 4, 2, 3];

                        // x, y, z
            int[] dims = {3, 4, 2, 3};
            //dims[x]*dims[y]*dims[z] = 18
            string[] defs = { "0", "1", "2",
                              "3", "4", "5",
                              "6", "7", "8",
                              "9", "10","11",

                              "12", "13", "14",
                              "15", "16", "17",
                              "18", "19", "20",
                              "21", "22", "23",

                              "100", "101", "102",
                              "103", "104", "105",
                              "106", "107", "108",
                              "109", "110", "111",

                              "112", "113", "114",
                              "115", "116", "117",
                              "118", "119", "120",
                              "121", "122", "123",

                              "200", "201", "202",
                              "203", "204", "205",
                              "206", "207", "208",
                              "209", "210", "211",

                              "212", "213", "214",
                              "215", "216", "217",
                              "218", "219", "220",
                              "221", "222", "223"

                            };

            string[] heads = { 

                                 "4D header",
                                 "4D subheader x",
                                 "4D subheader y",


                                 "0head overall",
                                 "1head x",
                                 "2head y",
                                 "3Red | Green | Blue |  for result coordinate X",
                                 "4Red | Green | Blue |  for result coordinate Y",
                                 "5Red | Green | Blue |  for result coordinate Z",
                                 "55Red | Green | Blue |  for result coordinate -Ct",
                                 "6 e1",
                                 "7 e2",
                                 "8 e3",
                                 "88e3",
                                 "09 txtr #1", "10 txtr #2",
                             
                                 "10head overall",
                                 "11head x",
                                 "12head y",
                                 "13Red | Green | Blue |  for result coordinate X",
                                 "14Red | Green | Blue |  for result coordinate Y",
                                 "15Red | Green | Blue |  for result coordinate Z",
                                 "155Red | Green | Blue |  for result coordinate -Ct",
                                 "16 e1",
                                 "17 e2",
                                 "18 e3",
                                 "188e3",
                                 "109 txtr #1", "110 txtr #2",

                             
                                 "210head overall",
                                 "211head x",
                                 "212head y",
                                 "213Red | Green | Blue |  for result coordinate X",
                                 "214Red | Green | Blue |  for result coordinate Y",
                                 "215Red | Green | Blue |  for result coordinate Z",
                                 "2155Red | Green | Blue |  for result coordinate -Ct",
                                 "216 e1",
                                 "217 e2",
                                 "218 e3",
                                 "2188e3",
                                 "2109 txtr #1", "2110 txtr #2"                                                          
                             
                             };

            Expander xpx26 = wpfamkparam.MtrxEditor(4, ref dims, ref defs, ref heads, 0);
            DockPanel.SetDock(xpx26, Dock.Top);
            DckPnl_CmmndPnl.Children.Add(xpx26);
            int[] IP = null;
            int k5=0;
            int k6=0;
            tblTemp.Text =  wpfamkparam.ParseMtrx(xpx26, 4, ref dims, out IP);

            tblTemp.Text = "\n\n";
                for (int k0=0; k0<dims[0]; k0++)
                for (int k1=0; k1<dims[1]; k1++)
                for (int k2=0; k2<dims[2]; k2++)
                for (int k3=0; k3<dims[3]; k3++)
                {
                    k5++; k6++;
                //tblTemp.Text += IP[k1 * dims[2] * dims[0] + k2 * dims[0] + k0 + k3 * dims[0] * dims[1] * dims[2]].ToString() + " | ";
                tblTemp.Text += IP[k5-1].ToString() + " | ";
                if (k6 > 2) { tblTemp.Text += "  |||  ";  }
                if (k6 > 14) { tblTemp.Text += "\n"; k6 = 0; }
                };

            */

                        // x, y, z
            int[] dims1 = { 3, 3, 2};
            //dims[x]*dims[y]*dims[z] = 18
            //[2][0] [1][1] [2][2]
            //[2][2] [0][1] [2][2]
            //[2][2] [1][1] [0][2]

            string[] defs1 = { wpfamkparam.mtrxMainCh[0,0,0].ToString(), wpfamkparam.mtrxMainCh[0,1,0].ToString(), wpfamkparam.mtrxMainCh[0,2,0].ToString(), //txtr1
                               wpfamkparam.mtrxMainCh[0,0,1].ToString(), wpfamkparam.mtrxMainCh[0,1,1].ToString(), wpfamkparam.mtrxMainCh[0,2,1].ToString(), //txtr2

                               wpfamkparam.mtrxMainCh[1,0,0].ToString(), wpfamkparam.mtrxMainCh[1,1,0].ToString(), wpfamkparam.mtrxMainCh[1,2,0].ToString(), //txtr1
                               wpfamkparam.mtrxMainCh[1,0,1].ToString(), wpfamkparam.mtrxMainCh[1,1,1].ToString(), wpfamkparam.mtrxMainCh[1,2,1].ToString(), //txtr2

                               wpfamkparam.mtrxMainCh[2,0,0].ToString(), wpfamkparam.mtrxMainCh[2,1,0].ToString(), wpfamkparam.mtrxMainCh[2,2,0].ToString(), //txtr1
                               wpfamkparam.mtrxMainCh[2,0,1].ToString(), wpfamkparam.mtrxMainCh[2,1,1].ToString(), wpfamkparam.mtrxMainCh[2,2,1].ToString(), //txtr2

                             };

            string[] heads1 = { 
                                 "0Textures Convolution Tensor Matrix",
                                 "1Red Green Blue Chnlls",
                                 "2Output Vector Components e1, e2, e3",
                                 "3Red | Green | Blue |  for result coordinate X",
                                 "4Red | Green | Blue |  for result coordinate Y",
                                 "5Red | Green | Blue |  for result coordinate Z",
                                 "6 e1",
                                 "7 e2",
                                 "8 e3",
                                 "09texture #1", "10texture #2"
                              };


            Expander xpx27 = wpfamkparam.MtrxEditor(3, ref dims1, ref defs1, ref heads1, 0);
            DockPanel.SetDock(xpx27, Dock.Top);
            DckPnl_CmmndPnl.Children.Add(xpx27);//[3]

            /*
            int[] IP = null;
            int k5 = 0;
            int k6 = 0;
            tblTemp.Text = wpfamkparam.ParseMtrx(DckPnl_CmmndPnl.Children[4] as Expander/*xpx27*, 3, ref dims1, out IP);
            
            tblTemp.Text = "\n\n";
            for (int k0 = 0; k0 < dims1[0]; k0++)
                for (int k1 = 0; k1 < dims1[1]; k1++)
                    for (int k2 = 0; k2 < dims1[2]; k2++)
                        {
                            k5++; k6++;
                            //tblTemp.Text += IP[k1 * dims[2] * dims[0] + k2 * dims[0] + k0 + k3 * dims[0] * dims[1] * dims[2]].ToString() + " | ";
                            tblTemp.Text += IP[k5 - 1].ToString() + " | ";
                            if (k6 > 2) { tblTemp.Text += "  |||  "; }
                            if (k6 > 14) { tblTemp.Text += "\n"; k6 = 0; }
                        };
             */
            
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {

            
            

                 // 
            // Create the Geometry to draw. 
            //
            GeometryGroup ellipses = new GeometryGroup();
            ellipses.Children.Add(
                new EllipseGeometry(new Point(50,50), 45, 20)
                );
            ellipses.Children.Add(
                new EllipseGeometry(new Point(50, 50), 20, 45)
                );

            // 
            // Create a GeometryDrawing. 
            //
            GeometryDrawing aGeometryDrawing = new GeometryDrawing();
            aGeometryDrawing.Geometry = ellipses;

            // Paint the drawing with a gradient.
            aGeometryDrawing.Brush = 
                new LinearGradientBrush(
                    Colors.Blue, 
                    Color.FromRgb(204,204,255), 
                    new Point(0,0), 
                    new Point(1,1));

            // Outline the drawing with a solid color.
            aGeometryDrawing.Pen = new Pen(Brushes.Black, 10);

            // 
            // Use a DrawingImage and an Image control 
            // to display the drawing. 
            //
            DrawingImage geometryImage = new DrawingImage(aGeometryDrawing);

            // Freeze the DrawingImage for performance benefits.
            geometryImage.Freeze();

            int j = 12;
           
            
            // Call the MessageBox function using platform invoke.
            MessageBox(new IntPtr(0), "Hello World!", "Hello Dialog", 0);

        //   j = fngreng(j);

            mw_opt_initialize(0);
            mw.mw_opt.initialize();//neucode_1

            //wfpamkparam(int w_render_tech, int w_sizex, int w_sizey, int w_sec, String w_OutAviFileName);

           // ListBoxItem li = ((sender as ListBox).SelectedItem as ListBoxItem);
           // Double sz1 = Double.Parse(li.Content.ToString());

            /*
            StackPanel temp = ((DckPnl_CmmndPnl.Children[1] as GroupBox).Content as StackPanel).Children[0] as StackPanel;
            int tech_id = (temp.Children[1] as ComboBox).SelectedIndex;

                 temp = ((DckPnl_CmmndPnl.Children[1] as GroupBox).Content as StackPanel).Children[1] as StackPanel;
            int sx = int.Parse((temp.Children[1] as TextBox).Text);

                       temp = ((DckPnl_CmmndPnl.Children[1] as GroupBox).Content as StackPanel).Children[2] as StackPanel;
            int sy = int.Parse((temp.Children[1] as TextBox).Text);

                       temp = ((DckPnl_CmmndPnl.Children[1] as GroupBox).Content as StackPanel).Children[3] as StackPanel;
            int sec = int.Parse((temp.Children[1] as TextBox).Text);

                       temp = ((DckPnl_CmmndPnl.Children[1] as GroupBox).Content as StackPanel).Children[4] as StackPanel;
                       string nam = (temp.Children[1] as TextBox).Text;
            */

            wpfamkparam.ParseMgcCrstlls((DckPnl_CmmndPnl.Children[1] as GroupBox));
            wpfamkparam.ParseMnTxtrChnnls((DckPnl_CmmndPnl.Children[2] as Expander));
            wpfamkparam.ParseBlurs((DckPnl_CmmndPnl.Children[3] as Expander));

            wfpamkparam(wpfamkparam.tech_id, wpfamkparam.sx, wpfamkparam.sy, wpfamkparam.sec, wpfamkparam.nam);
            mw.mw_opt.mtrl1MatTecniq = (mtrl_1_tech)wpfamkparam.tech_id;
            mw.mw_opt.iSize = Math.Min(wpfamkparam.sx, wpfamkparam.sy);
            

            wfpamkparamchannel(
                wpfamkparam.Ch0.invMainCh,
                wpfamkparam.Ch0.negMainCh,
                wpfamkparam.Ch0.shftMainCh,
                wpfamkparam.Ch0.pwrMainCh,
                wpfamkparam.Ch0.deltaTxtrSize,

                wpfamkparam.Ch1.invMainCh,
                wpfamkparam.Ch1.negMainCh,
                wpfamkparam.Ch1.shftMainCh,
                wpfamkparam.Ch1.pwrMainCh,
                wpfamkparam.Ch1.deltaTxtrSize,

                wpfamkparam.Ch2.invMainCh,
                wpfamkparam.Ch2.negMainCh,
                wpfamkparam.Ch2.shftMainCh,
                wpfamkparam.Ch2.pwrMainCh,
                wpfamkparam.Ch2.deltaTxtrSize);

            mw.mw_opt.invMainCh0 =      wpfamkparam.Ch0.invMainCh;
            mw.mw_opt.negMainCh0 =      wpfamkparam.Ch0.negMainCh;
            mw.mw_opt.shftMainCh0 =     wpfamkparam.Ch0.shftMainCh;
            mw.mw_opt.pwrMainCh0 =      wpfamkparam.Ch0.pwrMainCh;
            mw.mw_opt.deltaTxtrSize =    wpfamkparam.Ch0.deltaTxtrSize;

            mw.mw_opt.invMainCh1 = wpfamkparam.Ch1.invMainCh;
            mw.mw_opt.negMainCh1 = wpfamkparam.Ch1.negMainCh;
            mw.mw_opt.shftMainCh1 = wpfamkparam.Ch1.shftMainCh;
            mw.mw_opt.pwrMainCh1 = wpfamkparam.Ch1.pwrMainCh;
            mw.mw_opt.deltaTxtr1Size = wpfamkparam.Ch1.deltaTxtrSize;


            mw.mw_opt.invMainCh2 = wpfamkparam.Ch2.invMainCh;
            mw.mw_opt.negMainCh2 = wpfamkparam.Ch2.negMainCh;
            mw.mw_opt.shftMainCh2 = wpfamkparam.Ch2.shftMainCh;
            mw.mw_opt.pwrMainCh2 = wpfamkparam.Ch2.pwrMainCh;
            mw.mw_opt.deltaTxtr2Size = wpfamkparam.Ch2.deltaTxtrSize;



       wfpamkparamblur(

//Blur
wpfamkparam.xBlur.itrBlur,     //number of blur iterations of amountBlur pixels each
wpfamkparam.xBlur.amountBlur,  //amount of pixels in blur
wpfamkparam.xBlur.beforeBlur,  //amount of pixels in blur before iterated blur
//Blur0
wpfamkparam.xBlur0.itrBlur,     //number of blur iterations of amountBlur pixels each
wpfamkparam.xBlur0.amountBlur,  //amount of pixels in blur
wpfamkparam.xBlur0.beforeBlur,  //amount of pixels in blur before iterated blur
//Blur1
wpfamkparam.xBlur1.itrBlur,     //number of blur iterations of amountBlur pixels each
wpfamkparam.xBlur1.amountBlur,  //amount of pixels in blur
wpfamkparam.xBlur1.beforeBlur,  //amount of pixels in blur before iterated blur
//Blur2
wpfamkparam.xBlur2.itrBlur,     //number of blur iterations of amountBlur pixels each
wpfamkparam.xBlur2.amountBlur,  //amount of pixels in blur
wpfamkparam.xBlur2.beforeBlur  //amount of pixels in blur before iterated blur
                                                   );
//Blur
mw.mw_opt.itrBlur = wpfamkparam.xBlur.itrBlur;     //number of blur iterations of amountBlur pixels each
mw.mw_opt.amountBlur = wpfamkparam.xBlur.amountBlur;  //amount of pixels in blur
mw.mw_opt.beforeBlur = wpfamkparam.xBlur.beforeBlur;  //amount of pixels in blur before iterated blur
//Blur0
mw.mw_opt.itrBlur0 = wpfamkparam.xBlur0.itrBlur;     //number of blur iterations of amountBlur pixels each
mw.mw_opt.amountBlur0 = wpfamkparam.xBlur0.amountBlur;  //amount of pixels in blur
mw.mw_opt.beforeBlur0 = wpfamkparam.xBlur0.beforeBlur;  //amount of pixels in blur before iterated blur
//Blur1
mw.mw_opt.itrBlur1 = wpfamkparam.xBlur1.itrBlur;     //number of blur iterations of amountBlur pixels each
mw.mw_opt.amountBlur1 = wpfamkparam.xBlur1.amountBlur;  //amount of pixels in blur
mw.mw_opt.beforeBlur1 = wpfamkparam.xBlur1.beforeBlur;  //amount of pixels in blur before iterated blur
//Blur2
mw.mw_opt.itrBlur2 = wpfamkparam.xBlur2.itrBlur;     //number of blur iterations of amountBlur pixels each
mw.mw_opt.amountBlur2 = wpfamkparam.xBlur2.amountBlur;  //amount of pixels in blur
mw.mw_opt.beforeBlur2 = wpfamkparam.xBlur2.beforeBlur;  //amount of pixels in blur before iterated blur



       // x, y, z
       int[] dims1 = { 3, 3, 2 };

       int[] IP1 = null;
//       int k5 = 0;
//       int k6 = 0;
       wpfamkparam.ParseMtrx(DckPnl_CmmndPnl.Children[4] as Expander, 3, ref dims1, out IP1);

/*
       for (int k0 = 0; k0 < dims1[0]; k0++)
           for (int k1 = 0; k1 < dims1[1]; k1++)
               for (int k2 = 0; k2 < dims1[2]; k2++)
               {
                   k5++; k6++;
                   //tblTemp.Text += IP[k1 * dims[2] * dims[0] + k2 * dims[0] + k0 + k3 * dims[0] * dims[1] * dims[2]].ToString() + " | ";
                   tblTemp.Text += IP1[k5 - 1].ToString() + " | ";
                   if (k6 > 2) { tblTemp.Text += "  |||  "; }
                   if (k6 > 14) { tblTemp.Text += "\n"; k6 = 0; }
               };
            */

            wfpamkparammtrxmainch(IP1[0],IP1[3],IP1[6],
                                  IP1[9],IP1[12],IP1[15],

                                  IP1[1],IP1[4],IP1[7],
                                  IP1[10],IP1[13],IP1[16],

                                  IP1[2],IP1[5],IP1[8],
                                  IP1[11],IP1[14],IP1[17]
                                  );

                    //rgb,e1e2e3,txtr1txtr2
       wpfamkparam.mtrxMainCh[0, 0, 0] = IP1[0];
       wpfamkparam.mtrxMainCh[1, 0, 0] = IP1[1];
       wpfamkparam.mtrxMainCh[2, 0, 0] = IP1[2];

       wpfamkparam.mtrxMainCh[0, 0, 1] = IP1[3];
       wpfamkparam.mtrxMainCh[1, 0, 1] = IP1[4];
       wpfamkparam.mtrxMainCh[2, 0, 1] = IP1[5];

       wpfamkparam.mtrxMainCh[0, 1, 0] = IP1[6];
       wpfamkparam.mtrxMainCh[1, 1, 0] = IP1[7];
       wpfamkparam.mtrxMainCh[2, 1, 0] = IP1[8];

       wpfamkparam.mtrxMainCh[0, 1, 1] = IP1[9];
       wpfamkparam.mtrxMainCh[1, 1, 1] = IP1[10];
       wpfamkparam.mtrxMainCh[2, 1, 1] = IP1[11];

       wpfamkparam.mtrxMainCh[0, 2, 0] = IP1[12];
       wpfamkparam.mtrxMainCh[1, 2, 0] = IP1[13];
       wpfamkparam.mtrxMainCh[2, 2, 0] = IP1[14];

       wpfamkparam.mtrxMainCh[0, 2, 1] = IP1[15];
       wpfamkparam.mtrxMainCh[1, 2, 1] = IP1[16];
       wpfamkparam.mtrxMainCh[2, 2, 1] = IP1[17];


       for (int e1 = 0, n=0; e1 < dims1[0]; e1++)
           for (int e3 = 0; e3 < dims1[2]; e3++)
               for (int e2 = 0; e2 < dims1[1]; e2++, n++)
                   mw.mw_opt.mtrxMainCh[e1, e2, e3] = IP1[n];


       mw.mt1 = new cl_mtrl_witrag(mw.mw_opt.iSize, mw.mw_opt.iSize);

       mw.mw_opt.generateIFLFile(wpfamkparam.sec*30, mw.mw_opt.prefixFN, mw.mw_opt.prefixFN);
       for (int i=0; i<wpfamkparam.sec*30; i++) j = wpfgreng.djenerator();



       j = djenerator(j);
            btn_crte.Content = j.ToString();
            Img_src.Source = geometryImage;


            Process.Start("greasle.exe", mw.mw_opt.prefixFN + ".ifl" + " -a mumus.avi");

                /*
            using (Process process = new Process()){
                //generateIFLFile(350, prefixFN, prefixFN);
                //prefixFN+".ifl";


            process.StartInfo.FileName = "greasle.exe";
            process.StartInfo.WorkingDirectory = "";
            process.StartInfo.Arguments = mw.mw_opt.prefixFN+".ifl"+" -a mumus.avi";
            process.Start();
          
            }*/


        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            mtrlxml.GenerateMaxMtrl();
            Sample s = new Sample();
            Sample.SMain(tblckTxt, trvwMaterial);

        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            VedMW mw1 = new VedMW();
            mw1.ShowDialog();
        }

        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            MaW maw1 = new MaW();
            maw1.ShowDialog();
        }


    }



}
