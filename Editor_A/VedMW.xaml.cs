﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Editor_A
{
    /// <summary>
    /// Interaction logic for VedMW.xaml
    /// </summary>
    public partial class VedMW : Window
    {
        public VedMW()
        {
            InitializeComponent();

            GroupBox gbx22 = wpfamkparam.CrtMgcCrstlls();
            DockPanel.SetDock(gbx22, Dock.Top);
            DckPnl_CmmndPnl.Children.Add(gbx22);//[0]
        }

        private void btnRndr_Click(object sender, RoutedEventArgs e)
        {
            VedRndrDlg vrd = new VedRndrDlg();
            vrd.ShowDialog();
        }
    }
}
