﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace Editor_A
{

    struct Blur0012 //Blur,   Blur0,   Blur1,   Blur2
    {
        public int itrBlur;     //number of blur iterations of amountBlur pixels each
        public int amountBlur;  //amount of pixels in blur
        public int beforeBlur;  //amount of pixels in blur before iterated blur
    }


    struct MainCh_deltaTxtrSize
    {

        public double invMainCh;       // inverse or not main channel (1.0f, -1.0f) == f(x) or 1/f(x)
        public double negMainCh;       // negate or not main channel (1.0f ... -1.0f) == f(x) or  -f(x)
        public double shftMainCh;      // shift constant of main channel f(x) + a
        public double pwrMainCh;       // pwr of main channel pow(f(x),pwrMainCh) default value -1.0f/3.0f
        public double deltaTxtrSize;         // >=0

  
    }

    class wpfamkparam
    {
       
/// <summary>
/// первый отдел 
/// - разрешение текстуры размер (Х,У)
/// - имя выходного видеофайла
/// - выбор техники текстурирования (пока две: волшебные кристаллы и витраж)
/// </summary>

        public enum LineEditorInputOptions { leioTextBox, leioComboBox };
        public static StackPanel LineEditor (
            string EditorNamePrefix,
            string LabelText,
            string TxtBoxDefValue,
            LineEditorInputOptions typ = LineEditorInputOptions.leioTextBox,
            double LabelWidth = 120,
            double TxtBoxWidth = 150         )

        {
            StackPanel spLineEditor = new StackPanel();
            spLineEditor.Name = "sp" + EditorNamePrefix;
            spLineEditor.Orientation = Orientation.Horizontal;

            

            Label lLbl = new Label();
            lLbl.Name = "l" + EditorNamePrefix;
            lLbl.Content=LabelText;
            lLbl.Width = LabelWidth;
            spLineEditor.Children.Add(lLbl);

            switch (typ)
            {
                case LineEditorInputOptions.leioTextBox:
            TextBox tbTextBox = new TextBox();
            tbTextBox.Name = "tb" + EditorNamePrefix;
            tbTextBox.Text = TxtBoxDefValue;
            tbTextBox.Margin = new Thickness(4);
            tbTextBox.Width = TxtBoxWidth;            
            spLineEditor.Children.Add(tbTextBox);
                    break;

                case LineEditorInputOptions.leioComboBox:
            ComboBox cbComboBox = new ComboBox();
            cbComboBox.Name = "cb" + EditorNamePrefix;
            cbComboBox.Text = TxtBoxDefValue;
            cbComboBox.Margin = new Thickness(4);
            cbComboBox.Width = TxtBoxWidth;            
            spLineEditor.Children.Add(cbComboBox);
                break;

            }



            spLineEditor.VerticalAlignment = VerticalAlignment.Center;
            spLineEditor.HorizontalAlignment = HorizontalAlignment.Center;
            spLineEditor.Margin = new Thickness(0);
            DockPanel.SetDock(spLineEditor, Dock.Top);


            return spLineEditor;
        }
        public static GroupBox CrtMgcCrstlls()
        {
            GroupBox gbCrtMgcCrstlls = new GroupBox();
            gbCrtMgcCrstlls.Name = "gbCrtMgcCrstlls";
            gbCrtMgcCrstlls.Header = "Create Magic Crystalls";

            StackPanel spCrtMgcCrstlls = new StackPanel();
            spCrtMgcCrstlls.Name = "spCrtMgcCrstlls";
            
            ComboBoxItem cbiStndGlss = new ComboBoxItem();
            cbiStndGlss.Name = "cbiStndGlss";
            cbiStndGlss.Content = "Stained-glass window";



            ComboBoxItem cbiMgcCrstlls = new ComboBoxItem();
            cbiMgcCrstlls.Name = "cbiMgcCrstlls";
            cbiMgcCrstlls.Content = "Magic crystalls";
            cbiMgcCrstlls.IsSelected = true;




            int id = spCrtMgcCrstlls.Children.Count;
            spCrtMgcCrstlls.Children.Add(LineEditor("SlctRndrTchnq", "Render technique", "",LineEditorInputOptions.leioComboBox));
            ((spCrtMgcCrstlls.Children[id] as StackPanel).Children[1] as ComboBox).Items.Add(cbiStndGlss);
            ((spCrtMgcCrstlls.Children[id] as StackPanel).Children[1] as ComboBox).Items.Add(cbiMgcCrstlls);

            spCrtMgcCrstlls.Children.Add(LineEditor("sizeX","Size X=","256"));
            spCrtMgcCrstlls.Children.Add(LineEditor("sizeY", "Size Y=", "256"));
            spCrtMgcCrstlls.Children.Add(LineEditor("dursec", "Duration,seconds=", "5"));
            spCrtMgcCrstlls.Children.Add(LineEditor("savfilnam", "Output .AVI filename", "MyVideo.avi"));

            gbCrtMgcCrstlls.Content = spCrtMgcCrstlls;




            return gbCrtMgcCrstlls;
        }

        public static int tech_id = 0;
        public static int sx = 0;
        public static int sy = 0;
        public static int sec = 0;
        public static string nam = "Video.avi";

        public static bool ParseMgcCrstlls(GroupBox gbX)
        {
            StackPanel temp = (gbX.Content as StackPanel).Children[0] as StackPanel;
            tech_id = (temp.Children[1] as ComboBox).SelectedIndex;

            temp = (gbX.Content as StackPanel).Children[1] as StackPanel;
            sx = int.Parse((temp.Children[1] as TextBox).Text);

            temp = (gbX.Content as StackPanel).Children[2] as StackPanel;
            sy = int.Parse((temp.Children[1] as TextBox).Text);

            temp = (gbX.Content as StackPanel).Children[3] as StackPanel;
            sec = int.Parse((temp.Children[1] as TextBox).Text);

            temp = (gbX.Content as StackPanel).Children[4] as StackPanel;
            nam = (temp.Children[1] as TextBox).Text;
            
            return true;
        }


/// <summary>
/// второй отдел 
/// - зерно рнд-генерации
/// - рнд генератор
/// - константы преобразования для трех главных цветовых каналов RGB
/// остальные - еще не отображенные
/// </summary>

        public static int randSeed = 29711203; ///init value(seed) for random numbers generator
	    //! public bool  brandSeed; // if true get randseed automatically from clock
	    //! public int   iSize;	 // output bitmap screen size 256,512,1024,2048,4096
        //! public string prefixFN;  //prefix for output filename
	    //! public int   idFN;		//incremmentally number for filename (prefixFN+itoa(idFN)+".png"), if<0 no number in filename used, just prefix
	    //! public bool bFN_gen;   //if true generate number for filename incremmentally
	    //! public string wFN[200];

        public static Random r = new Random(randSeed);
        public static MainCh_deltaTxtrSize Ch0, Ch1, Ch2;

        public static Expander MainChannelEditor(string EditorNamePostfix/* "0", "1", "2" */)
        {
            Expander xpMnChnnl = new Expander();
            xpMnChnnl.Name = "xpMnChnnl" + EditorNamePostfix;
            xpMnChnnl.Header = "Edit Main Texture Channel #" + EditorNamePostfix;

            StackPanel spMnChnnl = new StackPanel();
            spMnChnnl.Name = "spMnChnnl" + EditorNamePostfix;

            spMnChnnl.Children.Add(LineEditor("invMainCh"  + EditorNamePostfix, "inverse (1,-1)=f(x) or 1/f(x)", "0,9280"));
            spMnChnnl.Children.Add(LineEditor("negMainCh"  + EditorNamePostfix, "negate  (1,-1)=f(x) or  -f(x)", "0,6050"));
            spMnChnnl.Children.Add(LineEditor("shftMainCh" + EditorNamePostfix, "shift const (0, a) = f(x) + a", "0,080"));
            spMnChnnl.Children.Add(LineEditor("pwrMainCh"  + EditorNamePostfix, "power pow(f(x), pwrValue)", "-0,333333"));
            spMnChnnl.Children.Add(LineEditor("deltaTxtr"+EditorNamePostfix+"Size", "texure anim delta (>=0)", "24,0"));


            xpMnChnnl.Content = spMnChnnl;
            xpMnChnnl.IsExpanded = false;
            return xpMnChnnl;
        }


        public static Expander MnTxtrChnnls()
        {
            Expander xpMnTxtrChnnls = new Expander();
            xpMnTxtrChnnls.Name = "MnTxtrChnnls";
            xpMnTxtrChnnls.Header = "Main texture channels";

            StackPanel spMnTxtrChnnls = new StackPanel();
            spMnTxtrChnnls.Name = "spMnTxtrChnnls";

            
            spMnTxtrChnnls.Children.Add(MainChannelEditor("0") );
            spMnTxtrChnnls.Children.Add(MainChannelEditor("1") );
            spMnTxtrChnnls.Children.Add(MainChannelEditor("2") );

            xpMnTxtrChnnls.Content = spMnTxtrChnnls;
            xpMnTxtrChnnls.IsExpanded = false;
            return xpMnTxtrChnnls;
        }

        public static bool ParseMainChannelEditor(Expander xpX, ref MainCh_deltaTxtrSize x)
        {

            StackPanel temp = (xpX.Content as StackPanel).Children[0] as StackPanel;
            x.invMainCh = double.Parse((temp.Children[1] as TextBox).Text);

            temp = (xpX.Content as StackPanel).Children[1] as StackPanel;
            x.negMainCh = double.Parse((temp.Children[1] as TextBox).Text);

            temp = (xpX.Content as StackPanel).Children[2] as StackPanel;
            x.shftMainCh = double.Parse((temp.Children[1] as TextBox).Text);

            temp = (xpX.Content as StackPanel).Children[3] as StackPanel;
            x.pwrMainCh = double.Parse((temp.Children[1] as TextBox).Text);

            temp = (xpX.Content as StackPanel).Children[4] as StackPanel;
            x.deltaTxtrSize = double.Parse((temp.Children[1] as TextBox).Text);

            return true;
        }

        public static bool ParseMnTxtrChnnls(Expander xpX)
        {

            
            StackPanel temp = (xpX.Content as StackPanel);

            ParseMainChannelEditor((temp.Children[0] as Expander), ref wpfamkparam.Ch0);
            ParseMainChannelEditor((temp.Children[1] as Expander), ref wpfamkparam.Ch1);
            ParseMainChannelEditor((temp.Children[2] as Expander), ref wpfamkparam.Ch2);
            return true;
        }




/// <summary>
/// третий отдел 
/// - блюр на общую целлюляр текстуру и на три текстуры РГБ-каналов
/// - матрица коммутации/смешения каналов текстур
/// - константы преобразования для трех главных цветовых каналов RGB
/// 
/// </summary>


    public static Blur0012 xBlur,xBlur0,xBlur1,xBlur2;

    //the matrix contains prepared values for mixing of 3 textures and stores result textures as dekart coordinates of vectors in 3d space,
    //that can be convoluted with fourth texture (for examle with Cellular RGB) as linear combination.
    //mtrxMainCh[id of basis of dekart components of result texture 0..2][id of result RGB channel 0..2][id's of input texture #1,#2 0..1]
	//[2][0] [1][1] [2][2]
	//[2][2] [0][1] [2][2]
	//[2][2] [1][1] [0][2]
	public static int[,,] mtrxMainCh = {       {{2,0},{1,1},{2,2}},
                                               {{2,2},{0,1},{2,2}},
                                               {{2,2},{1,1},{0,2}}
                                        };//matrix of showinng textures.. X,Y, a and b. where 0-txtr, 1-txtr1, 2-txtr2, 3-txtrCellul;

    public int txtrCellPoints;//number of points in Cellular texture
	//cellul_tech txtrCellTechniq;
	//mtrl_1_tech mtrl1MatTecniq;

    public int pixelate_x, pixelate_y;  //x- and y-integer magnificiator by zoom  the outgoing picture
    public bool bAnimOn;                //is animation mode on? where the texture generators turns only one time by the first reneder, then change it parameters

    public static Expander BlurEditor(string EditorNamePostfix/* "0", "1", "2", "3" */)
    {
        Expander xpBlrEdtr = new Expander();
        xpBlrEdtr.Name = "xpBlrEdtr" + EditorNamePostfix;
        xpBlrEdtr.Header = "Edit texture #" + EditorNamePostfix + " blur properties";

        StackPanel spBlrEdtr = new StackPanel();
        spBlrEdtr.Name = "spBlrEdtr" + EditorNamePostfix;


        spBlrEdtr.Children.Add(LineEditor("itrBlur" + EditorNamePostfix, "numof blur iterations in pixels", "0"));
        spBlrEdtr.Children.Add(LineEditor("amountBlur" + EditorNamePostfix, "amount of pixels in blur", "0"));
        spBlrEdtr.Children.Add(LineEditor("beforeBlur" + EditorNamePostfix, "before iterated amnt of pxls in blur", "10"));

        xpBlrEdtr.Content = spBlrEdtr;
        xpBlrEdtr.IsExpanded = false;
        return xpBlrEdtr;
    }
    public static Expander CrtBlurs()
    {
        Expander xpBlurs = new Expander();
        xpBlurs.Name = "xpBlurs";
        xpBlurs.Header = "Create blur for all of textures 1-4";

        StackPanel spBlurs = new StackPanel();
        spBlurs.Name = "spBlurs";


        spBlurs.Children.Add(BlurEditor("0"));
        spBlurs.Children.Add(BlurEditor("1"));
        spBlurs.Children.Add(BlurEditor("2"));
        spBlurs.Children.Add(BlurEditor("3"));

        xpBlurs.Content = spBlurs;
        return xpBlurs;
    }


    public static bool ParseBlurEditor(Expander xpX, ref Blur0012 x)
    {

        StackPanel temp = (xpX.Content as StackPanel).Children[0] as StackPanel;
        x.itrBlur = int.Parse((temp.Children[1] as TextBox).Text);

        temp = (xpX.Content as StackPanel).Children[1] as StackPanel;
        x.amountBlur = int.Parse((temp.Children[1] as TextBox).Text);

        temp = (xpX.Content as StackPanel).Children[2] as StackPanel;
        x.beforeBlur = int.Parse((temp.Children[1] as TextBox).Text);

        return true;
    }

    public static bool ParseBlurs(Expander xpX)
    {


        StackPanel temp = (xpX.Content as StackPanel);

        ParseBlurEditor((temp.Children[0] as Expander), ref wpfamkparam.xBlur);
        ParseBlurEditor((temp.Children[1] as Expander), ref wpfamkparam.xBlur0);
        ParseBlurEditor((temp.Children[2] as Expander), ref wpfamkparam.xBlur1);
        ParseBlurEditor((temp.Children[3] as Expander), ref wpfamkparam.xBlur2);
        return true;
    }

    public static Expander MtrxLineEditor(int res, ref int[] dims, ref string[] defval, ref string[] heads, int line_id, int itrtn = 0)
    {

        int L1 = (3 + 2 * dims[1] + dims[2] - 0) * itrtn;
        if (res > 3) L1 = 3 + (3 + 2 * dims[1] + dims[2] - 0) * itrtn;
        // if (itrtn == 1) L1 = 0;
        
        int L2 = 0;//
        if (res > 3) {
            L2 = itrtn * dims[0] * dims[1] * dims[2];
                    }


        Expander xpLnNEdtr = new Expander();//экспандер одной строки матрицы
        xpLnNEdtr.Name = "xpLnNEdtr" + (itrtn).ToString();
        xpLnNEdtr.Header = heads[3+line_id + L1];


        StackPanel spMtrxLine;



        TextBlock tbHead_Z = new TextBlock();
        tbHead_Z.Text = heads[3 + dims[1] + line_id + L1];
        tbHead_Z.VerticalAlignment = VerticalAlignment.Center;
        tbHead_Z.HorizontalAlignment = HorizontalAlignment.Center;
        tbHead_Z.LayoutTransform = new RotateTransform(-90.0);

        StackPanel spHead_Z_wrapper = new StackPanel();//горизонтальная стекпанель аггрегирующая строку матрицы и подзаголовок Z
        spHead_Z_wrapper.Name = "spHead_Z_wrapper" + (itrtn).ToString();
        spHead_Z_wrapper.Orientation = Orientation.Horizontal;
        spHead_Z_wrapper.Children.Add(tbHead_Z);




        StackPanel spLnNEdtr = new StackPanel();//вертикальная стекпанель из линий вводимых полей одной строки матрицы
        spLnNEdtr.Name = "spLnNEdtr" + (itrtn).ToString(); ;
        spLnNEdtr.Orientation = Orientation.Vertical;





        TextBlock tbHead;
        TextBox tboxTextBox;



        for (int n = 0; n < dims[2]; n++) //перечисляем линии вводимых полей
        {


            spMtrxLine = new StackPanel();//горизонтальная линия вводимых полей
            spMtrxLine.Name = "spMtrxLine" + n.ToString() + (itrtn).ToString();
            spMtrxLine.Orientation = Orientation.Horizontal;


            tbHead = new TextBlock();
            tbHead.Text = heads[3 + 2 * dims[1] + n + L1]; //заголовок текущей линии полей

            spMtrxLine.Children.Add(tbHead);

            for (int i = 0; i < dims[0]; i++) //все вводимые поля текущей линии полей
            {

                tboxTextBox = new TextBox();
                tboxTextBox.Name = "tbox" + (itrtn).ToString();

                tboxTextBox.Text = defval[line_id * dims[2] * dims[0] + n * dims[0] + i + L2];
                    
                //    dims[0] * i + dims[1] * line_id + dims[2] * n ];
                //   z*RY*RX + (Y*rx+x)
                
                tboxTextBox.Margin = new Thickness(4);
                tboxTextBox.Width = 33;
                spMtrxLine.Children.Add(tboxTextBox);
            }


            spLnNEdtr.Children.Add(spMtrxLine);//добавляем очередную линию полей в создаваемую строку матрицы

        }

        spHead_Z_wrapper.Children.Add(spLnNEdtr);
        xpLnNEdtr.Content = spHead_Z_wrapper;
        return xpLnNEdtr;//возвращаем экспандер созданной одной строки матрицы
    }

        /// <summary>
        /// создает визуальный контрол для ввода знчений многомерной мартицы
        /// с заданными значениями и заголовками
        /// </summary>
        /// <param name="res"> размерность матрицы 3+</param>
        /// <param name="dims">длина каждой оси (кол-во элементов каждой размерности)</param>
        /// <param name="defval">символьная матрица значений по умолчанию</param>
        /// <param name="Heads">символьная матрица заголовков и подзаголовков </param>
        /// <param name="Postfix">добавляемый постфикс к имени элемента в визуальном дереве элементов</param>
        /// <returns></returns>
    public static Expander MtrxEditor(int res, ref int[] dims, ref string[] defval, ref string[] heads, int itrtn=0)
    {

        int L1=0;//смещение на строку в массиве заголовков (heads) и значений по умолчанию (defval).


        if (res == 3) L1 = 0; //когда только три измерения то никакой итерации нет функция вызывается только один раз

        //когда четыре измерения то сверху добавляется еще четыре строки в массиве заголовков ид = (0,1,2)
        if (res >= 4)
        if (itrtn > 0)//
        {
            L1 = 3+ (3 + 2 * dims[1] + dims[2] - 0) * (itrtn - 1);        
        }
        


        Expander xpMtrxEdtr = new Expander();
        xpMtrxEdtr.Name = "xpMtrxEdtr_" + itrtn.ToString();
        xpMtrxEdtr.Header = heads[0 + L1];

        StackPanel spMtrxEdtr = new StackPanel();
        spMtrxEdtr.Name = "spMtrxEdtr_" + itrtn.ToString();


        TextBlock tbHead_X = new TextBlock();
        tbHead_X.Text = heads[1 + L1];
        tbHead_X.VerticalAlignment = VerticalAlignment.Center;
        tbHead_X.HorizontalAlignment = HorizontalAlignment.Center;
        spMtrxEdtr.Children.Add(tbHead_X);

        StackPanel spMtrxEdtrInner1 = new StackPanel();
        spMtrxEdtrInner1.Name = "spMtrxEdtrInner1_" + itrtn.ToString();
        spMtrxEdtrInner1.Orientation = Orientation.Horizontal;


        /////////////////////
        TextBlock tbHead_Y = new TextBlock();
        tbHead_Y.Text = heads[2 + L1];
        tbHead_Y.VerticalAlignment = VerticalAlignment.Center;
        tbHead_Y.HorizontalAlignment = HorizontalAlignment.Center;
        tbHead_Y.LayoutTransform = new RotateTransform(-90.0);
        /////////////////////

        spMtrxEdtrInner1.Children.Add(tbHead_Y);

        /////////////////////
        StackPanel spMtrxEdtrInner2 = new StackPanel();
        spMtrxEdtrInner2.Name = "spMtrxEdtrInner2_" + itrtn.ToString();
        spMtrxEdtrInner2.Orientation = Orientation.Vertical;



        
                if ( (res-itrtn) <= 3)
                {
                    int temp_itrtn = itrtn;
                    if ( (res>3)&&(itrtn > 0) ) temp_itrtn -= 1;

                    for (int i = 0; i < dims[1]; i++)
                    //spMtrxEdtrInner2.Children.Add(LineNEditor(dims[res - itrtn -3], dims[res - itrtn -2], ref heads /*!*/, heads[1], "result_E3"));
                      spMtrxEdtrInner2.Children.Add(MtrxLineEditor(res, ref dims, ref defval, ref heads, i, temp_itrtn));
                }
                else
                {
                    /*
                    myheads = new string[len];
                    mydims = new int[res - 1];
                    int m = 0;
                    for (int j = 1; j < res; j++)
                    {
                        mydims[j - 1] = dims[j];
                        for (int l = 0; l < dims[j]; l++)
                        {
                            myheads[m] = heads[dims[0] + m];
                            m++;
                        }
                    }
                    */

                    for (int i = 0; i < dims[res-itrtn-1]; i++)
                    spMtrxEdtrInner2.Children.Add(MtrxEditor(res, ref dims, ref defval, ref heads, i+1));
                }

        ////////////////////

        spMtrxEdtrInner1.Children.Add(spMtrxEdtrInner2);

        spMtrxEdtr.Children.Add(spMtrxEdtrInner1);

        xpMtrxEdtr.Content = spMtrxEdtr;
        xpMtrxEdtr.IsExpanded = false;
        return xpMtrxEdtr;
    }
            //
    public static bool ParseMtrxLineEditor(Expander xpX, int res, ref int[] dims, ref int[] val, int line_id /*0..dims[1]*/, int L2 = 0/* itrtn*dims[0]*dims[1]*dims[2] */)
    {
        for (int i2 = 0; i2 < dims[2]; i2++)
            for (int i0 = 0; i0 < dims[0]; i0++)
                //xpLnNEdtr.spHeadZ_wrapper[1].spLnNEdtr[i2].spMtrxLine[i0]
                val[line_id*dims[2]*dims[0] + i2 * dims[0] + i0 + L2] = int.Parse(((((xpX.Content as StackPanel).Children[1] as StackPanel).Children[i2] as StackPanel).Children[i0+1] as TextBox).Text.ToString());
        return true;
    }

    public static bool ParseMtrx(Expander xpX, int res, ref int[] dims, out int[] val)
        {
            //string sres = "";

            val = null;
            if ((res < 3) || (res > 4)) return false;
            Expander xpMtrxLineEditor, xpMtrxLineEditor0;

            if (res==3) {

                val = new int[dims[0] * dims[1] * dims[2]];
                for (int i1 = 0; i1 < dims[1]; i1++)
                {
                    //           xpMtrxEdtr.spMtrxEdtr[1].spMtrxEdtrInner1[1].spMtrxEdtrInner2[0..dims[1]].(function MtrxLineEditor as Expander)
                    xpMtrxLineEditor = ((((xpX.Content as StackPanel).Children[1] as StackPanel).Children[1] as StackPanel).Children[i1] as Expander);                    
                    ParseMtrxLineEditor(xpMtrxLineEditor, res, ref dims, ref val, i1, 0);
                }
                        }
            else
            {
                //sres = "";
                val = new int[ dims[0] * dims[1] * dims[2] * dims[3] ];

                for (int i3 = 0; i3 < dims[3]; i3++) { 
                    xpMtrxLineEditor0 = ((((xpX.Content as StackPanel).Children[1] as StackPanel).Children[1] as StackPanel).Children[i3] as Expander);
                  //  sres += xpMtrxLineEditor0.Name.ToString()+"\n"+"========="+"\n";


                for (int i1 = 0; i1 < dims[1]; i1++)
                {
                    //           xpMtrxEdtr.spMtrxEdtr[1].spMtrxEdtrInner1[1].spMtrxEdtrInner2[0..dims[1]].(function MtrxLineEditor as Expander)
                    xpMtrxLineEditor = ((((xpMtrxLineEditor0.Content as StackPanel).Children[1] as StackPanel).Children[1] as StackPanel).Children[i1] as Expander);

                    // sres += "\n";
                    // sres += xpMtrxLineEditor.Name.ToString();

                    ParseMtrxLineEditor(xpMtrxLineEditor, res, ref dims, ref val, i1, i3 * dims[0] * dims[1] * dims[2]);
                }
                }


            }

            return true;
        }
    }

}
