﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using amk_rgb = System.Double;


namespace Editor_A
{

    public enum cellul_tech
    {
        ct_nearest,
        ct_diffntl_nrst,
        ct_diffntl2_nrst,
        ct_diffntl5_nrst,

        ct_diffntl2_nrst_quad,
        ct_folliage
    }; // peretchislenie technik dlja karty cellular

    public enum mtrl_1_tech
    {
        mtrl_1_witrage,
        mtrl_1_crstll_marble_sky
    }; // peretchislenie technik materialoff dlja materiala 1

    /// <summary>
    /// main options for animation
    /// </summary>
public struct amk_options
{

    public double invMainCh0, invMainCh1, invMainCh2;  // inverse or not main channel (1.0f, -1.0f) == f(x) or 1/f(x)
	public double negMainCh0, negMainCh1, negMainCh2;  // negate or not main channel (1.0f ... -1.0f) == f(x) or  -f(x)
	public double shftMainCh0, shftMainCh1, shftMainCh2;// shift constant of main channel f(x) + a
	public double pwrMainCh0, pwrMainCh1, pwrMainCh2; // pwr of main channel pow(f(x),pwrMainCh) default value -1.0f/3.0f
	public double deltaTxtrSize, deltaTxtr1Size, deltaTxtr2Size; // >=0

	public int   randSeed;  ///init value(seed) for random numbers generator
	public bool  brandSeed; // if true get randseed automatically from clock
	public Random rand;
	public int   iSize;	 // output bitmap screen size 256,512,1024,2048,4096
    public string prefixFN;  //prefix for output filename
	public int   idFN;		//incremmentally number for filename (prefixFN+itoa(idFN)+".png"), if<0 no number in filename used, just prefix
	public bool bFN_gen;   //if true generate number for filename incremmentally
	public string wFN;

	public int    itrBlur,   itrBlur0,   itrBlur1,   itrBlur2;//number of blur iterations of amountBlur pixels each
	public int amountBlur,amountBlur0,amountBlur1,amountBlur2;//amount of pixels in blur
	public int beforeBlur,beforeBlur0,beforeBlur1,beforeBlur2;//amount of pixels in blur before iterated blur

	//the matrix contains prepared values for mixing of 3 textures and stores result textures as dekart coordinates of vectors in 3d space,
	//that can be convoluted with fourth texture (for examle with Cellular RGB) as linear combination.
	//mtrxMainCh[id of basis of dekart components of result texture 0..2][id of result RGB channel 0..2][id's of input texture #1,#2 0..1]
	//[2][0] [1][1] [2][2]
	//[2][2] [0][1] [2][2]
	//[2][2] [1][1] [0][2]
	public int[,,] mtrxMainCh;//[3][3][2];//matrix of showinng textures.. X,Y, a and b. where 0-txtr, 1-txtr1, 2-txtr2, 3-txtrCellul;
	public int txtrCellPoints;//number of points in Cellular texture
	public cellul_tech txtrCellTechniq;
	public mtrl_1_tech mtrl1MatTecniq;

	public int pixelate_x, pixelate_y ;//x- and y-integer magnifickator by zoom  the outgoing picture
	public bool bAnimOn; //is animation mode on? where the texture generators turns only one time by the first reneder, then change it parameters

	/// <summary>
	/// INITIALIZE THE PARAMETERS OF RENDERING
	/// </summary>
	/// <param name="pbrandSeed">if true get randseed automatically from clock</param>
	/// <param name="prandSeed">init value(seed) for random numbers generator</param>
    public void initialize(bool pbrandSeed = true, int prandSeed = 29711203)
	{
    brandSeed = pbrandSeed;
    if  (brandSeed) randSeed =  Environment.TickCount;
    else	        randSeed = prandSeed; //19150103
	
    rand = new Random(randSeed);

	invMainCh0 = (rand.NextDouble()*100D) > 19D ? 1.0D : -1.0D;
	negMainCh0 = (rand.NextDouble()*1000D) / 1000.0D - .5D;
	shftMainCh0 =(rand.NextDouble()*1000D) / 1000.0D;
	pwrMainCh0 = -1.0D / 3.0D;

	invMainCh1 = invMainCh0;
	negMainCh1 = negMainCh0;
	shftMainCh1 = shftMainCh0;
	pwrMainCh1  = pwrMainCh0;


	invMainCh2 = invMainCh0;
	negMainCh2 = negMainCh0;
	shftMainCh2 = shftMainCh0;
	pwrMainCh2  = pwrMainCh0;

	deltaTxtrSize=4; deltaTxtr1Size=1; deltaTxtr2Size=3;



	//the matrix contains prepared values for mixing of 3 textures and stores result textures as dekart coordinates of vectors in 3d space,
	//that can be convoluted with fourth texture (for examle with Cellular RGB) as linear combination.
	//mtrxMainCh[id of basis of dekart components of result texture 0..2][id of result RGB channel 0..2][id's of input texture #1,#2 0..1]
	//                x  y  a,b  0-txtr, 1-txtr1, 2-txtr2
    mtrxMainCh =new int[3,3,2];

	mtrxMainCh[0,0,0] = 2;
	mtrxMainCh[0,0,1] = 0;
	mtrxMainCh[1,0,0] = 1;
	mtrxMainCh[1,0,1] = 1;
	mtrxMainCh[2,0,0] = 2;
	mtrxMainCh[2,0,1] = 2;

	mtrxMainCh[0,1,0] = 1;
	mtrxMainCh[0,1,1] = 1;
	mtrxMainCh[1,1,0] = 0;
	mtrxMainCh[1,1,1] = 1;
	mtrxMainCh[2,1,0] = 1;
	mtrxMainCh[2,1,1] = 1;

	mtrxMainCh[0,2,0] = 2;
	mtrxMainCh[0,2,1] = 2;
	mtrxMainCh[1,2,0] = 1;
	mtrxMainCh[1,2,1] = 1;
	mtrxMainCh[2,2,0] = 0;
	mtrxMainCh[2,2,1] = 2;


	iSize = 512;//512;//256;//1024; //4096;

	prefixFN = "tib2_";
	idFN = 0;//VERY IMPORTANT THAT WILL BE START FROM ZERO!!! (FOR ALL THE ANIMATION)
	bFN_gen = true;
		
	txtrCellPoints = (6 + ( rand.Next(5) ))*100;
	txtrCellTechniq = cellul_tech.ct_diffntl_nrst;
	//mtrl1MatTecniq = mtrl_1_tech::mtrl_1_witrage;
	mtrl1MatTecniq = mtrl_1_tech.mtrl_1_crstll_marble_sky;

	itrBlur=15;
	amountBlur=8;
	beforeBlur=5;

	itrBlur0=0;
	amountBlur0=0;
	beforeBlur0=10;

	itrBlur1=0;
	amountBlur1=0;
	beforeBlur1=10;

	itrBlur2=0;
	amountBlur2=0;
	beforeBlur2=10;

	pixelate_x=1; pixelate_y=1;//x- and y-integer magnifickator by zoom  the outgoing picture
	bAnimOn = false; //is animation mode on? where the texture generators turns only one time by the first reneder, then change it parameters

		//	enum iflCommander {iflPlain,iflPingPong};
        //generateIFLFile(350, prefixFN, prefixFN);
	//generateIFLFile (350, "tib2_", "tib2_");

	}

	



	public void gen_new_opt()// 
	{	
	pixelate_x=1; pixelate_y=1;
	bAnimOn = true;

	//18	itrBlur=0*5;
	//18	amountBlur=3;
	//18	beforeBlur=5;

	//18	itrBlur0=0;
	//18	amountBlur0=0;
	//18	beforeBlur0=10;//2;

	//18	itrBlur1=0;
	//18	amountBlur1=0;
	//18	beforeBlur1=10;//2;

	//18	itrBlur2=0;
	//18	amountBlur2=0;
	//18	beforeBlur2=10;//2;

	
	//18 deltaTxtrSize=4*6;
	//18 	deltaTxtr1Size=1*6;
	//18 	deltaTxtr2Size=3*6;


	if (idFN==0) {
		//18 		 invMainCh0 = 0.928000; negMainCh0 = 0.605000; shftMainCh0 = 0.080000; pwrMainCh0 = -0.333333;
		//18 		 invMainCh1 = 0.928000; negMainCh1 = 0.605000; shftMainCh1 = 0.080000; pwrMainCh1 = -0.333333;
		//18 		invMainCh2 = 0.928000; negMainCh2 = 1.605000; shftMainCh2 = 0.080000; pwrMainCh2 = -0.333333;
	} else 
	if (idFN==1) {
 //mt1->txtr->flip();
 //mt1->txtr1->flip();
 //mt1->txtr2->flip();
		//mt1->txtrCellul->flip(procedural::flipx,procedural::addy2addx2,1.0f,1.0f);

				} else
	{
		//mt1->txtrCellul->flip(procedural::flipx,procedural::addy2addx2,1.0f/(float)idFN,1.0f/(float)idFN);
		//mt1->txtrCellul->flip(procedural::flipxy,procedural::addxdivy,1.0f,1.0f);
		//shftMainCh0 = (shftMainCh0+(float)idFN*0.00005f);

		mw.mt1.txtr.move(-1,0);//green
        mw.mt1.txtr1.move(-4, 0);//blue // osnovnie belie wspolochi
        mw.mt1.txtr2.move(-2, 0);//red
        mw.mt1.txtrCellul.move(1, 0);//green
		

		
	}

	//                x  y  a,b  0-txtr, 1-txtr1, 2-txtr2
//19	mtrxMainCh[0][0][0] = 3; mtrxMainCh[1][0][0] = 2; mtrxMainCh[2][0][0] = 1;
//19	mtrxMainCh[0][0][1] = 3; mtrxMainCh[1][0][1] = 0; mtrxMainCh[2][0][1] = 1;

//19	mtrxMainCh[0][1][0] = 0; mtrxMainCh[1][1][0] = 3; mtrxMainCh[2][1][0] = 3;
//19	mtrxMainCh[0][1][1] = 0; mtrxMainCh[1][1][1] = 3; mtrxMainCh[2][1][1] = 1;

//19	mtrxMainCh[0][2][0] = 2; mtrxMainCh[1][2][0] = 1; mtrxMainCh[2][2][0] = 3;
//19	mtrxMainCh[0][2][1] = 0; mtrxMainCh[1][2][1] = 1; mtrxMainCh[2][2][1] = 3;

	iSize = iSize;//4096;

	prefixFN = prefixFN;
	//idFN = 1;
	bFN_gen = bFN_gen;
		
	txtrCellPoints = (9 + rand.Next(17) )*10;
	generateLogFile();

	}


	

	void reconstrFN() {wFN =prefixFN+idFN.ToString()+".png";}

	public void generateFN() {
		if (bFN_gen)
        wFN =prefixFN+idFN.ToString()+".png";//if incremment used
		else
        wFN =prefixFN+".png"; //if not (for static cadre)
		idFN++; //generate new id all the time (need for animatio)
		return;
							}

	public void generateLogFile()
	{
	string buff = prefixFN+idFN.ToString()+".txt";

      System.IO.TextWriter streamWriter = new System.IO.StreamWriter(buff);



              
            //streamReader = new StreamReader("g:\\prj\\ball14_1f.txt", Encoding.GetEncoding(1251));

      streamWriter.WriteLine("prefixFN = \"{0}\"; idFN = {1:D};\n\n", prefixFN, idFN);
      streamWriter.WriteLine("Image size (X*Y) = ({0:D} x {1:D})\n", iSize * pixelate_x, iSize * pixelate_y);
	streamWriter.WriteLine("///////////////////////////////////////////\n");
    streamWriter.WriteLine("iSize = {0:D};\n", iSize);
    streamWriter.WriteLine("itrBlur = {0:D};	amountBlur = {1:D};	beforeBlur = {2:D};\n ", itrBlur, amountBlur, beforeBlur);
    streamWriter.WriteLine("itrBlur0 = {0:D};	amountBlur0 = {1:D};	beforeBlur0 = {2:D};\n ", itrBlur0, amountBlur0, beforeBlur0);
    streamWriter.WriteLine("itrBlur1 = {0:D};	amountBlur1 = {1:D};	beforeBlur1 = {2:D};\n ", itrBlur1, amountBlur1, beforeBlur1);
    streamWriter.WriteLine("itrBlur2 = {0:D};	amountBlur2 = {1:D};	beforeBlur2 = {2:D};\n\n ", itrBlur2, amountBlur2, beforeBlur2);

    streamWriter.WriteLine("deltaTxtrSize = {0:G};	deltaTxtr1Size = {1:G};	deltaTxtr2Size = {2:G};\n\n ", deltaTxtrSize, deltaTxtr1Size, deltaTxtr2Size);


    streamWriter.WriteLine("invMainCh0 = {0:G}; negMainCh0 = {1:G}; shftMainCh0 = {2:G}; pwrMainCh0 = {3:G};\n", invMainCh0, negMainCh0, shftMainCh0, pwrMainCh0);
    streamWriter.WriteLine("invMainCh1 = {0:G}; negMainCh1 = {1:G}; shftMainCh1 = {2:G}; pwrMainCh1 = {3:G};\n", invMainCh1, negMainCh1, shftMainCh1, pwrMainCh1);
    streamWriter.WriteLine("invMainCh2 = {0:G}; negMainCh2 = {1:G}; shftMainCh2 = {2:G}; pwrMainCh2 = {3:G};\n", invMainCh2, negMainCh2, shftMainCh2, pwrMainCh2);

    streamWriter.WriteLine("txtrCellPoints = {0:D};\n", txtrCellPoints);

		//////prints the Cellular techniq
		 string ts;		
		 switch(txtrCellTechniq)	{
			case cellul_tech.ct_nearest: ts="ct_nearest"; break;
			case cellul_tech.ct_diffntl_nrst: ts="ct_diffntl_nrst"; break;
			case cellul_tech.ct_diffntl2_nrst: ts="ct_diffntl2_nrst"; break;
			case cellul_tech.ct_diffntl5_nrst:	ts="ct_diffntl5_nrst"; break;
			case cellul_tech.ct_diffntl2_nrst_quad:	ts="ct_diffntl2_nrst_quad"; break;
			case cellul_tech.ct_folliage: ts="ct_folliage"; break;
            default: ts = "unknown"; break;
         };
	streamWriter.WriteLine("txtrCellTechniq = {0};\n", ts);
		//////prints the Cellular techniq


		//////prints the Material 1 Techniq
		 string mt;		
		 switch(mtrl1MatTecniq)	{
			case mtrl_1_tech.mtrl_1_witrage: mt="mtrl_1_witrage //first tehnick - witrage, stucco, color smoke plams"; break;
			case mtrl_1_tech.mtrl_1_crstll_marble_sky: mt="mtrl_1_crstll_marble_sky //second tehnick - cristalls with inner light, marble, sky"; break;
            default: mt = "unknown"; break;
         };
		streamWriter.WriteLine("mtrl1MatTecniq = {0};\n", mt);
		//////prints the Material 1 Techniq


        streamWriter.WriteLine("randSeed = {0:D};\n", randSeed);
		streamWriter.WriteLine( "brandSeed = {0};\n", brandSeed ? "True" : "False");
		streamWriter.WriteLine( "bFN_gen	= {0}; ", bFN_gen ? "True" : "False");
		streamWriter.WriteLine( "//if true generate number for filename incremmentally\n");
		streamWriter.WriteLine( "string wFN= \"{0}\";\n",wFN);

		streamWriter.WriteLine("\n\n//int mtrxMainCh[3][3][2];//matrix of showinng textures.. X,Y, a and b. where 0-txtr, 1-txtr1, 2-txtr2;\n");
		for (int i=0; i<3;i++) {
					for (int j=0; j<3;j++)
                        streamWriter.WriteLine("mtrxMainCh[{0:D},{1:D},0] = {2:D}; mtrxMainCh[{3:D},{4:D},1] = {5:D}; ", j, i, mtrxMainCh[j, i, 0], j, i, mtrxMainCh[j, i, 1]);
					streamWriter.WriteLine( "\n");
		}
		//[2][0] [1][1] [2][2]
		//[2][2] [0][1] [2][2]
		//[2][2] [1][1] [0][2]


        streamWriter.WriteLine("\npixelate_x={0:D}; pixelate_y={1:D};//x- and y-integer magnifickator by zoom  the outgoing picture \n\n", pixelate_x, pixelate_y);
		
		streamWriter.WriteLine( " /////////////////////////////////////////////////////////////////////\n");
		streamWriter.WriteLine( " //is animation mode on? Where the texture generators\n");
		streamWriter.WriteLine( " //turns only one time by the first reneder, then change it parameters\n");
		streamWriter.WriteLine( "bAnimOn	= {0}; ", bAnimOn ? "True" : "False");
		/*
		for(int k=0; k<322;k++) 
			fprintf( flog, "animated_%d.png\n",k);

		for(int k=322; k>=0;k--) 
			fprintf( flog, "animated_%d.png\n",k);
			*/
              streamWriter.Close();

	}



		public enum iflCommander {iflPlain,iflPingPong};//command and generator for animation lists
        public void generateIFLFile(int num, string listfname, string bitmapfname, iflCommander pCmnd = iflCommander.iflPlain)
		{
    	string buff = listfname+".ifl";


        System.IO.TextWriter streamWriter = new System.IO.StreamWriter(buff);

		
		for(int k=0; k<num;k++) 
		streamWriter.WriteLine("{0}{1:D}.png\n",bitmapfname,k);

		if (pCmnd==iflCommander.iflPingPong)
		for(int k=num; k>=0;k--)
            streamWriter.WriteLine("{0}{1:D}.png\n", bitmapfname, k);
			
              streamWriter.Close();

		}
}

    public static class mw
    {
    public static amk_options mw_opt;
    public static cl_mtrl_witrag mt1;
    }



    /// <summary>
    /// all of procedural textures and filters modifiers of it
    /// </summary>
    public class procedural
    {
        /// <summary>
        /// the array of x,y,rgb=0,1,2 = 3*(x*size_y+y) + 0,1,2
        /// </summary>
	double[] arr;
	int size_x, size_y;


	public double get_arr(int x, int y) 
	{
		if ( (arr==null) ||
			 (x>=size_x) ||
			 (x< 0     ) ||
			 (y>=size_y) ||
			 (y< 0     ) ) return 0.0D;


		return arr[x*size_y+y];
	}

    public double set_arr(int x, int y, double val) 
	{
		if ( (arr==null) ||
			 (x>=size_x) ||
			 (x< 0     ) ||
			 (y>=size_y) ||
			 (y< 0     ) ) return 0.0D;

		       arr[x*size_y+y] = val;
		return arr[x*size_y+y];
	}


    public double DistToNearestPoint(double x, double y, double[] xcoords, double[] ycoords, int numPoints, ref double retMindist2, ref double retMindist3, ref double retMindist4, ref double retMindist5)
	{
    double mindist =  double.MaxValue;//3.4E+38;
	double mindist2 = double.MaxValue;//3.4E+38;
	double mindist3 = double.MaxValue;//3.4E+38;
	double mindist4 = double.MaxValue;//3.4E+38;
	double mindist5 = double.MaxValue;//3.4E+38;
    double dist;

	for (int i=0; i<numPoints; i++) {
        dist = Math.Sqrt( Math.Pow((xcoords[i]-x),2) + Math.Pow( (ycoords[i]-y),2) );
		if (dist < mindist) {mindist5=mindist4;mindist4=mindist3;mindist3=mindist2;mindist2=mindist; mindist= dist;}	
	}

	retMindist2=mindist2;
	retMindist3=mindist3;
	retMindist4=mindist4;
	retMindist5=mindist5;
    return mindist;
	}



    public enum flipcmd { flipnone, flipx, flipy, flipxy };
    public enum addcmd { addnone, addadd, addmult, addxdivy, addydivx, addxpowy, addypowx, addy2addx2 };

    /// <summary>
    /// flipping texture by x,y and xy with logic operation with source array
    /// </summary>
    public void flip(flipcmd fc = flipcmd.flipxy, addcmd cadd = addcmd.addnone, double DKoeff1 = 1.0D, double DKoeff2 = 1.0D, int p_size_x = 0, int p_size_y = 0)
	{
		double[] distBuffer = new double[size_x*size_y];

		int ix,iy;

		if ((size_x==0)||(size_y==0)) return;

		if (p_size_x==0) p_size_x=size_x; else if (p_size_x>size_x) p_size_x=  p_size_x - (p_size_x / size_x)*size_x;
		if (p_size_y==0) p_size_y=size_y; else if (p_size_y>size_y) p_size_y=  p_size_y - (p_size_y / size_y)*size_y;

for( int x=0; x<size_x; x++)
 for( int y=0; y<size_y; y++)
 {
	 ix = (p_size_x - x)-1;
	 iy = (p_size_y - y)-1;

	 //2distBuffer[x*size_y+y] = (arr[x*size_y+y]+arr[ix*size_y+y]+arr[x*size_y+iy]+arr[x*size_y+y])/3.0f;
	 
	 switch(fc)	{
	 case flipcmd.flipxy:	 distBuffer[x*size_y+y] = arr[ix*size_y+iy]; break;
	 case flipcmd.flipx:	 distBuffer[x*size_y+y] = arr[ix*size_y+ y]; break;
	 case flipcmd.flipy:	 distBuffer[x*size_y+y] = arr[ x*size_y+iy]; break;
	 case flipcmd.flipnone:
	 default: distBuffer[x*size_y+y] = arr[ x*size_y+y]; break;
						}

	 //Add or not source and flipped?
	 switch(cadd)	{
		case addcmd.addadd:
		distBuffer[x*size_y+y] = DKoeff1*arr[ x*size_y+y]+DKoeff2*distBuffer[ x*size_y+y];
		break;

		case addcmd.addmult:
		distBuffer[x*size_y+y] = DKoeff1*arr[ x*size_y+y]*DKoeff2*distBuffer[ x*size_y+y];
		break;

		case addcmd.addxdivy:
		distBuffer[x*size_y+y] = DKoeff1*arr[ x*size_y+y] / ((DKoeff2*distBuffer[ x*size_y+y]==0.0D) ? 1.0D : DKoeff2*distBuffer[ x*size_y+y]);
		break;

		case addcmd.addydivx:
		distBuffer[x*size_y+y] = DKoeff2*distBuffer[ x*size_y+y] / ((DKoeff1*arr[ x*size_y+y]==0.0D) ? 1.0D : DKoeff1*arr[ x*size_y+y]);
		break;

		case addcmd.addxpowy:
		distBuffer[x*size_y+y] = Math.Pow( DKoeff1*arr[ x*size_y+y], DKoeff2*distBuffer[ x*size_y+y] );
		break;

		case addcmd.addypowx:
		distBuffer[x*size_y+y] = Math.Pow( DKoeff2*distBuffer[ x*size_y+y], DKoeff1*arr[ x*size_y+y] );
		break;

		case addcmd.addy2addx2:
		distBuffer[x*size_y+y] = DKoeff2*distBuffer[ x*size_y+y]*distBuffer[ x*size_y+y]+ DKoeff1*arr[ x*size_y+y]*arr[ x*size_y+y];
		break;

		case addcmd.addnone:
		default:
			distBuffer[x*size_y+y] *= DKoeff1;
             break;
					}


 }
        distBuffer.CopyTo(arr,0);
	 
		return;
	}	//void flip(



    /// <summary>
    /// move the texture
    /// </summary>
    public void move(int p_size_x, int p_size_y)
	{
		double[] distBuffer = new double[size_x*size_y];

		int ix,iy;


for( int x=0; x<size_x; x++)	{
 for( int y=0; y<size_y; y++)
 {
	 ix = (x+p_size_x);
	 iy = (y+p_size_y);

	 if (ix<0) ix = size_x+ix;
	 if (ix>=size_x) ix = ix-size_x;

	 if (iy<0) iy = size_y+iy;
	 if (iy>=size_y) iy = iy-size_y;

	 distBuffer[ix*size_y+iy] = arr[x*size_y+y];
	 //1distBuffer[x*size_y+y] = arr[((x+p_size_x) % size_x)*size_y+((y+p_size_y) % size_y)];
 }

								}



        distBuffer.CopyTo(arr,0);
		return;
	}


    /// <summary>
    /// the bluring and dissolving texture like flame stripes
    /// </summary>
	public void move_dsslv_blur(int p_size_x,int p_size_y)
	{

		double[] distBuffer = new double[size_x*size_y];
		bool bUp = false;
		bool bDown = false;
		bool bLeft = false;
		bool bRight = false;


		if (p_size_y!=0) { if (p_size_y<0) bUp = true; else bDown = true; }
		if (p_size_x!=0) { if (p_size_x<0) bLeft = true; else bRight = true; }

		int ix,iy;


for( int x=0; x<size_x; x++)	{
 for( int y=0; y<size_y; y++)
 {
	 ix = (x+p_size_x);
	 iy = (y+p_size_y);

	 if (ix<0) ix = size_x+ix;
	 if (ix>=size_x) ix = ix-size_x;

	 if (iy<0) iy = size_y+iy;
	 if (iy>=size_y) iy = iy-size_y;

	 distBuffer[ix*size_y+iy] = arr[x*size_y+y];
	 //1distBuffer[x*size_y+y] = arr[((x+p_size_x) % size_x)*size_y+((y+p_size_y) % size_y)];
 }

								}

int iA; double DA;
int iB; double DB;
double DC;double DMin;double DDelta;


if (bLeft) for( int y=0; y<size_y; y++) 
	for( int k=-p_size_x; k>0; k--)
{
	iA = (size_x-k-1)*size_y+y;//levee
	iB = (size_x-k  )*size_y+y;//pravee
	DA = distBuffer[iA];	DB = distBuffer[iB];
	DC = (DB==0.0D) ? 1.0D : DA/DB;
	DMin = Math.Min(DA,DB); DDelta = (DA-DB);

distBuffer[iA] = DMin + 3.0D*DDelta/18.0D;
distBuffer[iB] = DMin + 1.0D*DDelta/18.0D;
}


if (bRight) for( int y=0; y<size_y; y++) 
	for( int k=p_size_x; k>0; k--)
{
	iA = (k)*size_y+y;//pravee
	iB = (k-1)*size_y+y;//levee
	DA = distBuffer[iA];	DB = distBuffer[iB];
	DC = (DB==0.0D) ? 1.0D : DA/DB;
	DMin = Math.Min(DA,DB); DDelta = (DA-DB);

distBuffer[iA] = DMin + 3.0D*DDelta/18.0D;
distBuffer[iB] = DMin + 1.0D*DDelta/18.0D;
}




if (bDown) for( int x=0; x<size_x; x++) 
	for( int k=p_size_y; k>0; k--)
{
	iA = x*size_y+k;//nige
	iB = x*size_y+k-1;//vishe
	DA = distBuffer[iA];	DB = distBuffer[iB];
	DC = (DB==0.0D) ? 1.0D : DA/DB;
	DMin = Math.Min(DA,DB); DDelta = (DA-DB);

distBuffer[iA] = DMin + 3.0D*DDelta/18.0D;
distBuffer[iB] = DMin + 1.0D*DDelta/18.0D;
}



if (bUp) for( int x=0; x<size_x; x++)
	for( int k=-p_size_y; k>0; k--)
{
	iA = x*size_y+(size_y-k)-1;//vishe
	iB = x*size_y+(size_y-k)-1+1;//nige
	DA = distBuffer[iA];	DB = distBuffer[iB];
	DC = (DB==0.0D) ? 0.50D : DA/DB;
	DMin = Math.Min(DA,DB); DDelta = (DA-DB);

distBuffer[iA] = DMin + 3.0D*DDelta/18.0D;
distBuffer[iB] = DMin + 1.0D*DDelta/18.0D;
}

distBuffer.CopyTo(arr,0);
		return;
	}




	public void generate_cellular(cellul_tech mytech=cellul_tech.ct_diffntl_nrst)
	{

        int numPoints = mw.mw_opt.txtrCellPoints;//26
double[] distBuffer = new double[size_x*size_y];
double[]    xcoords = new double[numPoints];
double[]    ycoords = new double[numPoints];
double mindist = size_x*size_x+size_y*size_y;//infinity;
double maxdist = 0;

for( int i=0; i<numPoints; i++) {
    xcoords[i] = mw.mw_opt.rand.NextDouble() * size_x;//random number between 0 and width
    ycoords[i] = mw.mw_opt.rand.NextDouble() * size_y;//random number between 0 and height
}


double distance = 0.0D;
double distance2 = 0.0D;
double distance3 = 0.0D;
double distance4 = 0.0D;
double distance5 = 0.0D;

for( int x=0; x<size_x; x++)
 for( int y=0; y<size_y; y++)
 {
	distance =DistToNearestPoint((double) x, (double) y,  xcoords,  ycoords, numPoints, ref distance2, ref distance3,ref distance4,ref distance5);
	
	
	switch(mytech) {

        case cellul_tech.ct_nearest:
	////////////////output2 png
	 distBuffer[x*size_y+y] = distance;
	////////////////output2 png
		break;

        case cellul_tech.ct_diffntl_nrst:
	////////////////output3 png
	 distBuffer[x*size_y+y] = distance2-distance;
	////////////////output3 png
	 break;




        case cellul_tech.ct_diffntl2_nrst:
	////////////////output4 png
    distBuffer[x*size_y+y] = Math.Pow((distance3-distance2),14)/Math.Pow((distance2-distance),14);//distance to nearest point
	////////////////output4 png
	break;








        case cellul_tech.ct_diffntl5_nrst:
	////////////////output1 png
	distBuffer[x*size_y+y] = 0.0D;

	for (int i=1; i<4; i++)
	distBuffer[x*size_y+y] += 
	(
	    Math.Pow((distance2-distance)*(distance3-distance2)*(distance4-distance3)*(distance5-distance4)/Math.Pow((distance5-distance4),4),1.0D/i) +
		Math.Pow((distance2-distance)*(distance3-distance2)*(distance4-distance3)*(distance5-distance4)/Math.Pow((distance4-distance3),4),1.0D/i) +
		Math.Pow((distance2-distance)*(distance3-distance2)*(distance4-distance3)*(distance5-distance4)/Math.Pow((distance3-distance2),4),1.0D/i) +
		Math.Pow((distance2-distance)*(distance3-distance2)*(distance4-distance3)*(distance5-distance4)/Math.Pow((distance2-distance ),4),1.0D/i)
		)/Math.Pow(4.0D,1.0D/i);	

	distBuffer[x*size_y+y] = distBuffer[x*size_y+y] / 3.0D;  
	////////////////output1 png
	break;

        case cellul_tech.ct_diffntl2_nrst_quad:
	////////////////output5 png glowing edges
	distBuffer[x*size_y+y] =  
		Math.Pow((distance3-distance),2)/Math.Pow((distance2-distance),2) -
		Math.Pow((distance3-distance2),2)/Math.Pow((distance2-distance),2);
	////////////////output5 png
	break;



        case cellul_tech.ct_folliage:
	////////////////output6 png folliage
		distBuffer[x*size_y+y] =  
		 ((distance4-distance )/3.0D + (distance3-distance )/2.0D+(distance2-distance))/(1.0D/3.0D+1.0D/2.0D+1.0D);
	////////////////output6 png folliage
	break;

	////////////////output6 png white folliage
	//distBuffer[x*size_y+y] =  (distance4+distance3+distance2+distance)/4;
	////////////////output6 png white folliage

	////////////////output7 png peggy
	//distBuffer[x*size_y+y] =  distance/(2.0D) - distance2/(2.0D*3.0D) + distance3/(2.0D*3.0D*4.0D) - distance4/(2.0D*3.0D*4.0D*5.0D) + distance5/(2.0D*3.0D*4.0D*5.0D*6.0D);
	////////////////output7 png

	////////////////output8 black blurred dotts on white
	//double k8 = 1.5D; //(2.5D(lenta metaparticles)..5.0D(kapli)..25.0D(dots)...50.0D(microdot))
	//distBuffer[x*size_y+y] =   min(k8*distance, (distance/(2.0D) + distance2/(2.0D*3.0D) + distance3/(2.0D*3.0D*4.0D) + distance4/(2.0D*3.0D*4.0D*5.0D) + distance5*(2.0D*3.0D*4.0D*5.0D*6.0D)) /(distance/(2.0D*3.0D*4.0D*5.0D*6.0D)*5.0D) );
	////////////////output8 black blurred dotts on white


	////////////////output9 zwezdnaja klaksa
	//double k9 =3.0D; //1-brightest 6.0 darkest
	//distBuffer[x*size_y+y] =  (distance*(2.0D) + distance2*(2.0D*3.0D) + distance3*(2.0D*3.0D*4.0D) + distance4*(2.0D*3.0D*4.0D*5.0D) + distance5*(2.0D*3.0D*4.0D*5.0D*6.0D)) /(distance*(2.0D*3.0D*4.0D*5.0D*6.0D)*k9);
	////////////////output9  zwezdnaja klaksa

	////////////////output10 chemistry framewaork
	//double k10 =12.5D; //5-brightest 25.0 darkest
	//distBuffer[x*size_y+y] =  (distance + distance2 + distance3 + distance4 + distance5) / k10;
	////////////////output10 chemistry framewaork

	////////////////output11 chemistry framewaork 2
	//distBuffer[x*size_y+y] = (distance2-distance)+(distance4-distance3);
	////////////////output11 chemistry framewaork 2

	////////////////output12 soap
	//double k12 =1.0D/0.52D;
	//double ik12 =0.5;
	//distBuffer[x*size_y+y] = pow( ( pow(distance2/(distance2-distance),ik12) + pow(distance3/(distance3-distance2),ik12)+pow(distance4/(distance4-distance3),ik12)+pow(distance5/(distance5-distance4),ik12) ),k12 );
	////////////////output12 soap



	////////////////output13
	//distBuffer[x*size_y+y] = (distance+distance2+distance3+distance4)/abs(distance-distance2+distance3-distance4);
	////////////////output13

	//distBuffer[x*size_y+y] =	(distance2/(distance5-distance4) + distance3/(distance5-distance4) + distance4/(distance5-distance4) +distance5/(distance5-distance4) + distance/(distance5-distance4));
	//distBuffer[x*size_y+y] = (distance5-distance3)*(distance3-distance)*(distance4-distance2)/pow((distance2-distance),3.0f);

	////////////////output
	//	 distBuffer[x*size_y+y] +=  
	//     pow((distance2-0.25f*distance),4)/pow((distance5-2.25f*distance),2) -
   	//	 pow((distance2-2.25f*distance),4)/pow((distance3-2.0f*distance),2)/(4.0f);
	////////////////output



	////////////////output14
	////	float k=32.0f;	float pwr=6.00f; float stp=2.0f;//zrapana powerhnost
	//	float k=32.0f;	float pwr=6.00f; float stp=13.0f;//zrapana powerhnost
//		distBuffer[x*size_y+y] +=  
//		pow(( pow((distance2-2.0f*distance),stp)/pow((distance3-2.0f*distance),stp) +
//			  pow((distance2-2.0f*distance),stp)/pow((distance3-2.0f*distance),stp)/k),pwr);
	////////////////output14


};

    if (distance < mindist) mindist = distance;
    if (distance > maxdist) maxdist = distance;
 }      


for( int x=0; x<size_x; x++)
 for( int y=0; y<size_y; y++)
	set_arr(x,y, (distBuffer[x*size_y+y]-mindist)/(maxdist-mindist));



	}




    public double CloudExpCurve(double v)
	{
		const double CloudCover = 22000.0D;//0 .. 255 give total cloud cover and empty sky respecitvely.

		//0 .. 1.0f  controls how fuzzy or sharp the clouds are.
		//Lower values give sharper, denser clouds, and values closer to 1.0 give fuzzier, thinner clouds.
		//Do not use values any greater than 1.0.
		const double CloudSharpness = 0.99992095D;

      int c = (int) (v - CloudCover);
      if (c < 0)  c=0;
 
      double CloudDensity = 65535.0D - (Math.Pow(CloudSharpness,c) * 65535.0D);

      return CloudDensity;
	}


	public void generate_Perlin()//Perlin
	{
        //const int PSize = 512; // çäåñü ñòàâèøü ñâîé ðàçìåð//256
        //double Noise[PSize][PSize];// = new double[PSize][PSize];
		double[] distBuffer = new double[size_x*size_y];

				//srand(19150103);//134
		            for (int y = 0; y < size_y; y++)
                    for (int x = 0; x < size_x; x++) distBuffer[x*size_y+y]=0.0D;//1-(rand() % 1000) *0.001D;
					
					int PSize = Math.Min(size_x,size_y);

		            int d = PSize >> 1;
            while (true)
            {
                for (int y = 0; y < PSize; y += (d + d))
                {
                    for (int x = 0; x < PSize; x += (d + d))
                    {
					//distBuffer[x*size_y+y]
                        distBuffer[((x + d) % (PSize - 1)) * size_y + y] = (distBuffer[x * size_y + y] + distBuffer[((x + d + d) % (PSize - 1)) * size_y + y]) * 0.5f + (double)d * (0.001D * (double)(mw.mw_opt.rand.NextDouble() * (1000D)) - 0.5D);
                        distBuffer[x * size_y + ((y + d) % (PSize - 1))] = (distBuffer[x * size_y + y] + distBuffer[x * size_y + ((y + d + d) % (PSize - 1))]) * 0.5D + (double)d * (0.001D * (double)(mw.mw_opt.rand.NextDouble() * (1000D)) - 0.5D);
                        distBuffer[ ((x + d) % (PSize - 1))*size_y+((y + d) % (PSize - 1))] = 
							(distBuffer[x*size_y+ y] + distBuffer[((x + d + d) % (PSize - 1))*size_y+( (y + d + d) % (PSize - 1))] +
							 distBuffer[x*size_y+( (y + d + d) % (PSize - 1))] +
							 distBuffer[((x + d + d) % (PSize - 1))*size_y+y]) * 0.25D +
                             (double)d * (0.001D * (double)(mw.mw_opt.rand.NextDouble() * (1000D)) - 0.5D);
                    }
                }

                if (d <= 1) break;

                d = d >> 1;
            }


        distBuffer.CopyTo(arr,0);
        /*
 for( int x=0; x<size_x; x++)
 for( int y=0; y<size_y; y++) //set_arr(x,y, distBuffer[x*size_y+y]);
    //set_arr(x,y, CloudExpCurve( distBuffer[x*size_y+y] * 65535.0f )  / 65535.0f );
	set_arr(x,y, distBuffer[x*size_y+y]);
        */

		return;
	}



    public void generate_Cloud()
	{

 generate_cellular();
 for( int x=0; x<size_x; x++)
 for( int y=0; y<size_y; y++)
  set_arr(x,y, CloudExpCurve( (get_arr(x,y) * 65535.0D ))  / 65535.0D );

	}

    /// <summary>
    /// simple blur - blurring image with depth as parameter
    /// </summary>
	public void simple_blur_(int depth)
	{
// double px, double py
//double finx = 0;
//double ffrx = modf(px,&finx);

//double finy = 0;
//double ffry = modf(py,&finy);
		double v,w;
		procedural temp = new procedural(size_x,size_y);


		 for( int x=0; x<size_x; x++)
		 for( int y=0; y<size_y; y++)
			{

		 v=0.0D;//get_arr(x,y);//
		 w=0.0D;
		 for( int jx=0; jx<depth; jx++){
			 
			 for( int jy=0; jy<depth; jy++) {
		       v += get_arr( (x-jx)>0 ? (x-jx) :             x , (y-jy)>0 ? (y-jy) : y );
		  //   v += get_arr( (x-jx)>0 ? (x-jx) : (x-jx)+size_x , (y-jy)>0 ? (y-jy) : (y-jy)+size_y);
											}
			 w += v / (double)depth; v=0.0D;
		 }
			//set_arr(x,y, v /(depth*depth-1) );
			//temp->set_arr(x,y, v /(double)(depth*depth-1.0D) );
			temp.set_arr(x,y, w / ((double)depth) );
			}

		 		 for( int y=0; y<size_y; y++)
				//for( int x=0; x<size_x; x++) set_arr(x,y,abs(temp->get_arr(x,y))>256.0D ? abs(256.0D / temp->get_arr(x,y)) : (temp->get_arr(x,y) ) );
				// for( int x=0; x<size_x; x++) set_arr(x,y,(temp->get_arr(x,y))>1.0D ? 1.0D / temp->get_arr(x,y) : temp->get_arr(x,y) );

				//for( int x=0; x<size_x; x++) set_arr(x,y,temp->get_arr(x,y)>1.0D ? temp->get_arr(x,y) - (double)((int)temp->get_arr(x,y)) : temp->get_arr(x,y));
				
				for( int x=0; x<size_x; x++) {
					
					set_arr(x,y,Math.Min(256.0D,Math.Abs(temp.get_arr(x,y))) );

					//2// w = Math.Abs(temp.get_arr(x,y));
					//2// set_arr(x,y, (w>1.0D) ? 1.0D / w : w );
				}
				
	}

	/* bad idea from mikle flamemaker
	void generate_()
	{

double* distBuffer = new double[size_x*size_y];

int ia1,ia2,ia3,ia4,ia5,ia6,ia7;;

double temp;
double x0, y0;
double x1, y1;
double x2, y2;

double i;
double Sz =128;
double Kf;


int ix, iy;

for( int x=0; x<size_x; x++)
 for( int y=0; y<size_y; y++)
	distBuffer[x*size_y+y] = 0.0D;


  for(i = 7; i>= 1.0D; i-= 1.0D)
  {
    Kf = pow(1.4D, i);
	for( y0 = 0; y0<= size_y; y0+= Sz)
	for( x0 = 0; x0<= size_x; x0+= Sz)
	{

        x1 = x0 + Sz / 2.0D;
        y1 = y0 + Sz / 2.0D;

        //x2 = (x0 + Sz) - 127.0D*floorf((x0 + Sz) / 127.0D);
        //y2 = (y0 + Sz) - 127.0D*floorf((y0 + Sz) / 127.0D);
		x2 = modff((x0 + Sz), &temp);
		y2 = modff((y0 + Sz), &temp);

		ia1 = x1*size_y + y0;
		ia2 = x0*size_y + y1;
		ia3 = x1*size_y + y1;
		ia4 = x0*size_y + y0;
		ia5 = x2*size_y + y0;
		ia6 = x0*size_y + y2;
		ia7 = x2*size_y + y2;

        distBuffer[ia1] = Kf * ((rand()/ (RAND_MAX + 1)) - 0.5D) + (0 + distBuffer[ia4] + distBuffer[ia5]) * 0.5D;
        distBuffer[ia2] = Kf * ((rand()/ (RAND_MAX + 1)) - 0.5D) + (0 + distBuffer[ia4] + distBuffer[ia6]) * 0.5D;
        distBuffer[ia3] = Kf * ((rand()/ (RAND_MAX + 1)) - 0.5D) + (0 + distBuffer[ia4] + distBuffer[ia6] + distBuffer[ia5] + distBuffer[ia7]) * 0.25D;
	}

    Sz = Sz / 2;
  }

  Kf = 0;

	for( y0 = 0; y0<= size_y; y0+= Sz)
	for( x0 = 0; x0<= size_x; x0+= Sz) {
		ia4 = x0*size_y + y0;
      if (Kf < Math.Abs(distBuffer[ia4]))  Kf = Math.Abs(distBuffer[ia4]);
	}

  Kf = size_x / Kf;


	for( y0 = 0; y0<= size_x; y0+= Sz)
	for( x0 = 0; x0<= size_x; x0+= Sz){
		ia4 = x0*size_y + y0;
		distBuffer[ia4] = (distBuffer[ia4]*Kf + size_x)*0x10000;//21.0D;//0x10101;			
	}

	//float t2,alf;
for( int x=0; x<size_x; x++)
 for( int y=0; y<size_y; y++) {
	 / *
	 t2=hypotf((double)x,(double)y);
	 alf=atanf( (y==0) ? 999999.9f : (double)x / (double)y );

	 set_arr(
		 110.0D+t2*cos(alf+3.14D/4.0D),
		 203.0D+t2*sin(alf+3.14D/4.0D),
		 modff( distBuffer[x*size_y+y]/256.0D, &temp) );* /
	set_arr(0.0D+x,0.0D+y, modff( distBuffer[x*size_y+y]/size_x, &temp) );
 }


delete[] distBuffer; distBuffer=NULL;
return;
	}
 */
/* bad idea from mikle flamemaker*/


	public procedural(int psize_x, int psize_y) 
	{
        size_x=psize_x; size_y=psize_y;
		arr = new double[size_x*size_y];
	}

    }




    //////////////////////////////////////////////////



    /// <summary>
    /// the first material from 4 textures (3 perlin + 1 cellular ) and two technique of it compositions
    /// </summary>
    /// <remarks>keept in self own result array of bitmap pixels.</remarks>
public class cl_mtrl_witrag {

    public cl_mtrl_witrag(int psize_x, int psize_y)
    {
        size_x = psize_x;
        size_y = psize_y;

        arr = new double[3 * psize_x * psize_y];


        txtr = null;
        txtr1 = null;
        txtr2 = null;
        txtrCellul = null;

    }

    /*? біла просто функция */public amk_rgb[] material_witrag(int psize_x, int psize_y) { return new amk_rgb[3]; }
    /*? біла просто функция */public amk_rgb[] material_crstll_marble_sky(int psize_x, int psize_y) { return new amk_rgb[3]; }


    public amk_rgb[] arr;
    public int size_x, size_y;

    public procedural txtr;
    public procedural txtr1;
    public procedural txtr2;
    public procedural txtrCellul;

    public amk_rgb[] material_witrag(bool pbLegacy = false)
{
	int psize_x = size_x;
	int psize_y = size_y;
	
	int PSize = Math.Min(psize_x, psize_y);

	if (!pbLegacy) {

		txtr = new procedural((int)(PSize+mw.mw_opt.deltaTxtrSize), (int)(PSize+mw.mw_opt.deltaTxtrSize) );
		txtr.generate_Perlin();
		if (mw.mw_opt.beforeBlur0>0) txtr.simple_blur_(mw.mw_opt.beforeBlur0);//10
		for(int i=0; i<mw.mw_opt.itrBlur0; i++) txtr.simple_blur_(mw.mw_opt.amountBlur0);//8

		txtr1 = new procedural((int)(PSize+mw.mw_opt.deltaTxtr1Size),(int)(PSize+mw.mw_opt.deltaTxtr1Size));
		txtr1.generate_Perlin();
		if (mw.mw_opt.beforeBlur1>0) txtr1.simple_blur_(mw.mw_opt.beforeBlur1);//10
		for(int i=0; i<mw.mw_opt.itrBlur1; i++) txtr1.simple_blur_(mw.mw_opt.amountBlur1);//8

		txtr2 = new procedural((int)(PSize+mw.mw_opt.deltaTxtr2Size), (int)(PSize+mw.mw_opt.deltaTxtr2Size));
		txtr2.generate_Perlin();
		if (mw.mw_opt.beforeBlur2>0) txtr2.simple_blur_(mw.mw_opt.beforeBlur2);//10
		for(int i=0; i<mw.mw_opt.itrBlur2; i++) txtr2.simple_blur_(mw.mw_opt.amountBlur2);//8

		txtrCellul = new procedural(PSize,PSize);
		txtrCellul.generate_cellular(mw.mw_opt.txtrCellTechniq);		
		if (mw.mw_opt.beforeBlur>0) txtrCellul.simple_blur_(mw.mw_opt.beforeBlur);		
		for(int i=0; i<mw.mw_opt.itrBlur; i++) txtrCellul.simple_blur_(mw.mw_opt.amountBlur);//8
				} else {
					//here code where txtr,txtr1,txtr2,txtrCellul - Moved, Scaled and Rotated
						}

		//where 0-txtr, 1-txtr1, 2-txtr2, txtrCellul;
    double[] ttxtr = { 0.0D, 0.0D, 0.0D, 0.0D };

                for (int y = 0; y < PSize; y += 1)
                    for (int x = 0; x < PSize; x += 1) {

						ttxtr[0] = txtr.get_arr(x,y);
						ttxtr[1] = txtr1.get_arr(x,y);
						ttxtr[2] = txtr2.get_arr(x,y);
						ttxtr[3] = txtrCellul.get_arr(x,y);
		
						
						switch(mw.mw_opt.mtrl1MatTecniq)
                        {

                            #region mtrl_1_witrage
                            case mtrl_1_tech.mtrl_1_witrage:
arr[3*(x*psize_y+y) + 0] =
//arr[3*(x*psize_y+y)+0] =
//1.0f-
mw.mw_opt.shftMainCh0+mw.mw_opt.negMainCh0*Math.Pow(
					(txtrCellul.get_arr(x,y)*
					0.03D*(txtr2.get_arr(x,y)*txtr.get_arr(x,y)+0.75D*txtr1.get_arr(x,y)+0.33D*txtr2.get_arr(x,y))/2.0D
					)/2.00D,mw.mw_opt.pwrMainCh0*mw.mw_opt.invMainCh0);

arr[3*(x*psize_y+y)+1] =
//arr[3*(x*psize_y+y)+1] =
mw.mw_opt.shftMainCh1+mw.mw_opt.negMainCh1*Math.Pow(
					(txtrCellul.get_arr(x,y)*
					0.010D*(txtr2.get_arr(x,y)+0.75D*txtr.get_arr(x,y)*txtr1.get_arr(x,y)+0.33D*txtr2.get_arr(x,y))/2.0D
					)/2.00D,mw.mw_opt.pwrMainCh1*mw.mw_opt.invMainCh1);

arr[3*(x*psize_y+y)+2] =
//arr[3*(x*psize_y+y)+2] =
mw.mw_opt.shftMainCh2+mw.mw_opt.negMainCh2*Math.Pow(
					(txtrCellul.get_arr(x,y)*
					0.030D*(txtr2.get_arr(x,y)+0.75D*txtr1.get_arr(x,y)+0.33D*txtr.get_arr(x,y)*txtr2.get_arr(x,y))/2.0D
					)/2.00D,mw.mw_opt.pwrMainCh2*mw.mw_opt.invMainCh2);
								break;
                            #endregion


                            #region mtrl_1_crstll_marble_sky
                            case mtrl_1_tech.mtrl_1_crstll_marble_sky:

arr[3*(x*psize_y+y)+0] =

//0.06f/
//1.0f/ -
//2//1.0f -
mw.mw_opt.shftMainCh0+mw.mw_opt.negMainCh0*
					Math.Pow(
					 //( txtrCellul ->get_arr(x,y) < 0.1f ? (rand() % 10)/10.0f : txtrCellul ->get_arr(x,y) ) *
					 // txtrCellul ->get_arr(x,y)*
					 ttxtr[3]*
					(ttxtr[mw.mw_opt.mtrxMainCh[0,0,0]]*ttxtr[mw.mw_opt.mtrxMainCh[0,0,1]]+
					 ttxtr[mw.mw_opt.mtrxMainCh[0,1,0]]*ttxtr[mw.mw_opt.mtrxMainCh[0,1,1]]+
					 ttxtr[mw.mw_opt.mtrxMainCh[0,2,0]]*ttxtr[mw.mw_opt.mtrxMainCh[0,2,1]]
					// txtr2 ->get_arr(x,y)*txtr ->get_arr(x,y)+
					// txtr1 ->get_arr(x,y)*txtr1 ->get_arr(x,y)+
					// txtr2 ->get_arr(x,y)*txtr2 ->get_arr(x,y)
					)/3.0D
					,mw.mw_opt.pwrMainCh0*mw.mw_opt.invMainCh0);////, 0.1f);

arr[3*(x*psize_y+y)+1] =
//0.03f/
//2//1.0f -
mw.mw_opt.shftMainCh1+mw.mw_opt.negMainCh1*
					Math.Pow(
					 //( txtrCellul ->get_arr(x,y) < 0.1f ? (rand() % 10)/10.0f : txtrCellul ->get_arr(x,y) ) *
					// txtrCellul ->get_arr(x,y)*
					ttxtr[3]*
                    (ttxtr[mw.mw_opt.mtrxMainCh[1,0,0]] * ttxtr[mw.mw_opt.mtrxMainCh[1,0,1]] +
                     ttxtr[mw.mw_opt.mtrxMainCh[1,1,0]] * ttxtr[mw.mw_opt.mtrxMainCh[1,1,1]] +
                     ttxtr[mw.mw_opt.mtrxMainCh[1,2,0]] * ttxtr[mw.mw_opt.mtrxMainCh[1,2,1]]

					//txtr2 ->get_arr(x,y)*txtr2 ->get_arr(x,y)+
					// txtr ->get_arr(x,y)*txtr1 ->get_arr(x,y)+
					//txtr2 ->get_arr(x,y)*txtr2 ->get_arr(x,y)
					)/3.0D
					,mw.mw_opt.pwrMainCh1*mw.mw_opt.invMainCh1);////, 0.0001f);

arr[3*(x*psize_y+y)+2] =
//0.01f/
mw.mw_opt.shftMainCh2+mw.mw_opt.negMainCh2*
					Math.Pow(
					// ( txtrCellul ->get_arr(x,y) < 0.1f ? (rand() % 10)/10.0f : txtrCellul ->get_arr(x,y) ) *
					// txtrCellul ->get_arr(x,y)*
					ttxtr[3]*
                    (ttxtr[mw.mw_opt.mtrxMainCh[2,0,0]] * ttxtr[mw.mw_opt.mtrxMainCh[2,0,1]] +
                     ttxtr[mw.mw_opt.mtrxMainCh[2,1,0]] * ttxtr[mw.mw_opt.mtrxMainCh[2,1,1]] +
                     ttxtr[mw.mw_opt.mtrxMainCh[2,2,0]] * ttxtr[mw.mw_opt.mtrxMainCh[2,2,1]]

					//txtr2 ->get_arr(x,y)*txtr2 ->get_arr(x,y)+
					//txtr1 ->get_arr(x,y)*txtr1 ->get_arr(x,y)+
					//txtr ->get_arr(x,y)*txtr2 ->get_arr(x,y)
					)/3.0D
					,mw.mw_opt.pwrMainCh2*mw.mw_opt.invMainCh2);////, 0.0001f);
break;
    #endregion
					}
					}


return arr;
}


}





//////////////////////////////////////////////////






}
