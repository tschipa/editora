// greng.cpp : Defines the exported functions for the DLL application.
//

#include "stdafx.h"
#include "greng.h"

#define _UNICODE

#include <tchar.h>
#include <wchar.h>
#include <string.h>




//author: Booster:Igor Spiridonov
//email: igwasm@rambler.ru
//record white noise video
#include <windows.h>
#include <vfw.h>
#include <math.h>
#include <stdio.h>
#include <limits.h>


#include "procedural.h"

#pragma comment (lib, "vfw32")


bool ChooseCodec(HWND hWnd, AVICOMPRESSOPTIONS *pOptionsVideo);
bool CreateVideoStream(DWORD dwFrameRate, size_t WidthFrame, size_t HeightFrame, PAVISTREAM *ppStreamVideo);
bool MakeCompressedVideoStream(size_t WidthFrame, size_t HeightFrame, AVICOMPRESSOPTIONS *pVideoOptions, PAVISTREAM pStreamVideo, PAVISTREAM *ppCompressedStreamVideo);
bool CreateAudioStream(const WAVEFORMATEX *pwf, PAVIFILE pAviFile, PAVISTREAM *ppStreamAudio);
bool WriteFrameVideoCompress(PAVISTREAM pCompressedStreamVideo, BITMAPINFO *pBmp, DWORD dwFrameNum);
bool WriteFrameAudio(PAVISTREAM  pStreamAudio, void *pData, size_t sizeData, DWORD dwFrameNum);
void AviClean();

AVICOMPRESSOPTIONS OptionsVideo;
PAVIFILE pAviFile = 0;
PAVISTREAM pStreamVideo = 0;
PAVISTREAM pCompressedStreamVideo = 0;
PAVISTREAM pStreamAudio = 0;


struct tag_a_param
{
	int WidthFrame = 256;
	int HeightFrame = 256;
	int FrameRate = 30;
	int CountSecond = 5;
	int CountFrames = FrameRate * CountSecond;
	int AmplitudeSound = 0x4000;
	wchar_t OutAviFileName[4096];

} a_param;


GRENG_API void mw_opt_initialize(int x)
{
	mw_opt.initialize();
};


GRENG_API int  wfpamkparam(int w_render_tech, int w_sizex, int w_sizey, int w_sec, LPWSTR w_OutAviFileName)
{

	a_param.WidthFrame = w_sizex;
	a_param.HeightFrame = w_sizey;
	mw_opt.iSize = min(a_param.WidthFrame, a_param.HeightFrame);

	a_param.CountSecond = w_sec;
	a_param.CountFrames = a_param.FrameRate * a_param.CountSecond;

	

		rsize_t len = wcslen(w_OutAviFileName);
		wsprintf(a_param.OutAviFileName, L"%s", w_OutAviFileName);
		mw_opt.mtrl1MatTecniq = (mtrl_1_tech)w_render_tech;

	return 0;
};

GRENG_API int  wfpamkparamchannel(
	double w_invMainCh0,
	double w_negMainCh0,
	double w_shftMainCh0,
	double w_pwrMainCh0,
	double w_deltaTxtr0Size,

	double w_invMainCh1,
	double w_negMainCh1,
	double w_shftMainCh1,
	double w_pwrMainCh1,
	double w_deltaTxtr1Size,

	double w_invMainCh2,
	double w_negMainCh2,
	double w_shftMainCh2,
	double w_pwrMainCh2,
	double w_deltaTxtr2Size)

{


	mw_opt.invMainCh0 = w_invMainCh0;
	mw_opt.negMainCh0 = w_negMainCh0;
	mw_opt.shftMainCh0 = w_shftMainCh0;
	mw_opt.pwrMainCh0 = w_pwrMainCh0;
	mw_opt.deltaTxtrSize = w_deltaTxtr0Size;

	mw_opt.invMainCh1 = w_invMainCh1;
	mw_opt.negMainCh1 = w_negMainCh1;
	mw_opt.shftMainCh1 = w_shftMainCh1;
	mw_opt.pwrMainCh1 = w_pwrMainCh1;
	mw_opt.deltaTxtr1Size = w_deltaTxtr1Size;

	mw_opt.invMainCh2 = w_invMainCh2;
	mw_opt.negMainCh2 = w_negMainCh2;
	mw_opt.shftMainCh2 = w_shftMainCh2;
	mw_opt.pwrMainCh2 = w_pwrMainCh2;
	mw_opt.deltaTxtr2Size = w_deltaTxtr2Size;


	return 0;
};

GRENG_API int  wfpamkparamblur(
	//Blur
	int w_itrBlur,     //number of blur iterations of amountBlur pixels each
	int w_amountBlur,  //amount of pixels in blur
	int w_beforeBlur,  //amount of pixels in blur before iterated blur
	//Blur0
	int w_itrBlur0,     //number of blur iterations of amountBlur pixels each
	int w_amountBlur0,  //amount of pixels in blur
	int w_beforeBlur0,  //amount of pixels in blur before iterated blur
	//Blur1
	int w_itrBlur1,     //number of blur iterations of amountBlur pixels each
	int w_amountBlur1,  //amount of pixels in blur
	int w_beforeBlur1,  //amount of pixels in blur before iterated blur
	//Blur2
	int w_itrBlur2,     //number of blur iterations of amountBlur pixels each
	int w_amountBlur2,  //amount of pixels in blur
	int w_beforeBlur2  //amount of pixels in blur before iterated blur
	)
{
	mw_opt.itrBlur = w_itrBlur;
	mw_opt.amountBlur = w_amountBlur;
	mw_opt.beforeBlur = w_beforeBlur;

	mw_opt.itrBlur0 = w_itrBlur0;
	mw_opt.amountBlur0 = w_amountBlur0;
	mw_opt.beforeBlur0 = w_beforeBlur0;

	mw_opt.itrBlur1 = w_itrBlur1;
	mw_opt.amountBlur1 = w_amountBlur1;
	mw_opt.beforeBlur1 = w_beforeBlur1;

	mw_opt.itrBlur2 = w_itrBlur2;
	mw_opt.amountBlur2 = w_amountBlur2;
	mw_opt.beforeBlur2 = w_beforeBlur2;

	return 0;
};



GRENG_API int  wfpamkparammtrxmainch(
	//e1
	int el000,
	int el001,

	int el010,
	int el011,

	int el020,
	int el021,
	//e2
	int el100,
	int el101,

	int el110,
	int el111,

	int el120,
	int el121,
	//e3
	int el200,
	int el201,

	int el210,
	int el211,

	int el220,
	int el221
	)
{
	//e1
	mw_opt.mtrxMainCh[0][0][0] = el000;
	mw_opt.mtrxMainCh[0][0][1] = el001;

	mw_opt.mtrxMainCh[0][1][0] = el010;
	mw_opt.mtrxMainCh[0][1][1] = el011;

	mw_opt.mtrxMainCh[0][2][0] = el020;
	mw_opt.mtrxMainCh[0][2][1] = el021;

	//e2
	mw_opt.mtrxMainCh[1][0][0] = el100;
	mw_opt.mtrxMainCh[1][0][1] = el101;

	mw_opt.mtrxMainCh[1][1][0] = el110;
	mw_opt.mtrxMainCh[1][1][1] = el111;

	mw_opt.mtrxMainCh[1][2][0] = el120;
	mw_opt.mtrxMainCh[1][2][1] = el121;

	//e3
	mw_opt.mtrxMainCh[2][0][0] = el200;
	mw_opt.mtrxMainCh[2][0][1] = el201;

	mw_opt.mtrxMainCh[2][1][0] = el210;
	mw_opt.mtrxMainCh[2][1][1] = el211;

	mw_opt.mtrxMainCh[2][2][0] = el220;
	mw_opt.mtrxMainCh[2][2][1] = el221;

	return 0;
};



GRENG_API INT32 djenerator(int x)
{	
	
	

	if (mw_opt.brandSeed)
		mw_opt.randSeed = (unsigned int)GetTickCount();
	else
		srand(mw_opt.randSeed);//134

	mt1 = new cl_mtrl_witrag(mw_opt.iSize, mw_opt.iSize);

	//////////////////////////////////////////////////////////////////////////////


	if (!ChooseCodec(0, &OptionsVideo))
		return 0;

	AVIFileInit();
	if (AVIFileOpen(&pAviFile, a_param.OutAviFileName, OF_WRITE | OF_CREATE, NULL) != AVIERR_OK)
		return 0;

	if (!CreateVideoStream(a_param.FrameRate, a_param.WidthFrame, a_param.HeightFrame, &pStreamVideo))
	{
		AviClean();
		return 0;
	}

	if (!MakeCompressedVideoStream(a_param.WidthFrame, a_param.HeightFrame, &OptionsVideo, pStreamVideo, &pCompressedStreamVideo))
	{
		AviClean();
		return 0;
	}

	WAVEFORMATEX wf;
	ZeroMemory(&wf, sizeof(WAVEFORMATEX));
	wf.cbSize = sizeof(WAVEFORMATEX);
	wf.wFormatTag = WAVE_FORMAT_PCM;
	wf.nChannels = 2;
	wf.nSamplesPerSec = 44100;
	wf.wBitsPerSample = 16;
	wf.nBlockAlign = (wf.wBitsPerSample * wf.nChannels) / 8;
	wf.nAvgBytesPerSec = wf.nSamplesPerSec * wf.nBlockAlign;

	if (!CreateAudioStream(&wf, pAviFile, &pStreamAudio))
	{
		AviClean();
		return 0;
	}

	const DWORD sizeVideoFrame = a_param.WidthFrame * a_param.HeightFrame * 3;
	void *pVideo = malloc(sizeVideoFrame + sizeof(BITMAPINFOHEADER));
	BITMAPINFOHEADER *pBitm = (BITMAPINFOHEADER*)pVideo;
	ZeroMemory(pBitm, sizeof(BITMAPINFOHEADER));
	pBitm->biWidth = a_param.WidthFrame;
	pBitm->biHeight = a_param.HeightFrame;
	pBitm->biBitCount = 24;
	pBitm->biPlanes = 1;
	pBitm->biSize = sizeof(BITMAPINFOHEADER);
	pBitm->biCompression = BI_RGB;
	pBitm->biSizeImage = sizeVideoFrame;

	char *pBits = (char*)pVideo + sizeof(BITMAPINFOHEADER);

	int CountSoundInFrame = wf.nAvgBytesPerSec / a_param.FrameRate;
	DWORD *pSound = (DWORD*)malloc(CountSoundInFrame);

	printf("Recording ");

	for (int i = 0; i<a_param.CountFrames; i++)
	{





		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		mw_opt.gen_new_opt();
		UINT sc_bitmapWidth = mw_opt.pixelate_x*mw_opt.iSize; //512;//2048;//(UINT) obj->m_pBackBufferRT->GetSize().width;
		UINT sc_bitmapHeight = mw_opt.pixelate_y*mw_opt.iSize; //512;//2048;//408;//(UINT) obj->m_pBackBufferRT->GetSize().height;//480;






		//amk_rgb* r = material_witrag(PSize, PSize);
		////cl_mtrl_witrag* mt1 = new cl_mtrl_witrag(mw_opt.iSize,mw_opt.iSize);
		mt1->material_witrag(((mw_opt.bAnimOn) && (mw_opt.idFN>0)));
		//2 amk_rgb* r = material_crstll_marble_sky(PSize, PSize);


		//Video frame
		for (int i2 = 0; i2<(a_param.WidthFrame * a_param.HeightFrame); i2++)
		{
			unsigned int color = (((float)rand()) / RAND_MAX) * 0xffffff;
			pBits[i2 * 3] = 255;//((char*)&color)[0];
			pBits[i2 * 3 + 1] = 255;//((char*)&color)[0];
			pBits[i2 * 3 + 2] = 255;//((char*)&color)[0];


		}
		///*

		for (int x = 0; x < mw_opt.iSize; x += 1)
		for (int y = 0; y < mw_opt.iSize; y += 1)
		{




			pBits[2 + 3 * (x*mw_opt.pixelate_x*a_param.HeightFrame + (mw_opt.iSize - y - 1)*mw_opt.pixelate_y)] = (char)min(255, (256 * mt1->arr[x*(mw_opt.iSize) + y][0]));//(256*(mt1->arr[x*(mw_opt.iSize)+y][0])/(2*INT_MAX+1) );
			pBits[1 + 3 * (x*mw_opt.pixelate_x*a_param.HeightFrame + (mw_opt.iSize - y - 1)*mw_opt.pixelate_y)] = (char)min(255, (256 * mt1->arr[x*(mw_opt.iSize) + y][1]));//(256*(mt1->arr[x*(mw_opt.iSize)+y][1])/(2*INT_MAX+1) );
			pBits[0 + 3 * (x*mw_opt.pixelate_x*a_param.HeightFrame + (mw_opt.iSize - y-1)*mw_opt.pixelate_y)] = (char)min(255,(256 * mt1->arr[x*(mw_opt.iSize) + y][2]));//(256*(mt1->arr[x*(mw_opt.iSize)+y][2])/(2*INT_MAX+1) );

		}
		//		*/

		mw_opt.generateFN();
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



		//Audio frame
		for (int i3 = 0; i3<CountSoundInFrame / 4; i3++)
		{
			unsigned short x = (((float)rand()) / RAND_MAX)*a_param.AmplitudeSound;
			unsigned short y = (((float)rand()) / RAND_MAX)*a_param.AmplitudeSound;
			((WORD*)pSound)[i3 * 2] = x;
			((WORD*)pSound)[i3 * 2 + 1] = y;
		}

		if (!WriteFrameVideoCompress(pCompressedStreamVideo, (BITMAPINFO*)pBitm, i))
			break;
		if (!WriteFrameAudio(pStreamAudio, pSound, CountSoundInFrame, i))
			break;

		if (!(i%a_param.FrameRate))
			printf(".");
	}
	free(pSound);
	free(pVideo);
	AviClean();
	return 0;
}


bool ChooseCodec(HWND hWnd, AVICOMPRESSOPTIONS *pOptionsVideo)
{
	COMPVARS cv;
	ZeroMemory(&cv, sizeof(cv));
	cv.cbSize = sizeof(cv);

	if (ICCompressorChoose(hWnd, ICMF_CHOOSE_DATARATE |
		ICMF_CHOOSE_KEYFRAME, NULL, NULL, &cv, ("����� � ��������� ������")))
	{
		ZeroMemory(pOptionsVideo, sizeof(AVICOMPRESSOPTIONS));

		pOptionsVideo->fccType = streamtypeVIDEO;
		pOptionsVideo->fccHandler = cv.fccHandler;

		if (cv.lKey)
		{
			pOptionsVideo->dwKeyFrameEvery = cv.lKey;
			pOptionsVideo->dwFlags |= AVICOMPRESSF_KEYFRAMES;
		}

		pOptionsVideo->dwQuality = cv.lQ;

		if (cv.lDataRate)
		{
			pOptionsVideo->dwFlags |= AVICOMPRESSF_DATARATE;
			pOptionsVideo->dwBytesPerSecond = cv.lDataRate * 1024;
		}

		ICCompressorFree(&cv);
		return true;
	}
	return false;
}

bool CreateVideoStream(DWORD dwFrameRate, size_t WidthFrame, size_t HeightFrame, PAVISTREAM *ppStreamVideo)
{
	AVISTREAMINFO sStreamInfo;
	ZeroMemory(&sStreamInfo, sizeof(sStreamInfo));
	sStreamInfo.fccType = streamtypeVIDEO;
	sStreamInfo.fccHandler = 0;
	sStreamInfo.dwScale = 1;
	sStreamInfo.dwRate = dwFrameRate;
	sStreamInfo.dwSuggestedBufferSize = 0;
	SetRect(&sStreamInfo.rcFrame, 0, 0, WidthFrame, HeightFrame);
	return (AVIERR_OK == AVIFileCreateStream(pAviFile, ppStreamVideo, &sStreamInfo));
}

bool MakeCompressedVideoStream(size_t WidthFrame, size_t HeightFrame, AVICOMPRESSOPTIONS *pVideoOptions,
	PAVISTREAM pStreamVideo, PAVISTREAM *ppCompressedStreamVideo)
{
	BITMAPINFO bi;
	ZeroMemory(&bi, sizeof(BITMAPINFO));
	bi.bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
	bi.bmiHeader.biWidth = WidthFrame;
	bi.bmiHeader.biHeight = HeightFrame;
	bi.bmiHeader.biSizeImage = WidthFrame * HeightFrame * 3;
	bi.bmiHeader.biPlanes = 1;
	bi.bmiHeader.biBitCount = 24;
	bi.bmiHeader.biCompression = BI_RGB;
	void *pBits = ((LPBYTE)&bi) + bi.bmiHeader.biSize + bi.bmiHeader.biClrUsed * sizeof(RGBQUAD);
	if (AVIERR_OK == AVIMakeCompressedStream(ppCompressedStreamVideo, pStreamVideo, pVideoOptions, NULL))
	{
		if (AVIERR_OK == AVIStreamSetFormat(*ppCompressedStreamVideo, 0, &bi, ((LPBYTE)pBits) - ((LPBYTE)&bi)))
			return true;

		AVIStreamRelease(*ppCompressedStreamVideo);
		pCompressedStreamVideo = 0;
	}

	bi.bmiHeader.biBitCount = 16;
	if (AVIERR_OK == AVIMakeCompressedStream(ppCompressedStreamVideo, pStreamVideo, pVideoOptions, NULL))
	{
		if (AVIERR_OK == AVIStreamSetFormat(*ppCompressedStreamVideo, 0, &bi, ((LPBYTE)pBits) - ((LPBYTE)&bi)))
			return true;

		AVIStreamRelease(*ppCompressedStreamVideo);
		pCompressedStreamVideo = 0;
	}

	bi.bmiHeader.biBitCount = 8;
	if (AVIERR_OK == AVIMakeCompressedStream(ppCompressedStreamVideo, pStreamVideo, pVideoOptions, NULL))
	{
		if (AVIERR_OK == AVIStreamSetFormat(*ppCompressedStreamVideo, 0, &bi, ((LPBYTE)pBits) - ((LPBYTE)&bi)))
			return true;

		AVIStreamRelease(*ppCompressedStreamVideo);
		pCompressedStreamVideo = 0;
	}

	return false;
}

bool CreateAudioStream(const WAVEFORMATEX *pwf, PAVIFILE pAviFile, PAVISTREAM *ppStreamAudio)
{
	AVISTREAMINFO sStreamInfo;
	ZeroMemory(&sStreamInfo, sizeof(sStreamInfo));
	sStreamInfo.fccType = streamtypeAUDIO;
	sStreamInfo.dwScale = pwf->nBlockAlign;
	sStreamInfo.dwRate = pwf->nSamplesPerSec * sStreamInfo.dwScale;
	sStreamInfo.dwInitialFrames = 1;
	sStreamInfo.dwSampleSize = pwf->nBlockAlign;
	sStreamInfo.dwQuality = (DWORD)-1;
	if (AVIFileCreateStream(pAviFile, ppStreamAudio, &sStreamInfo) != AVIERR_OK)
		return false;
	return (AVIERR_OK == AVIStreamSetFormat(*ppStreamAudio, 0, (void*)pwf, sizeof(WAVEFORMATEX)));
}

bool WriteFrameVideoCompress(PAVISTREAM pCompressedStreamVideo, BITMAPINFO *pBmp, DWORD dwFrameNum)
{
	return (AVIERR_OK == (AVIStreamWrite(pCompressedStreamVideo, dwFrameNum, 1,
		((LPBYTE)pBmp) + pBmp->bmiHeader.biSize + pBmp->bmiHeader.biClrUsed * sizeof(RGBQUAD),
		pBmp->bmiHeader.biSizeImage, 0, NULL, NULL)));
}

bool WriteFrameAudio(PAVISTREAM pStreamAudio, void *pData, size_t sizeData, DWORD dwFrameNum)
{
	return (AVIERR_OK == AVIStreamWrite(pStreamAudio, dwFrameNum,
		1, pData, sizeData, 0, NULL, NULL));
}

void AviClean()
{
	if (pStreamVideo)
	{
		AVIStreamRelease(pStreamVideo);
		pStreamVideo = 0;
	}

	if (pCompressedStreamVideo)
	{
		AVIStreamRelease(pCompressedStreamVideo);
		pCompressedStreamVideo = 0;
	}

	if (pStreamAudio)
	{
		AVIStreamRelease(pStreamAudio);
		pStreamAudio = 0;
	}

	if (pAviFile)
	{
		AVIFileRelease(pAviFile);
		pAviFile = 0;
	}

	AVIFileExit();
}
