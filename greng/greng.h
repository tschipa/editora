// The following ifdef block is the standard way of creating macros which make exporting 
// from a DLL simpler. All files within this DLL are compiled with the GRENG_EXPORTS
// symbol defined on the command line. This symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see 
// GRENG_API functions as being imported from a DLL, whereas this DLL sees symbols
// defined with this macro as being exported.
#ifdef GRENG_EXPORTS
#define GRENG_API __declspec(dllexport)
#else
#define GRENG_API __declspec(dllimport)
#endif



extern "C" GRENG_API int djenerator(int x);
extern "C" GRENG_API void mw_opt_initialize(int x);
extern "C" GRENG_API int wfpamkparam(int w_render_tech, int w_sizex, int w_sizey, int w_sec, LPWSTR w_OutAviFileName);
extern "C" GRENG_API int  wfpamkparamchannel(
	double w_invMainCh0,
	double w_negMainCh0,
	double w_shftMainCh0,
	double w_pwrMainCh0,
	double w_deltaTxtr0Size,

	double w_invMainCh1,
	double w_negMainCh1,
	double w_shftMainCh1,
	double w_pwrMainCh1,
	double w_deltaTxtr1Size,

	double w_invMainCh2,
	double w_negMainCh2,
	double w_shftMainCh2,
	double w_pwrMainCh2,
	double w_deltaTxtr2Size);

extern "C" GRENG_API int  wfpamkparamblur(
	//Blur
	int w_itrBlur,     //number of blur iterations of amountBlur pixels each
	int w_amountBlur,  //amount of pixels in blur
	int w_beforeBlur,  //amount of pixels in blur before iterated blur
	//Blur0
	int w_itrBlur0,     //number of blur iterations of amountBlur pixels each
	int w_amountBlur0,  //amount of pixels in blur
	int w_beforeBlur0,  //amount of pixels in blur before iterated blur
	//Blur1
	int w_itrBlur1,     //number of blur iterations of amountBlur pixels each
	int w_amountBlur1,  //amount of pixels in blur
	int w_beforeBlur1,  //amount of pixels in blur before iterated blur
	//Blur2
	int w_itrBlur2,     //number of blur iterations of amountBlur pixels each
	int w_amountBlur2,  //amount of pixels in blur
	int w_beforeBlur2  //amount of pixels in blur before iterated blur
	);

extern "C" GRENG_API int  wfpamkparammtrxmainch(
	//e1
	int el000,
	int el001,

	int el010,
	int el011,

	int el020,
	int el021,
	//e2
	int el100,
	int el101,

	int el110,
	int el111,

	int el120,
	int el121,
	//e3
	int el200,
	int el201,

	int el210,
	int el211,

	int el220,
	int el221
	);
