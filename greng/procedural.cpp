#include "StdAfx.h"
#include "procedural.h"

float mabs(float p)
{
	if (p < 0.0f) return -p;
	return p;
}


procedural::procedural(int psize_x, int psize_y) : size_x(psize_x), size_y(psize_y)
	{
		arr = new float[size_x*size_y];
		ZeroMemory(arr,size_x*size_y);
		//for (int i=0; i<size_x*size_y; i++) arr[i] = 0.0f;

	};
procedural::~procedural(void)
	{
		delete [] arr; arr=NULL;
	};

amk_options mw_opt;


amk_rgb* material_crstll_marble_sky(int psize_x, int psize_y)
{
	
	//srand(19150103);
			amk_rgb* arr = (amk_rgb*) new float[3*psize_x*psize_y];
			ZeroMemory((float*)arr,3*psize_x*psize_y);
			//for (int i=0; i<3*psize_x*psize_y; i++) arr[i] = 0.0f;
			int PSize = min(psize_x, psize_y);

		

		procedural* txtr = new procedural(PSize+mw_opt.deltaTxtrSize,PSize+mw_opt.deltaTxtrSize);
		txtr->generate_();
		if (mw_opt.beforeBlur0>0) txtr->simple_blur_(mw_opt.beforeBlur0);//10
		for(int i=0; i<mw_opt.itrBlur0; i++) txtr->simple_blur_(mw_opt.amountBlur0);//8


		procedural* txtr1 = new procedural(PSize+mw_opt.deltaTxtr1Size,PSize+mw_opt.deltaTxtr1Size);
		txtr1->generate_();
		if (mw_opt.beforeBlur1>0)txtr1->simple_blur_(mw_opt.beforeBlur1);//10
		for(int i=0; i<mw_opt.itrBlur1; i++) txtr1->simple_blur_(mw_opt.amountBlur1);//8


		procedural* txtr2 = new procedural(PSize+mw_opt.deltaTxtr2Size,PSize+mw_opt.deltaTxtr2Size);
		txtr2->generate_();
		if (mw_opt.beforeBlur2>0) txtr2->simple_blur_(mw_opt.beforeBlur2);//10
		for(int i=0; i<mw_opt.itrBlur2; i++) txtr2->simple_blur_(mw_opt.amountBlur2);//8

		procedural* txtrCellul = new procedural(PSize,PSize);
		txtrCellul->generate_cellular();		
		if (mw_opt.beforeBlur>0) txtrCellul->simple_blur_(mw_opt.beforeBlur);		
		for(int i=0; i<mw_opt.itrBlur; i++) txtrCellul->simple_blur_(mw_opt.amountBlur);//8
		//txtrCellul->simple_blur_(150);
		//txtrCellul->simple_blur_(5);		

                for (int y = 0; y < PSize; y += 1)
                    for (int x = 0; x < PSize; x += 1)
                    {

						/*
arr[x*psize_y+y][0] =
//arr[3*(x*psize_y+y)+0] =
//1.0f-
					(txtrCellul ->get_arr(x,y)*
					0.03f*(txtr2 ->get_arr(x,y)*txtr ->get_arr(x,y)+0.75f*txtr1 ->get_arr(x,y)+0.33f*txtr2 ->get_arr(x,y))/2.0f
					)/2.00f;

arr[x*psize_y+y][1] =
//arr[3*(x*psize_y+y)+1] =
					(txtrCellul ->get_arr(x,y)*
					0.010f*(txtr2 ->get_arr(x,y)+0.75f*txtr ->get_arr(x,y)*txtr1 ->get_arr(x,y)+0.33f*txtr2 ->get_arr(x,y))/2.0f
					)/2.00f;

arr[x*psize_y+y][2] =
//arr[3*(x*psize_y+y)+2] =
					(txtrCellul ->get_arr(x,y)*
					0.030f*(txtr2 ->get_arr(x,y)+0.75f*txtr1 ->get_arr(x,y)+0.33f*txtr ->get_arr(x,y)*txtr2 ->get_arr(x,y))/2.0f
					)/2.00f;
*/
arr[x*psize_y+y][0] =
//0.06f/
//1.0f/ -
//2//1.0f -
mw_opt.shftMainCh0+mw_opt.negMainCh0*
					pow(
					 //( txtrCellul ->get_arr(x,y) < 0.1f ? (rand() % 10)/10.0f : txtrCellul ->get_arr(x,y) ) *
					 txtrCellul ->get_arr(x,y)*
					(txtr2 ->get_arr(x,y)*txtr ->get_arr(x,y)+txtr1 ->get_arr(x,y)*txtr1 ->get_arr(x,y)+txtr2 ->get_arr(x,y)*txtr2 ->get_arr(x,y))/3.0f
					,mw_opt.pwrMainCh0*mw_opt.invMainCh0);////, 0.1f);

arr[x*psize_y+y][1] =
//0.03f/
//2//1.0f -
mw_opt.shftMainCh1+mw_opt.negMainCh1*
					pow(
					 //( txtrCellul ->get_arr(x,y) < 0.1f ? (rand() % 10)/10.0f : txtrCellul ->get_arr(x,y) ) *
					txtrCellul ->get_arr(x,y)*
					(txtr2 ->get_arr(x,y)*txtr2 ->get_arr(x,y)+txtr ->get_arr(x,y)*txtr1 ->get_arr(x,y)+txtr2 ->get_arr(x,y)*txtr2 ->get_arr(x,y))/3.0f
					,mw_opt.pwrMainCh1*mw_opt.invMainCh1);////, 0.0001f);

arr[x*psize_y+y][2] =
//0.01f/
mw_opt.shftMainCh2+mw_opt.negMainCh2*
					pow(
					txtrCellul ->get_arr(x,y)*
					// ( txtrCellul ->get_arr(x,y) < 0.1f ? (rand() % 10)/10.0f : txtrCellul ->get_arr(x,y) ) *
					(txtr2 ->get_arr(x,y)*txtr2 ->get_arr(x,y)+txtr1 ->get_arr(x,y)*txtr1 ->get_arr(x,y)+txtr ->get_arr(x,y)*txtr2 ->get_arr(x,y))/3.0f
					,mw_opt.pwrMainCh2*mw_opt.invMainCh2);////, 0.0001f);

					}

		delete txtr;
		delete txtr1;
		delete txtr2;
		delete txtrCellul;

return arr;
};
amk_rgb* material_witrag(int psize_x, int psize_y)
{
	
	//srand(19150103);
			amk_rgb* arr = (amk_rgb*) new float[3*psize_x*psize_y];
			ZeroMemory((float*)arr,3*psize_x*psize_y);
			//for (int i=0; i<3*psize_x*psize_y; i++) arr[i] = 0.0f;
			int PSize = min(psize_x, psize_y);

		

		procedural* txtr = new procedural(PSize+mw_opt.deltaTxtrSize,PSize+mw_opt.deltaTxtrSize);
		txtr->generate_();
		if (mw_opt.beforeBlur0>0) txtr->simple_blur_(mw_opt.beforeBlur0);//10
		for(int i=0; i<mw_opt.itrBlur0; i++) txtr->simple_blur_(mw_opt.amountBlur0);//8


		procedural* txtr1 = new procedural(PSize+mw_opt.deltaTxtr1Size,PSize+mw_opt.deltaTxtr1Size);
		txtr1->generate_();
		if (mw_opt.beforeBlur1>0)txtr1->simple_blur_(mw_opt.beforeBlur1);//10
		for(int i=0; i<mw_opt.itrBlur1; i++) txtr1->simple_blur_(mw_opt.amountBlur1);//8


		procedural* txtr2 = new procedural(PSize+mw_opt.deltaTxtr2Size,PSize+mw_opt.deltaTxtr2Size);
		txtr2->generate_();
		if (mw_opt.beforeBlur2>0) txtr2->simple_blur_(mw_opt.beforeBlur2);//10
		for(int i=0; i<mw_opt.itrBlur2; i++) txtr2->simple_blur_(mw_opt.amountBlur2);//8

		procedural* txtrCellul = new procedural(PSize,PSize);
		txtrCellul->generate_cellular(mw_opt.txtrCellTechniq);		
		if (mw_opt.beforeBlur>0) txtrCellul->simple_blur_(mw_opt.beforeBlur);		
		for(int i=0; i<mw_opt.itrBlur; i++) txtrCellul->simple_blur_(mw_opt.amountBlur);//8

		//where 0-txtr, 1-txtr1, 2-txtr2, txtrCellul;
		float ttxtr[4];

                for (int y = 0; y < PSize; y += 1)
                    for (int x = 0; x < PSize; x += 1) {

						ttxtr[0] = txtr  ->get_arr(x,y);
						ttxtr[1] = txtr1 ->get_arr(x,y);
						ttxtr[2] = txtr2 ->get_arr(x,y);
						ttxtr[3] = txtrCellul ->get_arr(x,y);
		
						
						switch(mw_opt.mtrl1MatTecniq) {

						case mtrl_1_tech::mtrl_1_witrage:
arr[x*psize_y+y][0] =
//arr[3*(x*psize_y+y)+0] =
//1.0f-
mw_opt.shftMainCh0+mw_opt.negMainCh0*pow(
					(txtrCellul ->get_arr(x,y)*
					0.03f*(txtr2 ->get_arr(x,y)*txtr ->get_arr(x,y)+0.75f*txtr1 ->get_arr(x,y)+0.33f*txtr2 ->get_arr(x,y))/2.0f
					)/2.00f,mw_opt.pwrMainCh0*mw_opt.invMainCh0);

arr[x*psize_y+y][1] =
//arr[3*(x*psize_y+y)+1] =
mw_opt.shftMainCh1+mw_opt.negMainCh1*pow(
					(txtrCellul ->get_arr(x,y)*
					0.010f*(txtr2 ->get_arr(x,y)+0.75f*txtr ->get_arr(x,y)*txtr1 ->get_arr(x,y)+0.33f*txtr2 ->get_arr(x,y))/2.0f
					)/2.00f,mw_opt.pwrMainCh1*mw_opt.invMainCh1);

arr[x*psize_y+y][2] =
//arr[3*(x*psize_y+y)+2] =
mw_opt.shftMainCh2+mw_opt.negMainCh2*pow(
					(txtrCellul ->get_arr(x,y)*
					0.030f*(txtr2 ->get_arr(x,y)+0.75f*txtr1 ->get_arr(x,y)+0.33f*txtr ->get_arr(x,y)*txtr2 ->get_arr(x,y))/2.0f
					)/2.00f,mw_opt.pwrMainCh2*mw_opt.invMainCh2);
								break;




						case mtrl_1_tech::mtrl_1_crstll_marble_sky:

arr[x*psize_y+y][0] =

//0.06f/
//1.0f/ -
//2//1.0f -
mw_opt.shftMainCh0+mw_opt.negMainCh0*
					pow(
					 //( txtrCellul ->get_arr(x,y) < 0.1f ? (rand() % 10)/10.0f : txtrCellul ->get_arr(x,y) ) *
					 // txtrCellul ->get_arr(x,y)*
					 ttxtr[3]*
					(ttxtr[mw_opt.mtrxMainCh[0][0][0]]*ttxtr[mw_opt.mtrxMainCh[0][0][1]]+
					 ttxtr[mw_opt.mtrxMainCh[1][0][0]]*ttxtr[mw_opt.mtrxMainCh[1][0][1]]+
					 ttxtr[mw_opt.mtrxMainCh[2][0][0]]*ttxtr[mw_opt.mtrxMainCh[2][0][1]]
					// txtr2 ->get_arr(x,y)*txtr ->get_arr(x,y)+
					// txtr1 ->get_arr(x,y)*txtr1 ->get_arr(x,y)+
					// txtr2 ->get_arr(x,y)*txtr2 ->get_arr(x,y)
					)/3.0f
					,mw_opt.pwrMainCh0*mw_opt.invMainCh0);////, 0.1f);

arr[x*psize_y+y][1] =
//0.03f/
//2//1.0f -
mw_opt.shftMainCh1+mw_opt.negMainCh1*
					pow(
					 //( txtrCellul ->get_arr(x,y) < 0.1f ? (rand() % 10)/10.0f : txtrCellul ->get_arr(x,y) ) *
					// txtrCellul ->get_arr(x,y)*
					ttxtr[3]*
					(ttxtr[mw_opt.mtrxMainCh[0][1][0]]*ttxtr[mw_opt.mtrxMainCh[0][1][1]]+
					 ttxtr[mw_opt.mtrxMainCh[1][1][0]]*ttxtr[mw_opt.mtrxMainCh[1][1][1]]+
					 ttxtr[mw_opt.mtrxMainCh[2][1][0]]*ttxtr[mw_opt.mtrxMainCh[2][1][1]]

					//txtr2 ->get_arr(x,y)*txtr2 ->get_arr(x,y)+
					// txtr ->get_arr(x,y)*txtr1 ->get_arr(x,y)+
					//txtr2 ->get_arr(x,y)*txtr2 ->get_arr(x,y)
					)/3.0f
					,mw_opt.pwrMainCh1*mw_opt.invMainCh1);////, 0.0001f);

arr[x*psize_y+y][2] =
//0.01f/
mw_opt.shftMainCh2+mw_opt.negMainCh2*
					pow(
					// ( txtrCellul ->get_arr(x,y) < 0.1f ? (rand() % 10)/10.0f : txtrCellul ->get_arr(x,y) ) *
					// txtrCellul ->get_arr(x,y)*
					ttxtr[3]*
					(ttxtr[mw_opt.mtrxMainCh[0][2][0]]*ttxtr[mw_opt.mtrxMainCh[0][2][1]]+
					 ttxtr[mw_opt.mtrxMainCh[1][2][0]]*ttxtr[mw_opt.mtrxMainCh[1][2][1]]+
					 ttxtr[mw_opt.mtrxMainCh[2][2][0]]*ttxtr[mw_opt.mtrxMainCh[2][2][1]]

					//txtr2 ->get_arr(x,y)*txtr2 ->get_arr(x,y)+
					//txtr1 ->get_arr(x,y)*txtr1 ->get_arr(x,y)+
					//txtr ->get_arr(x,y)*txtr2 ->get_arr(x,y)
					)/3.0f
					,mw_opt.pwrMainCh2*mw_opt.invMainCh2);////, 0.0001f);
break;

					}
					}

		delete txtr;
		delete txtr1;
		delete txtr2;
		delete txtrCellul;

return arr;
};


cl_mtrl_witrag::cl_mtrl_witrag(int psize_x, int psize_y) : size_x(psize_x), size_y(psize_y)
	{
			arr = (amk_rgb*) new float[3*psize_x*psize_y];
			ZeroMemory((float*)arr,3*psize_x*psize_y);

	txtr = NULL;
	txtr1 = NULL;
	txtr2 = NULL;
	txtrCellul = NULL;

	};

cl_mtrl_witrag::~cl_mtrl_witrag(void)
	{
		
		if ((float*)arr)		{delete []((float*)arr); arr = NULL;}
		if (txtr)				delete txtr;		txtr=NULL;
		if (txtr1)				delete txtr1;		txtr1=NULL;
		if (txtr2)				delete txtr2;		txtr2=NULL;
		if (txtrCellul)			delete txtrCellul;	txtrCellul=NULL;
	};

cl_mtrl_witrag* mt1 = NULL;





	void amk_options::gen_new_opt(void)// 
	{	
	pixelate_x=1; pixelate_y=1;
	bAnimOn = true;

	//18	itrBlur=0*5;
	//18	amountBlur=3;
	//18	beforeBlur=5;

	//18	itrBlur0=0;
	//18	amountBlur0=0;
	//18	beforeBlur0=10;//2;

	//18	itrBlur1=0;
	//18	amountBlur1=0;
	//18	beforeBlur1=10;//2;

	//18	itrBlur2=0;
	//18	amountBlur2=0;
	//18	beforeBlur2=10;//2;

	
	//18 deltaTxtrSize=4*6;
	//18 	deltaTxtr1Size=1*6;
	//18 	deltaTxtr2Size=3*6;


	if (idFN==0) {
		//18 		 invMainCh0 = 0.928000; negMainCh0 = 0.605000; shftMainCh0 = 0.080000; pwrMainCh0 = -0.333333;
		//18 		 invMainCh1 = 0.928000; negMainCh1 = 0.605000; shftMainCh1 = 0.080000; pwrMainCh1 = -0.333333;
		//18 		invMainCh2 = 0.928000; negMainCh2 = 1.605000; shftMainCh2 = 0.080000; pwrMainCh2 = -0.333333;
	} else 
	if (idFN==1) {
 //mt1->txtr->flip();
 //mt1->txtr1->flip();
 //mt1->txtr2->flip();
		//mt1->txtrCellul->flip(procedural::flipx,procedural::addy2addx2,1.0f,1.0f);

				} else
	{
		//mt1->txtrCellul->flip(procedural::flipx,procedural::addy2addx2,1.0f/(float)idFN,1.0f/(float)idFN);
		//mt1->txtrCellul->flip(procedural::flipxy,procedural::addxdivy,1.0f,1.0f);
		//shftMainCh0 = (shftMainCh0+(float)idFN*0.00005f);

		mt1->txtr ->move(-1,0);//green
		mt1->txtr1->move(-4,0);//blue // osnovnie belie wspolochi
		mt1->txtr2->move(-2,0);//red
		mt1->txtrCellul->move(1,0);//green


		
	}

	//                x  y  a,b  0-txtr, 1-txtr1, 2-txtr2
//19	mtrxMainCh[0][0][0] = 3; mtrxMainCh[1][0][0] = 2; mtrxMainCh[2][0][0] = 1;
//19	mtrxMainCh[0][0][1] = 3; mtrxMainCh[1][0][1] = 0; mtrxMainCh[2][0][1] = 1;

//19	mtrxMainCh[0][1][0] = 0; mtrxMainCh[1][1][0] = 3; mtrxMainCh[2][1][0] = 3;
//19	mtrxMainCh[0][1][1] = 0; mtrxMainCh[1][1][1] = 3; mtrxMainCh[2][1][1] = 1;

//19	mtrxMainCh[0][2][0] = 2; mtrxMainCh[1][2][0] = 1; mtrxMainCh[2][2][0] = 3;
//19	mtrxMainCh[0][2][1] = 0; mtrxMainCh[1][2][1] = 1; mtrxMainCh[2][2][1] = 3;

	iSize = iSize;//4096;

	prefixFN = prefixFN;
	//idFN = 1;
	bFN_gen = bFN_gen;
		
	txtrCellPoints = (9 + (rand() % 17 ))*10;
		generateLogFile();

	};



	void amk_options::gen_new_opt_bluegreen_a(void) //_bluegreen
	{
	mtrl1MatTecniq = mtrl_1_tech::mtrl_1_witrage;
	txtrCellTechniq = cellul_tech::ct_diffntl_nrst;
	bAnimOn = true;

	invMainCh0 = -1.0f;
	negMainCh0 =   -1.0f;//(rand() % 1000) / 1000.0f - .5f;


	if (idFN==0) {
	shftMainCh0 =  0.0f;
	pwrMainCh0 = -1.0f / 3.0f+0.0f*((float)((rand() % 100))/100.0f);

	invMainCh1 = 1.0f;
	negMainCh1 =   1.0f;//(rand() % 1000) / 1000.0f - .5f;
	shftMainCh1 =  0.0f;
	pwrMainCh1 = -1.0f / 3.0f+0.0f*((float)((rand() % 100))/100.0f);

	invMainCh2 = 1.0f;
	negMainCh2 =   1.0f;//(rand() % 1000) / 1000.0f - .5f;
	shftMainCh2 =  0.0f;
	pwrMainCh2 = -1.0f / 3.0f+0.0f*((float)((rand() % 100))/100.0f);

	//                x  y  a,b  0-txtr, 1-txtr1, 2-txtr2
	mtrxMainCh[0][0][0] = 2;
	mtrxMainCh[0][0][1] = 0;
	mtrxMainCh[1][0][0] = 1;
	mtrxMainCh[1][0][1] = 1;
	mtrxMainCh[2][0][0] = 2;
	mtrxMainCh[2][0][1] = 2;

	mtrxMainCh[0][1][0] = 1;
	mtrxMainCh[0][1][1] = 1;
	mtrxMainCh[1][1][0] = 0;
	mtrxMainCh[1][1][1] = 1;
	mtrxMainCh[2][1][0] = 1;
	mtrxMainCh[2][1][1] = 1;

	mtrxMainCh[0][2][0] = 2;
	mtrxMainCh[0][2][1] = 2;
	mtrxMainCh[1][2][0] = 1;
	mtrxMainCh[1][2][1] = 1;
	mtrxMainCh[2][2][0] = 0;
	mtrxMainCh[2][2][1] = 2;

	txtrCellPoints = (6 + (rand() % 5 ))*100;
	} else	{
		mt1->txtr ->move(0,-6);//green
		mt1->txtr1->move(0,-4);//blue // osnovnie belie wspolochi
		mt1->txtr2->move(0,-2);//red
			}
		generateLogFile();
	};


	void amk_options::gen_new_opt_witrage_original_a(void)// 
	{	
	pixelate_x=2; pixelate_y=2;
	mtrl1MatTecniq = mtrl_1_tech::mtrl_1_witrage;
	txtrCellTechniq = cellul_tech::ct_diffntl_nrst;
	bAnimOn = true;

	itrBlur=2;
	amountBlur=2;
	beforeBlur=0*5;

	itrBlur0=0*4;
	amountBlur0=3;
	beforeBlur0=2;//2;

	itrBlur1=0*4;
	amountBlur1=3;
	beforeBlur1=2;//2;

	itrBlur2=0*4;
	amountBlur2=3;
	beforeBlur2=2;//2;



	
	deltaTxtrSize=0;//0-witrage, 1-stucco
	deltaTxtr1Size=0;
	deltaTxtr2Size=0;



	if (idFN==0) {
	//stucco
 invMainCh0 = 1.0; negMainCh0 = 1.000f; shftMainCh0 = 0.00000; pwrMainCh0 = 1.0f;
 invMainCh1 = 1.0; negMainCh1 = 1.000f; shftMainCh1 = 0.00000; pwrMainCh1 = 1.0f;
 invMainCh2 = 1.0; negMainCh2 = 1.000f; shftMainCh2 = 0.00000; pwrMainCh2 = 1.0f;
				} else
	{
		//shftMainCh0 = (shftMainCh0+(float)idFN*0.001f);
		mt1->txtr ->move(0,6);//green
		mt1->txtr1->move(0,6);//blue // osnovnie belie wspolochi
		mt1->txtr2->move(0,6);//red
		

		
	}



	//                x  y  a,b  0-txtr, 1-txtr1, 2-txtr2
	mtrxMainCh[0][0][0] = 2;
	mtrxMainCh[0][0][1] = 0;
	mtrxMainCh[1][0][0] = 1;
	mtrxMainCh[1][0][1] = 1;
	mtrxMainCh[2][0][0] = 2;
	mtrxMainCh[2][0][1] = 2;

	mtrxMainCh[0][1][0] = 1;
	mtrxMainCh[0][1][1] = 1;
	mtrxMainCh[1][1][0] = 0;
	mtrxMainCh[1][1][1] = 1;
	mtrxMainCh[2][1][0] = 1;
	mtrxMainCh[2][1][1] = 1;

	mtrxMainCh[0][2][0] = 2;
	mtrxMainCh[0][2][1] = 2;
	mtrxMainCh[1][2][0] = 1;
	mtrxMainCh[1][2][1] = 1;
	mtrxMainCh[2][2][0] = 0;
	mtrxMainCh[2][2][1] = 2;


	prefixFN = prefixFN;
		
	txtrCellPoints = 180;//(9 + (rand() % 17 ))*10;
		generateLogFile();

	};


	void amk_options::gen_new_opt_tiberium_2a(void)// 
	{
	pixelate_x=1; pixelate_y=1;
	bAnimOn = true;

	itrBlur=0*5;
	amountBlur=3;
	beforeBlur=5;

	itrBlur0=0;
	amountBlur0=0;
	beforeBlur0=10;//2;

	itrBlur1=0;
	amountBlur1=0;
	beforeBlur1=10;//2;

	itrBlur2=0;
	amountBlur2=0;
	beforeBlur2=10;//2;


	deltaTxtrSize=4*6;
	deltaTxtr1Size=1*6;
	deltaTxtr2Size=3*6;


	if (idFN==0) {
 invMainCh0 = 0.928000f; negMainCh0 = 0.605000f; shftMainCh0 = 0.080000f; pwrMainCh0 = -0.333333f;
 invMainCh1 = 0.928000f; negMainCh1 = 0.605000f; shftMainCh1 = 0.080000f; pwrMainCh1 = -0.333333f;
 invMainCh2 = 0.928000f; negMainCh2 = 1.605000f; shftMainCh2 = 0.080000f; pwrMainCh2 = -0.333333f;
				} else
	{
		//shftMainCh0 = (shftMainCh0+(float)idFN*0.001f);
		mt1->txtr ->move_dsslv_blur(0,-1);//green
		mt1->txtr1->move_dsslv_blur(-4,0);//blue // osnovnie belie wspolochi
		mt1->txtr2->move(1,2);//red
		

		
	}

	//                x  y  a,b  0-txtr, 1-txtr1, 2-txtr2
	mtrxMainCh[0][0][0] = 3; mtrxMainCh[1][0][0] = 2; mtrxMainCh[2][0][0] = 1;
	mtrxMainCh[0][0][1] = 3; mtrxMainCh[1][0][1] = 0; mtrxMainCh[2][0][1] = 1;

	mtrxMainCh[0][1][0] = 0; mtrxMainCh[1][1][0] = 3; mtrxMainCh[2][1][0] = 3;
	mtrxMainCh[0][1][1] = 0; mtrxMainCh[1][1][1] = 3; mtrxMainCh[2][1][1] = 1;

	mtrxMainCh[0][2][0] = 2; mtrxMainCh[1][2][0] = 1; mtrxMainCh[2][2][0] = 3;
	mtrxMainCh[0][2][1] = 0; mtrxMainCh[1][2][1] = 1; mtrxMainCh[2][2][1] = 3;

	iSize = iSize;//4096;

	prefixFN = prefixFN;
	//idFN = 1;
	bFN_gen = bFN_gen;
		
	txtrCellPoints = (9 + (rand() % 17 ))*10;
		generateLogFile();

	};


	void amk_options::gen_new_opt_protoplasma_2a(void) //_protoplasma_2_
	{
	bAnimOn = true;

	itrBlur=15;
	amountBlur=8;
	beforeBlur=5;

	itrBlur0=0;
	amountBlur0=0;
	beforeBlur0=5;//2;

	itrBlur1=0;
	amountBlur1=0;
	beforeBlur1=5;//2;

	itrBlur2=0;
	amountBlur2=0;
	beforeBlur2=15;//2;


	deltaTxtrSize=4*16;
	deltaTxtr1Size=(1+2)*16;
	deltaTxtr2Size=3*32;


	//meylw43
	
	if (idFN==0) {
invMainCh0 = 0.066000; negMainCh0 = -0.199000; shftMainCh0 = 0.0891000f; pwrMainCh0 = -0.333333;
invMainCh1 = 0.629000; negMainCh1 = -0.957000; shftMainCh1 = 0.0729000f*12.0f; pwrMainCh1 = -0.333333;
invMainCh2 = 1.500752000; negMainCh2 = 0.219000; shftMainCh2 =  0.0046000f; pwrMainCh2 = -0.333333;
				} else
	{
		//shftMainCh0 = (shftMainCh0+(float)idFN*0.001f);
		mt1->txtr ->move_dsslv_blur(0,-1);//green
		mt1->txtr1->move_dsslv_blur(-4,0);//blue // osnovnie belie wspolochi
		mt1->txtr2->move(1,2);//red

		
	}


	//                x  y  a,b  0-txtr, 1-txtr1, 2-txtr2
	mtrxMainCh[0][0][0] = 2;
	mtrxMainCh[0][0][1] = 0;
	mtrxMainCh[1][0][0] = 1;
	mtrxMainCh[1][0][1] = 1;
	mtrxMainCh[2][0][0] = 2;
	mtrxMainCh[2][0][1] = 2;

	mtrxMainCh[0][1][0] = 1;
	mtrxMainCh[0][1][1] = 1;
	mtrxMainCh[1][1][0] = 0;
	mtrxMainCh[1][1][1] = 1;
	mtrxMainCh[2][1][0] = 1;
	mtrxMainCh[2][1][1] = 1;

	mtrxMainCh[0][2][0] = 2;
	mtrxMainCh[0][2][1] = 2;
	mtrxMainCh[1][2][0] = 1;
	mtrxMainCh[1][2][1] = 1;
	mtrxMainCh[2][2][0] = 0;
	mtrxMainCh[2][2][1] = 2;




	prefixFN = prefixFN;
	//idFN = 1;
	bFN_gen = bFN_gen;
		
	txtrCellPoints = (9 + (rand() % 5 ))*20;


	char buff[1024];
	sprintf_s(buff,"%s%d.txt",prefixFN,idFN);
	FILE *flog;
	if (fopen_s( &flog, buff, "w" )==0) {
			
	
	fprintf( flog, "prefixFN = %s; idFN = %d;\n", prefixFN, idFN);
	fprintf( flog, "///////////////////////////////////////////");
	fprintf( flog, "iSize = %d;\n", iSize);
	fprintf( flog, "itrBlur = %d;	amountBlur = %d;	beforeBlur = %d;\n ", itrBlur,	amountBlur,	beforeBlur);
	fprintf( flog, "itrBlur0 = %d;	amountBlur0 = %d;	beforeBlur0 = %d;\n ", itrBlur0,	amountBlur0,	beforeBlur0);
	fprintf( flog, "itrBlur1 = %d;	amountBlur1 = %d;	beforeBlur1 = %d;\n ", itrBlur1,	amountBlur1,	beforeBlur1);
	fprintf( flog, "itrBlur2 = %d;	amountBlur2 = %d;	beforeBlur2 = %d;\n\n ", itrBlur2,	amountBlur2,	beforeBlur2);
	
	fprintf( flog, "deltaTxtrSize = %f;	deltaTxtr1Size = %f;	deltaTxtr2Size = %f;\n\n ", deltaTxtrSize,	deltaTxtr1Size,	deltaTxtr2Size);


		fprintf( flog, "invMainCh0 = %f; negMainCh0 = %f; shftMainCh0 = %f; pwrMainCh0 = %f;\n", invMainCh0, negMainCh0, shftMainCh0, pwrMainCh0);
		fprintf( flog, "invMainCh1 = %f; negMainCh1 = %f; shftMainCh1 = %f; pwrMainCh1 = %f;\n", invMainCh1, negMainCh1, shftMainCh1, pwrMainCh1);
		fprintf( flog, "invMainCh2 = %f; negMainCh2 = %f; shftMainCh2 = %f; pwrMainCh2 = %f;\n", invMainCh2, negMainCh2, shftMainCh2, pwrMainCh2);
	
		fprintf( flog, "txtrCellPoints = %d;\n", txtrCellPoints);

					fclose( flog );
											};


	};