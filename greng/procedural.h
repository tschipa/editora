#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <malloc.h>
#include <memory.h>
#include <wchar.h>
#include <windows.h>

float mabs(float p);

enum cellul_tech {
	ct_nearest,
	ct_diffntl_nrst,
	ct_diffntl2_nrst,
	ct_diffntl5_nrst, 
	
	ct_diffntl2_nrst_quad,
	ct_folliage}; // peretchislenie technik dlja karty cellular

enum mtrl_1_tech {
	mtrl_1_witrage,
	mtrl_1_crstll_marble_sky}; // peretchislenie technik materialoff dlja materiala 1


struct amk_options
{
	float invMainCh0, invMainCh1, invMainCh2;  // inverse or not main channel (1.0f, -1.0f) == f(x) or 1/f(x)
	float negMainCh0, negMainCh1, negMainCh2;  // negate or not main channel (1.0f ... -1.0f) == f(x) or  -f(x)
	float shftMainCh0, shftMainCh1, shftMainCh2;// shift constant of main channel f(x) + a
	float pwrMainCh0, pwrMainCh1, pwrMainCh2; // pwr of main channel pow(f(x),pwrMainCh) default value -1.0f/3.0f
	float deltaTxtrSize, deltaTxtr1Size, deltaTxtr2Size; // >=0

	int   randSeed;  ///init value(seed) for random numbers generator
	bool  brandSeed; // if true get randseed automatically from clock
	int   iSize;	 // output bitmap screen size 256,512,1024,2048,4096
    char* prefixFN;  //prefix for output filename
	int   idFN;		//incremmentally number for filename (prefixFN+itoa(idFN)+".png"), if<0 no number in filename used, just prefix
	bool bFN_gen;   //if true generate number for filename incremmentally
	WCHAR wFN[200];

	int    itrBlur,   itrBlur0,   itrBlur1,   itrBlur2;//number of blur iterations of amountBlur pixels each
	int amountBlur,amountBlur0,amountBlur1,amountBlur2;//amount of pixels in blur
	int beforeBlur,beforeBlur0,beforeBlur1,beforeBlur2;//amount of pixels in blur before iterated blur

	//the matrix contains prepared values for mixing of 3 textures and stores result textures as dekart coordinates of vectors in 3d space,
	//that can be convoluted with fourth texture (for examle with Cellular RGB) as linear combination.
	//mtrxMainCh[id of basis of dekart components of result texture 0..2][id of result RGB channel 0..2][id's of input texture #1,#2 0..1]
	//[2][0] [1][1] [2][2]
	//[2][2] [0][1] [2][2]
	//[2][2] [1][1] [0][2]
	int mtrxMainCh[3][3][2];//matrix of showinng textures.. X,Y, a and b. where 0-txtr, 1-txtr1, 2-txtr2, 3-txtrCellul;
	int txtrCellPoints;//number of points in Cellular texture
	cellul_tech txtrCellTechniq;
	mtrl_1_tech mtrl1MatTecniq;

	int pixelate_x, pixelate_y ;//x- and y-integer magnifickator by zoom  the outgoing picture
	bool bAnimOn; //is animation mode on? where the texture generators turns only one time by the first reneder, then change it parameters
	void initialize(void)
	{
	invMainCh0 = (rand() % 100) > 19 ? 1.0f : -1.0f;
	negMainCh0 = (rand() % 1000) / 1000.0f - .5f;
	shftMainCh0 =(rand() % 1000) / 1000.0f;
	pwrMainCh0 = -1.0f / 3.0f;

	invMainCh1 = invMainCh0;
	negMainCh1 = negMainCh0;
	shftMainCh1 = shftMainCh0;
	pwrMainCh1  = pwrMainCh0;


	invMainCh2 = invMainCh0;
	negMainCh2 = negMainCh0;
	shftMainCh2 = shftMainCh0;
	pwrMainCh2  = pwrMainCh0;

	deltaTxtrSize=4; deltaTxtr1Size=1; deltaTxtr2Size=3;

	randSeed = 29711203; //19150103
	brandSeed = true;


	//the matrix contains prepared values for mixing of 3 textures and stores result textures as dekart coordinates of vectors in 3d space,
	//that can be convoluted with fourth texture (for examle with Cellular RGB) as linear combination.
	//mtrxMainCh[id of basis of dekart components of result texture 0..2][id of result RGB channel 0..2][id's of input texture #1,#2 0..1]
	//                x  y  a,b  0-txtr, 1-txtr1, 2-txtr2
	mtrxMainCh[0][0][0] = 2;
	mtrxMainCh[0][0][1] = 0;
	mtrxMainCh[1][0][0] = 1;
	mtrxMainCh[1][0][1] = 1;
	mtrxMainCh[2][0][0] = 2;
	mtrxMainCh[2][0][1] = 2;

	mtrxMainCh[0][1][0] = 1;
	mtrxMainCh[0][1][1] = 1;
	mtrxMainCh[1][1][0] = 0;
	mtrxMainCh[1][1][1] = 1;
	mtrxMainCh[2][1][0] = 1;
	mtrxMainCh[2][1][1] = 1;

	mtrxMainCh[0][2][0] = 2;
	mtrxMainCh[0][2][1] = 2;
	mtrxMainCh[1][2][0] = 1;
	mtrxMainCh[1][2][1] = 1;
	mtrxMainCh[2][2][0] = 0;
	mtrxMainCh[2][2][1] = 2;


	iSize = 512;//512;//256;//1024; //4096;

	prefixFN = "tib\\tib2_";
	idFN = 0;//VERY IMPORTANT THAT WILL BE START FROM ZERO!!! (FOR ALL THE ANIMATION)
	bFN_gen = true;
		
	txtrCellPoints = (6 + (rand() % 5 ))*100;
	txtrCellTechniq = cellul_tech::ct_diffntl_nrst;
	//mtrl1MatTecniq = mtrl_1_tech::mtrl_1_witrage;
	mtrl1MatTecniq = mtrl_1_tech::mtrl_1_crstll_marble_sky;

	itrBlur=15;
	amountBlur=8;
	beforeBlur=5;

	itrBlur0=0;
	amountBlur0=0;
	beforeBlur0=10;

	itrBlur1=0;
	amountBlur1=0;
	beforeBlur1=10;

	itrBlur2=0;
	amountBlur2=0;
	beforeBlur2=10;

	pixelate_x=1; pixelate_y=1;//x- and y-integer magnifickator by zoom  the outgoing picture
	bAnimOn = false; //is animation mode on? where the texture generators turns only one time by the first reneder, then change it parameters

		//	enum iflCommander {iflPlain,iflPingPong};
		//generateIFLFile(350, prefixFN, prefixFN);
	generateIFLFile(350, "tib2_", "tib2_");

	};

	
	void gen_new_opt(void); // _bluegreen
	void gen_new_opt_bluegreen_a(void); //_bluegreen
	void gen_new_opt_protoplasma_2a(void); //_protoplasma_2_
	void gen_new_opt_tiberium_2a(void); // 	best of flames!
	void gen_new_opt_witrage_original_a(void);// 

	void gen_new_opt_remastered(void)// _tiberium_2
	{
	pixelate_x=1; pixelate_y=1;

	itrBlur=0*5;
	amountBlur=3;
	beforeBlur=5;

	itrBlur0=0;
	amountBlur0=0;
	beforeBlur0=10;//2;

	itrBlur1=0;
	amountBlur1=0;
	beforeBlur1=10;//2;

	itrBlur2=0;
	amountBlur2=0;
	beforeBlur2=10;//2;


	deltaTxtrSize=4*6;
	deltaTxtr1Size=1*6;
	deltaTxtr2Size=3*6;




 invMainCh0 = 0.928000f; negMainCh0 = 0.605000f; shftMainCh0 = 0.080000f; pwrMainCh0 = -0.333333f;
 invMainCh1 = 0.928000f; negMainCh1 = 0.605000f; shftMainCh1 = 0.080000f; pwrMainCh1 = -0.333333f;
 invMainCh2 = 0.928000f; negMainCh2 = 1.605000f; shftMainCh2 = 0.080000f; pwrMainCh2 = -0.333333f;



	//                x  y  a,b  0-txtr, 1-txtr1, 2-txtr2
	mtrxMainCh[0][0][0] = 2; mtrxMainCh[1][0][0] = 3; mtrxMainCh[2][0][0] = 1;
	mtrxMainCh[0][0][1] = 2; mtrxMainCh[1][0][1] = 0; mtrxMainCh[2][0][1] = 1;

	mtrxMainCh[0][1][0] = 0; mtrxMainCh[1][1][0] = 2; mtrxMainCh[2][1][0] = 2;
	mtrxMainCh[0][1][1] = 0; mtrxMainCh[1][1][1] = 2; mtrxMainCh[2][1][1] = 1;

	mtrxMainCh[0][2][0] = 3; mtrxMainCh[1][2][0] = 1; mtrxMainCh[2][2][0] = 2;
	mtrxMainCh[0][2][1] = 0; mtrxMainCh[1][2][1] = 1; mtrxMainCh[2][2][1] = 2;

	iSize = iSize;//4096;

	prefixFN = prefixFN;
	//idFN = 1;
	bFN_gen = bFN_gen;
		
	txtrCellPoints = (9 + (rand() % 17 ))*10;
		generateLogFile();

	};

	
	
	void gen_new_opt_witrage_original(void)// _witrage_original
	{	
	pixelate_x=2; pixelate_y=2;
	mtrl1MatTecniq = mtrl_1_tech::mtrl_1_witrage;
	txtrCellTechniq = cellul_tech::ct_diffntl_nrst;
	
	itrBlur=2;
	amountBlur=2;
	beforeBlur=0*5;

	itrBlur0=0*4;
	amountBlur0=3;
	beforeBlur0=2;//2;

	itrBlur1=0*4;
	amountBlur1=3;
	beforeBlur1=2;//2;

	itrBlur2=0*4;
	amountBlur2=3;
	beforeBlur2=2;//2;



	
	deltaTxtrSize=0;//0-witrage, 1-stucco
	deltaTxtr1Size=0;
	deltaTxtr2Size=0;




	//stucco
 invMainCh0 = 1.0; negMainCh0 = 1.000f; shftMainCh0 = 0.00000; pwrMainCh0 = 1.0f;
 invMainCh1 = 1.0; negMainCh1 = 1.000f; shftMainCh1 = 0.00000; pwrMainCh1 = 1.0f;
 invMainCh2 = 1.0; negMainCh2 = 1.000f; shftMainCh2 = 0.00000; pwrMainCh2 = 1.0f;



	//                x  y  a,b  0-txtr, 1-txtr1, 2-txtr2
	mtrxMainCh[0][0][0] = 2;
	mtrxMainCh[0][0][1] = 0;
	mtrxMainCh[1][0][0] = 1;
	mtrxMainCh[1][0][1] = 1;
	mtrxMainCh[2][0][0] = 2;
	mtrxMainCh[2][0][1] = 2;

	mtrxMainCh[0][1][0] = 1;
	mtrxMainCh[0][1][1] = 1;
	mtrxMainCh[1][1][0] = 0;
	mtrxMainCh[1][1][1] = 1;
	mtrxMainCh[2][1][0] = 1;
	mtrxMainCh[2][1][1] = 1;

	mtrxMainCh[0][2][0] = 2;
	mtrxMainCh[0][2][1] = 2;
	mtrxMainCh[1][2][0] = 1;
	mtrxMainCh[1][2][1] = 1;
	mtrxMainCh[2][2][0] = 0;
	mtrxMainCh[2][2][1] = 2;


	prefixFN = prefixFN;
		
	txtrCellPoints = 180;//(9 + (rand() % 17 ))*10;
		generateLogFile();

	};

	
	void gen_new_opt_witrage_stucco(void)// _witrage_stucco
	{	
	pixelate_x=1; pixelate_y=1;
	mtrl1MatTecniq = mtrl_1_tech::mtrl_1_witrage;
	txtrCellTechniq = cellul_tech::ct_diffntl_nrst;
	
	itrBlur=6;
	amountBlur=3;
	beforeBlur=0*5;

	itrBlur0=0*4;
	amountBlur0=3;
	beforeBlur0=2;//2;

	itrBlur1=0*4;
	amountBlur1=3;
	beforeBlur1=2;//2;

	itrBlur2=0*4;
	amountBlur2=3;
	beforeBlur2=2;//2;



	
	deltaTxtrSize=4*6*1+4;//0-witrage, 1-stucco
	deltaTxtr1Size=1*6*0+1;
	deltaTxtr2Size=3*6*0+3;




	//stucco
 invMainCh0 = 1.0; negMainCh0 = 3.000f/4.00f; shftMainCh0 = 0.00000; pwrMainCh0 = 1.0f/3.0f;
 invMainCh1 = 1.0; negMainCh1 = 3.000f/4.00f; shftMainCh1 = 0.00000; pwrMainCh1 = 1.0f/3.0f;
 invMainCh2 = 1.0; negMainCh2 = 3.000f/4.00f; shftMainCh2 = 0.00000; pwrMainCh2 = 1.0f/3.0f;



	//                x  y  a,b  0-txtr, 1-txtr1, 2-txtr2
	mtrxMainCh[0][0][0] = 2;
	mtrxMainCh[0][0][1] = 0;
	mtrxMainCh[1][0][0] = 1;
	mtrxMainCh[1][0][1] = 1;
	mtrxMainCh[2][0][0] = 2;
	mtrxMainCh[2][0][1] = 2;

	mtrxMainCh[0][1][0] = 1;
	mtrxMainCh[0][1][1] = 1;
	mtrxMainCh[1][1][0] = 0;
	mtrxMainCh[1][1][1] = 1;
	mtrxMainCh[2][1][0] = 1;
	mtrxMainCh[2][1][1] = 1;

	mtrxMainCh[0][2][0] = 2;
	mtrxMainCh[0][2][1] = 2;
	mtrxMainCh[1][2][0] = 1;
	mtrxMainCh[1][2][1] = 1;
	mtrxMainCh[2][2][0] = 0;
	mtrxMainCh[2][2][1] = 2;


	prefixFN = prefixFN;
		
	txtrCellPoints = 180;//(9 + (rand() % 17 ))*10;
		generateLogFile();

	};

	
	void gen_new_opt_tiberium_2(void)// 
	{
	pixelate_x=1; pixelate_y=1;

	itrBlur=5;
	amountBlur=3;
	beforeBlur=5;

	itrBlur0=0;
	amountBlur0=0;
	beforeBlur0=10;//2;

	itrBlur1=0;
	amountBlur1=0;
	beforeBlur1=10;//2;

	itrBlur2=0;
	amountBlur2=0;
	beforeBlur2=10;//2;


	deltaTxtrSize=4*6;
	deltaTxtr1Size=1*6;
	deltaTxtr2Size=3*6;




 invMainCh0 = 0.928000f; negMainCh0 = 0.605000f; shftMainCh0 = 0.080000f; pwrMainCh0 = -0.333333f;
 invMainCh1 = 0.928000f; negMainCh1 = 0.605000f; shftMainCh1 = 0.080000f; pwrMainCh1 = -0.333333f;
 invMainCh2 = 0.928000f; negMainCh2 = 0.605000f; shftMainCh2 = 0.080000f; pwrMainCh2 = -0.333333f;



	//                x  y  a,b  0-txtr, 1-txtr1, 2-txtr2
	mtrxMainCh[0][0][0] = 2;
	mtrxMainCh[0][0][1] = 0;
	mtrxMainCh[1][0][0] = 1;
	mtrxMainCh[1][0][1] = 1;
	mtrxMainCh[2][0][0] = 2;
	mtrxMainCh[2][0][1] = 2;

	mtrxMainCh[0][1][0] = 1;
	mtrxMainCh[0][1][1] = 1;
	mtrxMainCh[1][1][0] = 0;
	mtrxMainCh[1][1][1] = 1;
	mtrxMainCh[2][1][0] = 1;
	mtrxMainCh[2][1][1] = 1;

	mtrxMainCh[0][2][0] = 2;
	mtrxMainCh[0][2][1] = 2;
	mtrxMainCh[1][2][0] = 1;
	mtrxMainCh[1][2][1] = 1;
	mtrxMainCh[2][2][0] = 0;
	mtrxMainCh[2][2][1] = 2;


	iSize = iSize;//4096;

	prefixFN = prefixFN;
	//idFN = 1;
	bFN_gen = bFN_gen;
		
	txtrCellPoints = (9 + (rand() % 17 ))*10;
		generateLogFile();

	};


	
	
	void gen_new_opt_nightly_sky(void)// nightly_sky_
	{
	itrBlur=15;
	amountBlur=8;
	beforeBlur=5;

	itrBlur0=0;
	amountBlur0=0;
	beforeBlur0=10;//2;

	itrBlur1=0;
	amountBlur1=0;
	beforeBlur1=10;//2;

	itrBlur2=0;
	amountBlur2=0;
	beforeBlur2=10;//2;


	deltaTxtrSize=16*4;
	deltaTxtr1Size=4*4;
	deltaTxtr2Size=12*4;




invMainCh0 = -0.532000f; negMainCh0 = -0.662000f; shftMainCh0 = 0.630000f; pwrMainCh0 = -0.333333f;
invMainCh1 = -0.283000f; negMainCh1 = -0.902000f; shftMainCh1 = 0.823000f; pwrMainCh1 = -0.333333f;
invMainCh2 = -0.411000f; negMainCh2 = -0.409000f; shftMainCh2 = 0.788000f; pwrMainCh2 = -0.333333f;




	//                x  y  a,b  0-txtr, 1-txtr1, 2-txtr2
	mtrxMainCh[0][0][0] = 2;
	mtrxMainCh[0][0][1] = 0;
	mtrxMainCh[1][0][0] = 1;
	mtrxMainCh[1][0][1] = 1;
	mtrxMainCh[2][0][0] = 2;
	mtrxMainCh[2][0][1] = 2;

	mtrxMainCh[0][1][0] = 1;
	mtrxMainCh[0][1][1] = 1;
	mtrxMainCh[1][1][0] = 0;
	mtrxMainCh[1][1][1] = 1;
	mtrxMainCh[2][1][0] = 1;
	mtrxMainCh[2][1][1] = 1;

	mtrxMainCh[0][2][0] = 2;
	mtrxMainCh[0][2][1] = 2;
	mtrxMainCh[1][2][0] = 1;
	mtrxMainCh[1][2][1] = 1;
	mtrxMainCh[2][2][0] = 0;
	mtrxMainCh[2][2][1] = 2;


	iSize = iSize;//4096;

	prefixFN = prefixFN;
	//idFN = 1;
	bFN_gen = bFN_gen;
		
	txtrCellPoints = (6 + (rand() % 20 ))*10;
		generateLogFile();

	};


	
	void gen_new_opt_tiberium_1_(void)// tiberium_1_
	{
	itrBlur=5;
	amountBlur=3;
	beforeBlur=5;

	itrBlur0=0;
	amountBlur0=0;
	beforeBlur0=10;//2;

	itrBlur1=0;
	amountBlur1=0;
	beforeBlur1=10;//2;

	itrBlur2=0;
	amountBlur2=0;
	beforeBlur2=10;//2;


	deltaTxtrSize=4*6;
	deltaTxtr1Size=1*6;
	deltaTxtr2Size=3*6;




 invMainCh0 = 0.928000; negMainCh0 = 0.605000; shftMainCh0 = 0.080000; pwrMainCh0 = -0.333333;
 invMainCh1 = 0.928000; negMainCh1 = 0.605000; shftMainCh1 = 0.080000; pwrMainCh1 = -0.333333;
 invMainCh2 = 0.928000; negMainCh2 = 0.605000; shftMainCh2 = 0.080000; pwrMainCh2 = -0.333333;



	//                x  y  a,b  0-txtr, 1-txtr1, 2-txtr2
	mtrxMainCh[0][0][0] = 2;
	mtrxMainCh[0][0][1] = 0;
	mtrxMainCh[1][0][0] = 1;
	mtrxMainCh[1][0][1] = 1;
	mtrxMainCh[2][0][0] = 2;
	mtrxMainCh[2][0][1] = 2;

	mtrxMainCh[0][1][0] = 1;
	mtrxMainCh[0][1][1] = 1;
	mtrxMainCh[1][1][0] = 0;
	mtrxMainCh[1][1][1] = 1;
	mtrxMainCh[2][1][0] = 1;
	mtrxMainCh[2][1][1] = 1;

	mtrxMainCh[0][2][0] = 2;
	mtrxMainCh[0][2][1] = 2;
	mtrxMainCh[1][2][0] = 1;
	mtrxMainCh[1][2][1] = 1;
	mtrxMainCh[2][2][0] = 0;
	mtrxMainCh[2][2][1] = 2;


	iSize = iSize;//4096;

	prefixFN = prefixFN;
	//idFN = 1;
	bFN_gen = bFN_gen;
		
	txtrCellPoints = (9 + (rand() % 17 ))*10;
		generateLogFile();

	};


	void gen_new_opt_ce_red_prpl_mrmr(void)//
	{
	itrBlur=12;//40;
	amountBlur=3;
	//beforeBlur2=10;

invMainCh0 = 1.000000; negMainCh0 = 1.000000; shftMainCh0 = 0.000000; pwrMainCh0 = -0.333333;
invMainCh1 = 1.000000; negMainCh1 = -0.457000; shftMainCh1 = 0.979000; pwrMainCh1 = -0.333333;
invMainCh2 = 1.000000; negMainCh2 = -0.457000; shftMainCh2 = 0.979000; pwrMainCh2 = -0.333333;


	deltaTxtrSize=4*10+4;
	deltaTxtr1Size=(1+0*2)*10+3;
	deltaTxtr2Size=3*10+1;



	//                x  y  a,b  0-txtr, 1-txtr1, 2-txtr2
	mtrxMainCh[0][0][0] = 2;
	mtrxMainCh[0][0][1] = 0;
	mtrxMainCh[1][0][0] = 1;
	mtrxMainCh[1][0][1] = 1;
	mtrxMainCh[2][0][0] = 2;
	mtrxMainCh[2][0][1] = 2;

	mtrxMainCh[0][1][0] = 1;
	mtrxMainCh[0][1][1] = 1;
	mtrxMainCh[1][1][0] = 0;
	mtrxMainCh[1][1][1] = 1;
	mtrxMainCh[2][1][0] = 1;
	mtrxMainCh[2][1][1] = 1;

	mtrxMainCh[0][2][0] = 2;
	mtrxMainCh[0][2][1] = 2;
	mtrxMainCh[1][2][0] = 1;
	mtrxMainCh[1][2][1] = 1;
	mtrxMainCh[2][2][0] = 0;
	mtrxMainCh[2][2][1] = 2;


	iSize = iSize;//4096;

	prefixFN = prefixFN;
	//idFN = 1;
	bFN_gen = bFN_gen;
		
	txtrCellPoints = (9 + (rand() % 5 ))*10;
	generateLogFile();
	};
	


	void gen_new_opt_sea_serp(void)//_sea_serp_
	{
	itrBlur=12;//40;
	amountBlur=3;
	//beforeBlur2=10;

	invMainCh0 = -1.000000; negMainCh0 = -1.000000; shftMainCh0 = 0.000000; pwrMainCh0 = -0.333333;
	invMainCh1 = 1.000000; negMainCh1 = 0.089000; shftMainCh1 = 0.484000; pwrMainCh1 = -0.333333;
	invMainCh2 = 1.000000; negMainCh2 = 0.089000; shftMainCh2 = 0.484000; pwrMainCh2 = -0.333333;


	deltaTxtrSize=4*10+4;
	deltaTxtr1Size=(1+0*2)*10+3;
	deltaTxtr2Size=3*10+1;



	//                x  y  a,b  0-txtr, 1-txtr1, 2-txtr2
	mtrxMainCh[0][0][0] = 2;
	mtrxMainCh[0][0][1] = 0;
	mtrxMainCh[1][0][0] = 1;
	mtrxMainCh[1][0][1] = 1;
	mtrxMainCh[2][0][0] = 2;
	mtrxMainCh[2][0][1] = 2;

	mtrxMainCh[0][1][0] = 1;
	mtrxMainCh[0][1][1] = 1;
	mtrxMainCh[1][1][0] = 0;
	mtrxMainCh[1][1][1] = 1;
	mtrxMainCh[2][1][0] = 1;
	mtrxMainCh[2][1][1] = 1;

	mtrxMainCh[0][2][0] = 2;
	mtrxMainCh[0][2][1] = 2;
	mtrxMainCh[1][2][0] = 1;
	mtrxMainCh[1][2][1] = 1;
	mtrxMainCh[2][2][0] = 0;
	mtrxMainCh[2][2][1] = 2;


	iSize = iSize;//4096;

	prefixFN = prefixFN;
	//idFN = 1;
	bFN_gen = bFN_gen;
		
	txtrCellPoints = (9 + (rand() % 5 ))*10;
	generateLogFile();
	};
	

	void gen_new_opt_tiberm_3(void)//_tiberm_3_
	{

	itrBlur0=5;	amountBlur0=2;	beforeBlur0=4;
	itrBlur1=5;	amountBlur1=2;	beforeBlur1=4;
	itrBlur2=5;	amountBlur2=2;	beforeBlur2=4;

	itrBlur=12;//40;
	amountBlur=3;
	beforeBlur=5;

	 //bluegreen ideal
	 invMainCh0 = -1.000000; negMainCh0 = -1.000000; shftMainCh0 = 0.001000; pwrMainCh0 = -0.333333;
	 invMainCh1 = 1.000000; negMainCh1 = 0.283000; shftMainCh1 = 0.040000; pwrMainCh1 = -0.333333;
	 invMainCh2 = 1.000000; negMainCh2 = 0.283000; shftMainCh2 = 0.040000; pwrMainCh2 = -0.333333;



	deltaTxtrSize=4*10+4;
	deltaTxtr1Size=(1+0*2)*10+3;
	deltaTxtr2Size=3*10+1;



	//                x  y  a,b  0-txtr, 1-txtr1, 2-txtr2
	mtrxMainCh[0][0][0] = 2;
	mtrxMainCh[0][0][1] = 0;
	mtrxMainCh[1][0][0] = 1;
	mtrxMainCh[1][0][1] = 1;
	mtrxMainCh[2][0][0] = 2;
	mtrxMainCh[2][0][1] = 2;

	mtrxMainCh[0][1][0] = 1;
	mtrxMainCh[0][1][1] = 1;
	mtrxMainCh[1][1][0] = 0;
	mtrxMainCh[1][1][1] = 1;
	mtrxMainCh[2][1][0] = 1;
	mtrxMainCh[2][1][1] = 1;

	mtrxMainCh[0][2][0] = 2;
	mtrxMainCh[0][2][1] = 2;
	mtrxMainCh[1][2][0] = 1;
	mtrxMainCh[1][2][1] = 1;
	mtrxMainCh[2][2][0] = 0;
	mtrxMainCh[2][2][1] = 2;


	iSize = iSize;//4096;

	prefixFN = prefixFN;
	//idFN = 1;
	bFN_gen = bFN_gen;
		
	txtrCellPoints = (9 + (rand() % 15 ))*10;
	generateLogFile();
	};
	


	void gen_new_opt_tiberm_4(void)//_tiberm_4_
	{

	itrBlur0=5;	amountBlur0=2;	beforeBlur0=4;
	itrBlur1=5;	amountBlur1=2;	beforeBlur1=4;
	itrBlur2=5;	amountBlur2=2;	beforeBlur2=4;

	itrBlur=12;//40;
	amountBlur=3;
	beforeBlur=5;

	invMainCh0 =  -1.0f;
	negMainCh0 =  -1.0f;//0.03;//(rand() % 2000) / 1000.0f - 1.0f;
	shftMainCh0 = 0.001f;//(rand() % 2000) / 1000.0f - 1.0f;
	pwrMainCh0 = -1.0f / 3.0f;

	invMainCh1 =  1.0f;
	negMainCh1 =  0.494f;//1.0f/0.01;//(rand() % 2000) / 1000.0f - 1.0f;
	shftMainCh1 = 0.353f;//(rand() % 2000) / 1000.0f - 1.0f;
	pwrMainCh1 = -1.0f / 3.0f;

	invMainCh2 =  1.0f;
	negMainCh2 =  0.494f;//0.03;// 1.0f/0.06;//(rand() % 2000) / 1000.0f - 1.0f;
	shftMainCh2 = 0.353f;//(rand() % 2000) / 1000.0f - 1.0f;
	pwrMainCh2 = -1.0f / 3.0f;


	deltaTxtrSize=4*10+4;
	deltaTxtr1Size=(1+0*2)*10+3;
	deltaTxtr2Size=3*10+1;



	//                x  y  a,b  0-txtr, 1-txtr1, 2-txtr2
	mtrxMainCh[0][0][0] = 2;
	mtrxMainCh[0][0][1] = 0;
	mtrxMainCh[1][0][0] = 1;
	mtrxMainCh[1][0][1] = 1;
	mtrxMainCh[2][0][0] = 2;
	mtrxMainCh[2][0][1] = 2;

	mtrxMainCh[0][1][0] = 1;
	mtrxMainCh[0][1][1] = 1;
	mtrxMainCh[1][1][0] = 0;
	mtrxMainCh[1][1][1] = 1;
	mtrxMainCh[2][1][0] = 1;
	mtrxMainCh[2][1][1] = 1;

	mtrxMainCh[0][2][0] = 2;
	mtrxMainCh[0][2][1] = 2;
	mtrxMainCh[1][2][0] = 1;
	mtrxMainCh[1][2][1] = 1;
	mtrxMainCh[2][2][0] = 0;
	mtrxMainCh[2][2][1] = 2;


	iSize = iSize;//4096;

	prefixFN = prefixFN;
	//idFN = 1;
	bFN_gen = bFN_gen;
		
	txtrCellPoints = (9 + (rand() % 15 ))*10;
	generateLogFile();
	};
	
	void gen_new_opt_tiberm_2_(void)//
	{
	itrBlur=2;//40;
	amountBlur=3;
	//beforeBlur2=10;

	invMainCh0 =  0.6f;
	negMainCh0 =  -1.0f;//0.03;//(rand() % 2000) / 1000.0f - 1.0f;
	shftMainCh0 = 1.0f;//(rand() % 2000) / 1000.0f - 1.0f;
	pwrMainCh0 = -1.0f / 3.0f;

	invMainCh0 =  0.6f;
	negMainCh0 =  1.0f;//1.0f/0.01;//(rand() % 2000) / 1000.0f - 1.0f;
	shftMainCh0 = 0.0;//(rand() % 2000) / 1000.0f - 1.0f;
	pwrMainCh0 = -1.0f / 3.0f;

	invMainCh0 =  0.6f;
	negMainCh0 = -1.0f;//0.03;// 1.0f/0.06;//(rand() % 2000) / 1000.0f - 1.0f;
	shftMainCh0 = 1.0f;//(rand() % 2000) / 1000.0f - 1.0f;
	pwrMainCh0 = -1.0f / 3.0f;


	deltaTxtrSize=4*10+4;
	deltaTxtr1Size=(1+2)*10+3;
	deltaTxtr2Size=3*10+1;



	//                x  y  a,b  0-txtr, 1-txtr1, 2-txtr2
	mtrxMainCh[0][0][0] = 2;
	mtrxMainCh[0][0][1] = 0;
	mtrxMainCh[1][0][0] = 1;
	mtrxMainCh[1][0][1] = 1;
	mtrxMainCh[2][0][0] = 2;
	mtrxMainCh[2][0][1] = 2;

	mtrxMainCh[0][1][0] = 1;
	mtrxMainCh[0][1][1] = 1;
	mtrxMainCh[1][1][0] = 0;
	mtrxMainCh[1][1][1] = 1;
	mtrxMainCh[2][1][0] = 1;
	mtrxMainCh[2][1][1] = 1;

	mtrxMainCh[0][2][0] = 2;
	mtrxMainCh[0][2][1] = 2;
	mtrxMainCh[1][2][0] = 1;
	mtrxMainCh[1][2][1] = 1;
	mtrxMainCh[2][2][0] = 0;
	mtrxMainCh[2][2][1] = 2;


	iSize = iSize;//4096;

	prefixFN = prefixFN;
	//idFN = 1;
	bFN_gen = bFN_gen;
		
	txtrCellPoints = (9 + (rand() % 5 ))*10;
	generateLogFile();

	};
	

void gen_new_opt_protoplasma_2_(void) //
	{
	itrBlur=15;
	amountBlur=8;
	beforeBlur=5;

	itrBlur0=0;
	amountBlur0=0;
	beforeBlur0=5;//2;

	itrBlur1=0;
	amountBlur1=0;
	beforeBlur1=5;//2;

	itrBlur2=0;
	amountBlur2=0;
	beforeBlur2=15;//2;


	deltaTxtrSize=4*16;
	deltaTxtr1Size=(1+2)*16;
	deltaTxtr2Size=3*32;


	//meylw43
invMainCh0 = 0.066000; negMainCh0 = -0.199000; shftMainCh0 = 0.0891000f; pwrMainCh0 = -0.333333;
invMainCh1 = 0.629000; negMainCh1 = -0.957000; shftMainCh1 = 0.0729000f*12.0f; pwrMainCh1 = -0.333333;
invMainCh2 = 1.500752000; negMainCh2 = 0.219000; shftMainCh2 =  0.0046000f; pwrMainCh2 = -0.333333;
	

	//                x  y  a,b  0-txtr, 1-txtr1, 2-txtr2
	mtrxMainCh[0][0][0] = 2;
	mtrxMainCh[0][0][1] = 0;
	mtrxMainCh[1][0][0] = 1;
	mtrxMainCh[1][0][1] = 1;
	mtrxMainCh[2][0][0] = 2;
	mtrxMainCh[2][0][1] = 2;

	mtrxMainCh[0][1][0] = 1;
	mtrxMainCh[0][1][1] = 1;
	mtrxMainCh[1][1][0] = 0;
	mtrxMainCh[1][1][1] = 1;
	mtrxMainCh[2][1][0] = 1;
	mtrxMainCh[2][1][1] = 1;

	mtrxMainCh[0][2][0] = 2;
	mtrxMainCh[0][2][1] = 2;
	mtrxMainCh[1][2][0] = 1;
	mtrxMainCh[1][2][1] = 1;
	mtrxMainCh[2][2][0] = 0;
	mtrxMainCh[2][2][1] = 2;




	prefixFN = prefixFN;
	//idFN = 1;
	bFN_gen = bFN_gen;
		
	txtrCellPoints = (9 + (rand() % 5 ))*20;


	char buff[1024];
	sprintf_s(buff,"%s%d.txt",prefixFN,idFN);
	FILE *flog;
	if (fopen_s( &flog, buff, "w" )==0) {
			
	
	fprintf( flog, "prefixFN = %s; idFN = %d;\n", prefixFN, idFN);
	fprintf( flog, "///////////////////////////////////////////");
	fprintf( flog, "iSize = %d;\n", iSize);
	fprintf( flog, "itrBlur = %d;	amountBlur = %d;	beforeBlur = %d;\n ", itrBlur,	amountBlur,	beforeBlur);
	fprintf( flog, "itrBlur0 = %d;	amountBlur0 = %d;	beforeBlur0 = %d;\n ", itrBlur0,	amountBlur0,	beforeBlur0);
	fprintf( flog, "itrBlur1 = %d;	amountBlur1 = %d;	beforeBlur1 = %d;\n ", itrBlur1,	amountBlur1,	beforeBlur1);
	fprintf( flog, "itrBlur2 = %d;	amountBlur2 = %d;	beforeBlur2 = %d;\n\n ", itrBlur2,	amountBlur2,	beforeBlur2);
	
	fprintf( flog, "deltaTxtrSize = %f;	deltaTxtr1Size = %f;	deltaTxtr2Size = %f;\n\n ", deltaTxtrSize,	deltaTxtr1Size,	deltaTxtr2Size);


		fprintf( flog, "invMainCh0 = %f; negMainCh0 = %f; shftMainCh0 = %f; pwrMainCh0 = %f;\n", invMainCh0, negMainCh0, shftMainCh0, pwrMainCh0);
		fprintf( flog, "invMainCh1 = %f; negMainCh1 = %f; shftMainCh1 = %f; pwrMainCh1 = %f;\n", invMainCh1, negMainCh1, shftMainCh1, pwrMainCh1);
		fprintf( flog, "invMainCh2 = %f; negMainCh2 = %f; shftMainCh2 = %f; pwrMainCh2 = %f;\n", invMainCh2, negMainCh2, shftMainCh2, pwrMainCh2);
	
		fprintf( flog, "txtrCellPoints = %d;\n", txtrCellPoints);

					fclose( flog );
											};


	};
void gen_new_opt_ytro(void)//
	{
	itrBlur=15;
	amountBlur=8;
	beforeBlur=5;

	itrBlur0=0;
	amountBlur0=0;
	beforeBlur0=10;//2;

	itrBlur1=0;
	amountBlur1=0;
	beforeBlur1=10;//2;

	itrBlur2=0;
	amountBlur2=0;
	beforeBlur2=10;//2;


	deltaTxtrSize=4*4;
	deltaTxtr1Size=1*3;
	deltaTxtr2Size=3*3;



invMainCh0 = 0.933000; negMainCh0 = 0.393000; shftMainCh0 = 0.578000; pwrMainCh0 = -0.333333;
invMainCh1 = 0.787000; negMainCh1 = 0.182000; shftMainCh1 = 0.268000; pwrMainCh1 = -0.333333;
invMainCh2 = 0.789000; negMainCh2 = -0.501000; shftMainCh2 = 0.972000; pwrMainCh2 = -0.333333;
	

	//                x  y  a,b  0-txtr, 1-txtr1, 2-txtr2
	mtrxMainCh[0][0][0] = 2;
	mtrxMainCh[0][0][1] = 0;
	mtrxMainCh[1][0][0] = 1;
	mtrxMainCh[1][0][1] = 1;
	mtrxMainCh[2][0][0] = 2;
	mtrxMainCh[2][0][1] = 2;

	mtrxMainCh[0][1][0] = 1;
	mtrxMainCh[0][1][1] = 1;
	mtrxMainCh[1][1][0] = 0;
	mtrxMainCh[1][1][1] = 1;
	mtrxMainCh[2][1][0] = 1;
	mtrxMainCh[2][1][1] = 1;

	mtrxMainCh[0][2][0] = 2;
	mtrxMainCh[0][2][1] = 2;
	mtrxMainCh[1][2][0] = 1;
	mtrxMainCh[1][2][1] = 1;
	mtrxMainCh[2][2][0] = 0;
	mtrxMainCh[2][2][1] = 2;




	prefixFN = prefixFN;
	//idFN = 1;
	bFN_gen = bFN_gen;
		
	txtrCellPoints = (9 + (rand() % 5 ))*20;


	char buff[1024];
	sprintf_s(buff,"%s%d.txt",prefixFN,idFN);
	FILE *flog;
	if (fopen_s( &flog, buff, "w" )==0) {
			
	
	fprintf( flog, "prefixFN = %s; idFN = %d;\n", prefixFN, idFN);
	fprintf( flog, "///////////////////////////////////////////");
	fprintf( flog, "iSize = %d;\n", iSize);
	fprintf( flog, "itrBlur = %d;	amountBlur = %d;	beforeBlur = %d;\n ", itrBlur,	amountBlur,	beforeBlur);
	fprintf( flog, "itrBlur0 = %d;	amountBlur0 = %d;	beforeBlur0 = %d;\n ", itrBlur0,	amountBlur0,	beforeBlur0);
	fprintf( flog, "itrBlur1 = %d;	amountBlur1 = %d;	beforeBlur1 = %d;\n ", itrBlur1,	amountBlur1,	beforeBlur1);
	fprintf( flog, "itrBlur2 = %d;	amountBlur2 = %d;	beforeBlur2 = %d;\n\n ", itrBlur2,	amountBlur2,	beforeBlur2);
	
	fprintf( flog, "deltaTxtrSize = %f;	deltaTxtr1Size = %f;	deltaTxtr2Size = %f;\n\n ", deltaTxtrSize,	deltaTxtr1Size,	deltaTxtr2Size);


		fprintf( flog, "invMainCh0 = %f; negMainCh0 = %f; shftMainCh0 = %f; pwrMainCh0 = %f;\n", invMainCh0, negMainCh0, shftMainCh0, pwrMainCh0);
		fprintf( flog, "invMainCh1 = %f; negMainCh1 = %f; shftMainCh1 = %f; pwrMainCh1 = %f;\n", invMainCh1, negMainCh1, shftMainCh1, pwrMainCh1);
		fprintf( flog, "invMainCh2 = %f; negMainCh2 = %f; shftMainCh2 = %f; pwrMainCh2 = %f;\n", invMainCh2, negMainCh2, shftMainCh2, pwrMainCh2);
	
		fprintf( flog, "txtrCellPoints = %d;\n", txtrCellPoints);

					fclose( flog );
											};


	};

	void gen_new_opt_ylz_newbl(void)//_yolloz
	{
	itrBlur=4+0*15;
	amountBlur=8-6;
	beforeBlur=0*5;

	itrBlur0=0;
	amountBlur0=0;
	beforeBlur0=5;//10;//2;

	itrBlur1=0;
	amountBlur1=0;
	beforeBlur1=5;//10;//2;

	itrBlur2=0;
	amountBlur2=0;
	beforeBlur2=5;//10;//2;


	deltaTxtrSize=4*4;
	deltaTxtr1Size=1*4;
	deltaTxtr2Size=3*4;



	invMainCh0 = 1.0f;
	negMainCh0 =   -1.0f;//(rand() % 1000) / 1000.0f - .5f;
	shftMainCh0 =  1.0f;
	if ((rand() % 100) > 33)
	{
		//shftMainCh0 =  0.0f;
		//negMainCh0 =   0.0f;
	}
	pwrMainCh0 = -1.0f / 3.0f+0.0f*((float)((rand() % 100))/100.0f);

	invMainCh1 =  1.0f;
	negMainCh1 =   -1.0f;//(rand() % 1000) / 1000.0f - .5f;
	shftMainCh1 =  1.0f;
	if ((rand() % 100) > 33)
	{
		//shftMainCh1 =  0.0f;
		//negMainCh1 =   0.0f;
	}
	pwrMainCh1 = -1.0f / 3.0f+0.0f*((float)((rand() % 100))/100.0f);


	invMainCh2 =  1.0f;
	negMainCh2 =   1.0f;//(rand() % 1000) / 1000.0f - .5f;
	shftMainCh2 =  0.0f;
	if ((rand() % 100) > 33)
	{
		shftMainCh2 =  0.0f;
		negMainCh2 =   0.0f;
	}
	pwrMainCh2 = -1.0f / 3.0f+0.0f*((float)((rand() % 100))/100.0f);



	/*//1
	invMainCh1 = (rand() % 100) > 19 ? 1.0f : -1.0f;
	negMainCh1 = -1.0f;//(rand() % 1000) / 1000.0f - .5f;
	shftMainCh1 = 1.0f - (rand() % 1000) / 1000.0f;
	pwrMainCh1 = -1.0f / 3.0f;
	*/



	//                x  y  a,b  0-txtr, 1-txtr1, 2-txtr2
	mtrxMainCh[0][0][0] = 2;
	mtrxMainCh[0][0][1] = 0;
	mtrxMainCh[1][0][0] = 1;
	mtrxMainCh[1][0][1] = 1;
	mtrxMainCh[2][0][0] = 2;
	mtrxMainCh[2][0][1] = 2;

	mtrxMainCh[0][1][0] = 1;
	mtrxMainCh[0][1][1] = 1;
	mtrxMainCh[1][1][0] = 0;
	mtrxMainCh[1][1][1] = 1;
	mtrxMainCh[2][1][0] = 1;
	mtrxMainCh[2][1][1] = 1;

	mtrxMainCh[0][2][0] = 2;
	mtrxMainCh[0][2][1] = 2;
	mtrxMainCh[1][2][0] = 1;
	mtrxMainCh[1][2][1] = 1;
	mtrxMainCh[2][2][0] = 0;
	mtrxMainCh[2][2][1] = 2;


	iSize = iSize;//4096;

	prefixFN = prefixFN;
	//idFN = 1;
	bFN_gen = bFN_gen;
		
	txtrCellPoints = (6 + (rand() % 5 ))*100;

	char buff[1024];
	sprintf_s(buff,"%s%d.txt",prefixFN,idFN);
	FILE *flog;
	if (fopen_s( &flog, buff, "w" )==0) {
			
	
	fprintf( flog, "prefixFN = %s; idFN = %d;\n", prefixFN, idFN);
	fprintf( flog, "///////////////////////////////////////////");
	fprintf( flog, "iSize = %d;\n", iSize);
	fprintf( flog, "itrBlur = %d;	amountBlur = %d;	beforeBlur = %d;\n ", itrBlur,	amountBlur,	beforeBlur);
	fprintf( flog, "itrBlur0 = %d;	amountBlur0 = %d;	beforeBlur0 = %d;\n ", itrBlur0,	amountBlur0,	beforeBlur0);
	fprintf( flog, "itrBlur1 = %d;	amountBlur1 = %d;	beforeBlur1 = %d;\n ", itrBlur1,	amountBlur1,	beforeBlur1);
	fprintf( flog, "itrBlur2 = %d;	amountBlur2 = %d;	beforeBlur2 = %d;\n\n ", itrBlur2,	amountBlur2,	beforeBlur2);

	fprintf( flog, "deltaTxtrSize = %f;	deltaTxtr1Size = %f;	deltaTxtr2Size = %f;\n\n ", deltaTxtrSize,	deltaTxtr1Size,	deltaTxtr2Size);

		fprintf( flog, "invMainCh0 = %f; negMainCh0 = %f; shftMainCh0 = %f; pwrMainCh0 = %f;\n", invMainCh0, negMainCh0, shftMainCh0, pwrMainCh0);
		fprintf( flog, "invMainCh1 = %f; negMainCh1 = %f; shftMainCh1 = %f; pwrMainCh1 = %f;\n", invMainCh1, negMainCh1, shftMainCh1, pwrMainCh1);
		fprintf( flog, "invMainCh2 = %f; negMainCh2 = %f; shftMainCh2 = %f; pwrMainCh2 = %f;\n", invMainCh2, negMainCh2, shftMainCh2, pwrMainCh2);
	
		fprintf( flog, "txtrCellPoints = %d;\n", txtrCellPoints);

					fclose( flog );
											};


	};


	void gen_new_opt_zele(void)
	{
	itrBlur=15;
	amountBlur=8;
	beforeBlur=5;

	itrBlur0=0;
	amountBlur0=0;
	beforeBlur0=10;//2;

	itrBlur1=0;
	amountBlur1=0;
	beforeBlur1=10;//2;

	itrBlur2=0;
	amountBlur2=0;
	beforeBlur2=10;//2;


	deltaTxtrSize=4*4;
	deltaTxtr1Size=1*4;
	deltaTxtr2Size=3*4;


	invMainCh0 = (rand() % 2000) / 1000.0f - 1.0f;//1.0f;//(rand() % 100) > 19 ? 3.0f : -1.0f+2.0f+2.0f;
	negMainCh0 = (rand() % 2000) / 1000.0f - 1.0f;
	shftMainCh0 = (rand() % 1000) / 1000.0f;
	pwrMainCh0 = -1.0f / 3.0f;

	invMainCh1 = (rand() % 1000) / 1000.0f - 1.0f;//1.0f;//(rand() % 100) > 19 ? 3.0f : -1.0f+2.0f+2.0f;
	negMainCh1 = (rand() % 2000) / 1000.0f - 1.0f;
	shftMainCh1 = (rand() % 1000) / 1000.0f;
	pwrMainCh1 = -1.0f / 3.0f;

	invMainCh2 =  (rand() % 1000) / 1000.0f - 1.0f;//1.0f;//(rand() % 100) > 19 ? 3.0f : -1.0f+2.0f+2.0f;
	negMainCh2 =  (rand() % 2000) / 1000.0f - 1.0f;
	shftMainCh2 = (rand() % 1000) / 1000.0f;
	pwrMainCh2 = -1.0f / 3.0f;

	

		//meylw-2-58 
		//invMainCh0 = 0.550000; negMainCh0 = 0.535000; shftMainCh0 = 0.260000; pwrMainCh0 = -0.333333;
		//invMainCh1 = 0.707000; negMainCh1 = 0.583000; shftMainCh1 = 0.173000; pwrMainCh1 = -0.333333;
		//invMainCh2 = 0.880000; negMainCh2 = -0.665000; shftMainCh2 = 0.788000; pwrMainCh2 = -0.333333;

	//60
	 //invMainCh0 = 0.932000; negMainCh0 = -0.527000; shftMainCh0 = 0.984000; pwrMainCh0 = -0.333333;
	 //69
	 //invMainCh0 = 0.830000; negMainCh0 = -0.590000; shftMainCh0 = 0.616000; pwrMainCh0 = -0.333333;

	 //61
	  invMainCh0 = -0.549000; negMainCh0 = -0.381000; shftMainCh0 = 0.985000; pwrMainCh0 = -0.333333;

	invMainCh1 = invMainCh0; negMainCh1 = negMainCh0; shftMainCh1 = shftMainCh0; pwrMainCh1 = pwrMainCh0;
	invMainCh2 = invMainCh0; negMainCh2 = negMainCh0; shftMainCh2 = shftMainCh0; pwrMainCh2 = pwrMainCh0;

	/*
	//54
	 invMainCh0 = 0.873000; negMainCh0 = -0.240000; shftMainCh0 = 0.290000; pwrMainCh0 = -0.333333;
	invMainCh1 = 0.873000; negMainCh1 = -0.240000; shftMainCh1 = 0.290000; pwrMainCh1 = -0.333333;
	invMainCh2 = 0.873000; negMainCh2 = -0.240000; shftMainCh2 = 0.290000; pwrMainCh2 = -0.333333;
	txtrCellPoints = 100;

	//48
	 invMainCh0 = 0.893000; negMainCh0 = -0.626000; shftMainCh0 = 0.727000; pwrMainCh0 = -0.333333;
	invMainCh1 = 0.893000; negMainCh1 = -0.626000; shftMainCh1 = 0.727000; pwrMainCh1 = -0.333333;
	invMainCh2 = 0.893000; negMainCh2 = -0.626000; shftMainCh2 = 0.727000; pwrMainCh2 = -0.333333;
	txtrCellPoints = 120;
	//txtrCellPoints = 120;
	*/

	/*
	pwrMainCh1 = pwrMainCh0;
	negMainCh1 = negMainCh0;
	shftMainCh1 = shftMainCh0;
	pwrMainCh1  = pwrMainCh0;


	pwrMainCh2 = pwrMainCh0;
	negMainCh2 = negMainCh0;
	shftMainCh2 = shftMainCh0;
	pwrMainCh2  = pwrMainCh0;
	*/



	//                x  y  a,b  0-txtr, 1-txtr1, 2-txtr2
	mtrxMainCh[0][0][0] = 2;
	mtrxMainCh[0][0][1] = 0;
	mtrxMainCh[1][0][0] = 1;
	mtrxMainCh[1][0][1] = 1;
	mtrxMainCh[2][0][0] = 2;
	mtrxMainCh[2][0][1] = 2;

	mtrxMainCh[0][1][0] = 1;
	mtrxMainCh[0][1][1] = 1;
	mtrxMainCh[1][1][0] = 0;
	mtrxMainCh[1][1][1] = 1;
	mtrxMainCh[2][1][0] = 1;
	mtrxMainCh[2][1][1] = 1;

	mtrxMainCh[0][2][0] = 2;
	mtrxMainCh[0][2][1] = 2;
	mtrxMainCh[1][2][0] = 1;
	mtrxMainCh[1][2][1] = 1;
	mtrxMainCh[2][2][0] = 0;
	mtrxMainCh[2][2][1] = 2;


	iSize = iSize;//4096;

	prefixFN = prefixFN;
	//idFN = 1;
	bFN_gen = bFN_gen;
		
	txtrCellPoints = (9 + (rand() % 5 ))*20;
	
	char buff[1024];
	sprintf_s(buff,"%s%d.txt",prefixFN,idFN);
	FILE *flog;
	if (fopen_s( &flog, buff, "w" )==0) {
			
	
	fprintf( flog, "prefixFN = %s; idFN = %d;\n", prefixFN, idFN);
	fprintf( flog, "///////////////////////////////////////////");
	fprintf( flog, "iSize = %d;\n", iSize);
	fprintf( flog, "itrBlur = %d;	amountBlur = %d;	beforeBlur = %d;\n ", itrBlur,	amountBlur,	beforeBlur);
	fprintf( flog, "itrBlur0 = %d;	amountBlur0 = %d;	beforeBlur0 = %d;\n ", itrBlur0,	amountBlur0,	beforeBlur0);
	fprintf( flog, "itrBlur1 = %d;	amountBlur1 = %d;	beforeBlur1 = %d;\n ", itrBlur1,	amountBlur1,	beforeBlur1);
	fprintf( flog, "itrBlur2 = %d;	amountBlur2 = %d;	beforeBlur2 = %d;\n\n ", itrBlur2,	amountBlur2,	beforeBlur2);

	fprintf( flog, "deltaTxtrSize = %f;	deltaTxtr1Size = %f;	deltaTxtr2Size = %f;\n\n ", deltaTxtrSize,	deltaTxtr1Size,	deltaTxtr2Size);


		fprintf( flog, "invMainCh0 = %f; negMainCh0 = %f; shftMainCh0 = %f; pwrMainCh0 = %f;\n", invMainCh0, negMainCh0, shftMainCh0, pwrMainCh0);
		fprintf( flog, "invMainCh1 = %f; negMainCh1 = %f; shftMainCh1 = %f; pwrMainCh1 = %f;\n", invMainCh1, negMainCh1, shftMainCh1, pwrMainCh1);
		fprintf( flog, "invMainCh2 = %f; negMainCh2 = %f; shftMainCh2 = %f; pwrMainCh2 = %f;\n", invMainCh2, negMainCh2, shftMainCh2, pwrMainCh2);
	
		fprintf( flog, "txtrCellPoints = %d;\n", txtrCellPoints);

					fclose( flog );
											};
	};


	void gen_new_opt_marmor(void)
	{
	itrBlur=15;
	amountBlur=8;
	beforeBlur=5;

	itrBlur0=0;
	amountBlur0=0;
	beforeBlur0=10;//2;

	itrBlur1=0;
	amountBlur1=0;
	beforeBlur1=10;//2;

	itrBlur2=0;
	amountBlur2=0;
	beforeBlur2=10;//2;


	deltaTxtrSize=4*8;
	deltaTxtr1Size=1*8;
	deltaTxtr2Size=3*8;


	invMainCh0 = (rand() % 2000) / 1000.0f - 1.0f;//1.0f;//(rand() % 100) > 19 ? 3.0f : -1.0f+2.0f+2.0f;
	negMainCh0 = (rand() % 2000) / 1000.0f - 1.0f;
	shftMainCh0 = (rand() % 1000) / 1000.0f;
	pwrMainCh0 = -1.0f / 3.0f;

	invMainCh1 = (rand() % 1000) / 1000.0f - 1.0f;//1.0f;//(rand() % 100) > 19 ? 3.0f : -1.0f+2.0f+2.0f;
	negMainCh1 = (rand() % 2000) / 1000.0f - 1.0f;
	shftMainCh1 = (rand() % 1000) / 1000.0f;
	pwrMainCh1 = -1.0f / 3.0f;

	invMainCh2 =  (rand() % 1000) / 1000.0f - 1.0f;//1.0f;//(rand() % 100) > 19 ? 3.0f : -1.0f+2.0f+2.0f;
	negMainCh2 =  (rand() % 2000) / 1000.0f - 1.0f;
	shftMainCh2 = (rand() % 1000) / 1000.0f;
	pwrMainCh2 = -1.0f / 3.0f;

	

		//meylw-2-58 
		//invMainCh0 = 0.550000; negMainCh0 = 0.535000; shftMainCh0 = 0.260000; pwrMainCh0 = -0.333333;
		//invMainCh1 = 0.707000; negMainCh1 = 0.583000; shftMainCh1 = 0.173000; pwrMainCh1 = -0.333333;
		//invMainCh2 = 0.880000; negMainCh2 = -0.665000; shftMainCh2 = 0.788000; pwrMainCh2 = -0.333333;

	//60
	 invMainCh0 = 0.932000; negMainCh0 = -0.527000; shftMainCh0 = 0.984000; pwrMainCh0 = -0.333333;
	 //69
	 //invMainCh0 = 0.830000; negMainCh0 = -0.590000; shftMainCh0 = 0.616000; pwrMainCh0 = -0.333333;

	invMainCh1 = invMainCh0; negMainCh1 = negMainCh0; shftMainCh1 = shftMainCh0; pwrMainCh1 = pwrMainCh0;
	invMainCh2 = invMainCh0; negMainCh2 = negMainCh0; shftMainCh2 = shftMainCh0; pwrMainCh2 = pwrMainCh0;

	/*
	//54
	 invMainCh0 = 0.873000; negMainCh0 = -0.240000; shftMainCh0 = 0.290000; pwrMainCh0 = -0.333333;
	invMainCh1 = 0.873000; negMainCh1 = -0.240000; shftMainCh1 = 0.290000; pwrMainCh1 = -0.333333;
	invMainCh2 = 0.873000; negMainCh2 = -0.240000; shftMainCh2 = 0.290000; pwrMainCh2 = -0.333333;
	txtrCellPoints = 100;

	//48
	 invMainCh0 = 0.893000; negMainCh0 = -0.626000; shftMainCh0 = 0.727000; pwrMainCh0 = -0.333333;
	invMainCh1 = 0.893000; negMainCh1 = -0.626000; shftMainCh1 = 0.727000; pwrMainCh1 = -0.333333;
	invMainCh2 = 0.893000; negMainCh2 = -0.626000; shftMainCh2 = 0.727000; pwrMainCh2 = -0.333333;
	txtrCellPoints = 120;
	//txtrCellPoints = 120;
	*/

	/*
	pwrMainCh1 = pwrMainCh0;
	negMainCh1 = negMainCh0;
	shftMainCh1 = shftMainCh0;
	pwrMainCh1  = pwrMainCh0;


	pwrMainCh2 = pwrMainCh0;
	negMainCh2 = negMainCh0;
	shftMainCh2 = shftMainCh0;
	pwrMainCh2  = pwrMainCh0;
	*/



	//                x  y  a,b  0-txtr, 1-txtr1, 2-txtr2
	mtrxMainCh[0][0][0] = 2;
	mtrxMainCh[0][0][1] = 0;
	mtrxMainCh[1][0][0] = 1;
	mtrxMainCh[1][0][1] = 1;
	mtrxMainCh[2][0][0] = 2;
	mtrxMainCh[2][0][1] = 2;

	mtrxMainCh[0][1][0] = 1;
	mtrxMainCh[0][1][1] = 1;
	mtrxMainCh[1][1][0] = 0;
	mtrxMainCh[1][1][1] = 1;
	mtrxMainCh[2][1][0] = 1;
	mtrxMainCh[2][1][1] = 1;

	mtrxMainCh[0][2][0] = 2;
	mtrxMainCh[0][2][1] = 2;
	mtrxMainCh[1][2][0] = 1;
	mtrxMainCh[1][2][1] = 1;
	mtrxMainCh[2][2][0] = 0;
	mtrxMainCh[2][2][1] = 2;


	iSize = iSize;//4096;

	prefixFN = prefixFN;
	//idFN = 1;
	bFN_gen = bFN_gen;
		
	txtrCellPoints = (9 + (rand() % 5 ))*10;
	
	char buff[1024];
	sprintf_s(buff,"%s%d.txt",prefixFN,idFN);
	FILE *flog;
	if (fopen_s( &flog, buff, "w" )==0) {
			
	
	fprintf( flog, "prefixFN = %s; idFN = %d;\n", prefixFN, idFN);
	fprintf( flog, "///////////////////////////////////////////");
	fprintf( flog, "iSize = %d;\n", iSize);
	fprintf( flog, "itrBlur = %d;	amountBlur = %d;	beforeBlur = %d;\n ", itrBlur,	amountBlur,	beforeBlur);
	fprintf( flog, "itrBlur0 = %d;	amountBlur0 = %d;	beforeBlur0 = %d;\n ", itrBlur0,	amountBlur0,	beforeBlur0);
	fprintf( flog, "itrBlur1 = %d;	amountBlur1 = %d;	beforeBlur1 = %d;\n ", itrBlur1,	amountBlur1,	beforeBlur1);
	fprintf( flog, "itrBlur2 = %d;	amountBlur2 = %d;	beforeBlur2 = %d;\n\n ", itrBlur2,	amountBlur2,	beforeBlur2);
	

		fprintf( flog, "invMainCh0 = %f; negMainCh0 = %f; shftMainCh0 = %f; pwrMainCh0 = %f;\n", invMainCh0, negMainCh0, shftMainCh0, pwrMainCh0);
		fprintf( flog, "invMainCh1 = %f; negMainCh1 = %f; shftMainCh1 = %f; pwrMainCh1 = %f;\n", invMainCh1, negMainCh1, shftMainCh1, pwrMainCh1);
		fprintf( flog, "invMainCh2 = %f; negMainCh2 = %f; shftMainCh2 = %f; pwrMainCh2 = %f;\n", invMainCh2, negMainCh2, shftMainCh2, pwrMainCh2);
	
		fprintf( flog, "txtrCellPoints = %d;\n", txtrCellPoints);

					fclose( flog );
											};
	};

	void gen_new_opt_mramor(void)
	{
	itrBlur=15;
	amountBlur=8;
	beforeBlur=5;

	itrBlur0=0;
	amountBlur0=0;
	beforeBlur0=10;

	itrBlur1=0;
	amountBlur1=0;
	beforeBlur1=10;

	itrBlur2=0;
	amountBlur2=0;
	beforeBlur2=10;





	invMainCh0 = (rand() % 2000) / 1000.0f - 1.0f;//1.0f;//(rand() % 100) > 19 ? 3.0f : -1.0f+2.0f+2.0f;
	negMainCh0 = (rand() % 2000) / 1000.0f - 1.0f;
	shftMainCh0 = (rand() % 1000) / 1000.0f;
	pwrMainCh0 = -1.0f / 3.0f;

	invMainCh1 = (rand() % 1000) / 1000.0f - 1.0f;//1.0f;//(rand() % 100) > 19 ? 3.0f : -1.0f+2.0f+2.0f;
	negMainCh1 = (rand() % 2000) / 1000.0f - 1.0f;
	shftMainCh1 = (rand() % 1000) / 1000.0f;
	pwrMainCh1 = -1.0f / 3.0f;

	invMainCh2 =  (rand() % 1000) / 1000.0f - 1.0f;//1.0f;//(rand() % 100) > 19 ? 3.0f : -1.0f+2.0f+2.0f;
	negMainCh2 =  (rand() % 2000) / 1000.0f - 1.0f;
	shftMainCh2 = (rand() % 1000) / 1000.0f;
	pwrMainCh2 = -1.0f / 3.0f;

	

		//meylw-2-58 
		//invMainCh0 = 0.550000; negMainCh0 = 0.535000; shftMainCh0 = 0.260000; pwrMainCh0 = -0.333333;
		//invMainCh1 = 0.707000; negMainCh1 = 0.583000; shftMainCh1 = 0.173000; pwrMainCh1 = -0.333333;
		//invMainCh2 = 0.880000; negMainCh2 = -0.665000; shftMainCh2 = 0.788000; pwrMainCh2 = -0.333333;

	//60
	 invMainCh0 = 0.932000; negMainCh0 = -0.527000; shftMainCh0 = 0.984000; pwrMainCh0 = -0.333333;
	 //69
	 //invMainCh0 = 0.830000; negMainCh0 = -0.590000; shftMainCh0 = 0.616000; pwrMainCh0 = -0.333333;

	invMainCh1 = invMainCh0; negMainCh1 = negMainCh0; shftMainCh1 = shftMainCh0; pwrMainCh1 = pwrMainCh0;
	invMainCh2 = invMainCh0; negMainCh2 = negMainCh0; shftMainCh2 = shftMainCh0; pwrMainCh2 = pwrMainCh0;

	/*
	//54
	 invMainCh0 = 0.873000; negMainCh0 = -0.240000; shftMainCh0 = 0.290000; pwrMainCh0 = -0.333333;
	invMainCh1 = 0.873000; negMainCh1 = -0.240000; shftMainCh1 = 0.290000; pwrMainCh1 = -0.333333;
	invMainCh2 = 0.873000; negMainCh2 = -0.240000; shftMainCh2 = 0.290000; pwrMainCh2 = -0.333333;
	txtrCellPoints = 100;

	//48
	 invMainCh0 = 0.893000; negMainCh0 = -0.626000; shftMainCh0 = 0.727000; pwrMainCh0 = -0.333333;
	invMainCh1 = 0.893000; negMainCh1 = -0.626000; shftMainCh1 = 0.727000; pwrMainCh1 = -0.333333;
	invMainCh2 = 0.893000; negMainCh2 = -0.626000; shftMainCh2 = 0.727000; pwrMainCh2 = -0.333333;
	txtrCellPoints = 120;
	//txtrCellPoints = 120;
	*/

	/*
	pwrMainCh1 = pwrMainCh0;
	negMainCh1 = negMainCh0;
	shftMainCh1 = shftMainCh0;
	pwrMainCh1  = pwrMainCh0;


	pwrMainCh2 = pwrMainCh0;
	negMainCh2 = negMainCh0;
	shftMainCh2 = shftMainCh0;
	pwrMainCh2  = pwrMainCh0;
	*/



	//                x  y  a,b  0-txtr, 1-txtr1, 2-txtr2
	mtrxMainCh[0][0][0] = 2;
	mtrxMainCh[0][0][1] = 0;
	mtrxMainCh[1][0][0] = 1;
	mtrxMainCh[1][0][1] = 1;
	mtrxMainCh[2][0][0] = 2;
	mtrxMainCh[2][0][1] = 2;

	mtrxMainCh[0][1][0] = 1;
	mtrxMainCh[0][1][1] = 1;
	mtrxMainCh[1][1][0] = 0;
	mtrxMainCh[1][1][1] = 1;
	mtrxMainCh[2][1][0] = 1;
	mtrxMainCh[2][1][1] = 1;

	mtrxMainCh[0][2][0] = 2;
	mtrxMainCh[0][2][1] = 2;
	mtrxMainCh[1][2][0] = 1;
	mtrxMainCh[1][2][1] = 1;
	mtrxMainCh[2][2][0] = 0;
	mtrxMainCh[2][2][1] = 2;


	iSize = iSize;//4096;

	prefixFN = prefixFN;
	//idFN = 1;
	bFN_gen = bFN_gen;
		
	txtrCellPoints = (9 + (rand() % 5 ))*10;
	
	char buff[1024];
	sprintf_s(buff,"%s%d.txt",prefixFN,idFN);
	FILE *flog;
	if (fopen_s( &flog, buff, "w" )==0) {
			
	
	fprintf( flog, "prefixFN = %s; idFN = %d;\n", prefixFN, idFN);
	fprintf( flog, "///////////////////////////////////////////");
	fprintf( flog, "iSize = %d;\n", iSize);
	fprintf( flog, "itrBlur = %d;	amountBlur = %d;	beforeBlur = %d;\n ", itrBlur,	amountBlur,	beforeBlur);
	fprintf( flog, "itrBlur0 = %d;	amountBlur0 = %d;	beforeBlur0 = %d;\n ", itrBlur0,	amountBlur0,	beforeBlur0);
	fprintf( flog, "itrBlur1 = %d;	amountBlur1 = %d;	beforeBlur1 = %d;\n ", itrBlur1,	amountBlur1,	beforeBlur1);
	fprintf( flog, "itrBlur2 = %d;	amountBlur2 = %d;	beforeBlur2 = %d;\n\n ", itrBlur2,	amountBlur2,	beforeBlur2);
	

		fprintf( flog, "invMainCh0 = %f; negMainCh0 = %f; shftMainCh0 = %f; pwrMainCh0 = %f;\n", invMainCh0, negMainCh0, shftMainCh0, pwrMainCh0);
		fprintf( flog, "invMainCh1 = %f; negMainCh1 = %f; shftMainCh1 = %f; pwrMainCh1 = %f;\n", invMainCh1, negMainCh1, shftMainCh1, pwrMainCh1);
		fprintf( flog, "invMainCh2 = %f; negMainCh2 = %f; shftMainCh2 = %f; pwrMainCh2 = %f;\n", invMainCh2, negMainCh2, shftMainCh2, pwrMainCh2);
	
		fprintf( flog, "txtrCellPoints = %d;\n", txtrCellPoints);

					fclose( flog );
											};
	};


	
	void gen_new_opt_divex(void)
	{
	itrBlur=40;
	amountBlur=8;

	invMainCh0 = -1.0f;
	negMainCh0 =  1.0f;//(rand() % 1000) / 1000.0f - .5f;
	shftMainCh0 =  0.0f;
	if ((rand() % 100) > 33)
	{
		//shftMainCh0 =  0.0f;
		//negMainCh0 =   0.0f;
	}
	pwrMainCh0 = -1.0f / 3.0f+0.0f*((float)((rand() % 100))/100.0f);


	invMainCh1 = -1.0f;
	negMainCh1 =   -0.3f;//(rand() % 1000) / 1000.0f - .5f;
	shftMainCh1 =  0.0f;
	if ((rand() % 100) > 33)
	{
		//shftMainCh1 =  0.0f;
		//negMainCh1 =   0.0f;
	}
	pwrMainCh1 = -1.0f / 3.0f+0.0f*((float)((rand() % 100))/100.0f);


	invMainCh2 = -1.0f;
	negMainCh2 =  -0.1f;//(rand() % 1000) / 1000.0f - .5f;

	//float d12 =  (rand() % 2000 ) / 2000.0f;//
	//negMainCh0 /= 10.0f;
	//negMainCh1  *= d12;
	//negMainCh2  *= d12;
	if (mabs(negMainCh0)>0.1f) negMainCh0 =  1 / negMainCh0;
	if (mabs(negMainCh1)>0.1f) negMainCh1 =  1 / negMainCh1;
	if (mabs(negMainCh2)>0.1f) negMainCh2 =  1 / negMainCh2;
	shftMainCh2 =  0.0f;
	if ((rand() % 100) > 33)
	{
		//shftMainCh2 =  0.0f;
		//negMainCh2 =   0.0f;
	}
	pwrMainCh2 = -1.0f / 3.0f+0.0f*((float)((rand() % 100))/100.0f);


	/*//1
	invMainCh1 = (rand() % 100) > 19 ? 1.0f : -1.0f;
	negMainCh1 = -1.0f;//(rand() % 1000) / 1000.0f - .5f;
	shftMainCh1 = 1.0f - (rand() % 1000) / 1000.0f;
	pwrMainCh1 = -1.0f / 3.0f;
	*/



	//                x  y  a,b  0-txtr, 1-txtr1, 2-txtr2
	mtrxMainCh[0][0][0] = 2;
	mtrxMainCh[0][0][1] = 0;
	mtrxMainCh[1][0][0] = 1;
	mtrxMainCh[1][0][1] = 1;
	mtrxMainCh[2][0][0] = 2;
	mtrxMainCh[2][0][1] = 2;

	mtrxMainCh[0][1][0] = 1;
	mtrxMainCh[0][1][1] = 1;
	mtrxMainCh[1][1][0] = 0;
	mtrxMainCh[1][1][1] = 1;
	mtrxMainCh[2][1][0] = 1;
	mtrxMainCh[2][1][1] = 1;

	mtrxMainCh[0][2][0] = 2;
	mtrxMainCh[0][2][1] = 2;
	mtrxMainCh[1][2][0] = 1;
	mtrxMainCh[1][2][1] = 1;
	mtrxMainCh[2][2][0] = 0;
	mtrxMainCh[2][2][1] = 2;


	iSize = iSize;//4096;

	prefixFN = prefixFN;
	//idFN = 1;
	bFN_gen = bFN_gen;
		
	txtrCellPoints = (9 + (rand() % 5 ))*10;

	};
	
	void gen_new_opt_reddy(void)
	{
	itrBlur=4;
	amountBlur=8;

	invMainCh0 = -1.0f;
	negMainCh0 =  0.350f;//(rand() % 1000) / 1000.0f - .5f;
	shftMainCh0 =  0.0f;
	if ((rand() % 100) > 33)
	{
		//shftMainCh0 =  0.0f;
		//negMainCh0 =   0.0f;
	}
	pwrMainCh0 = -1.0f / 3.0f+0.0f*((float)((rand() % 100))/100.0f);


	invMainCh1 = -1.0f;
	negMainCh1 =   0.3f;//(rand() % 1000) / 1000.0f - .5f;
	shftMainCh1 =  0.0f;
	if ((rand() % 100) > 33)
	{
		//shftMainCh1 =  0.0f;
		//negMainCh1 =   0.0f;
	}
	pwrMainCh1 = -1.0f / 3.0f+0.0f*((float)((rand() % 100))/100.0f);


	invMainCh2 = -1.0f;
	negMainCh2 =  0.1f;//(rand() % 1000) / 1000.0f - .5f;

	float d12 =  (rand() % 2000 ) / 2000.0f;//
	//negMainCh0 /= 10.0f;
	negMainCh1  *= d12;
	negMainCh2  *= d12;

	shftMainCh2 =  0.0f;
	if ((rand() % 100) > 33)
	{
		//shftMainCh2 =  0.0f;
		//negMainCh2 =   0.0f;
	}
	pwrMainCh2 = -1.0f / 3.0f+0.0f*((float)((rand() % 100))/100.0f);


	/*//1
	invMainCh1 = (rand() % 100) > 19 ? 1.0f : -1.0f;
	negMainCh1 = -1.0f;//(rand() % 1000) / 1000.0f - .5f;
	shftMainCh1 = 1.0f - (rand() % 1000) / 1000.0f;
	pwrMainCh1 = -1.0f / 3.0f;
	*/



	//                x  y  a,b  0-txtr, 1-txtr1, 2-txtr2
	mtrxMainCh[0][0][0] = 2;
	mtrxMainCh[0][0][1] = 0;
	mtrxMainCh[1][0][0] = 1;
	mtrxMainCh[1][0][1] = 1;
	mtrxMainCh[2][0][0] = 2;
	mtrxMainCh[2][0][1] = 2;

	mtrxMainCh[0][1][0] = 1;
	mtrxMainCh[0][1][1] = 1;
	mtrxMainCh[1][1][0] = 0;
	mtrxMainCh[1][1][1] = 1;
	mtrxMainCh[2][1][0] = 1;
	mtrxMainCh[2][1][1] = 1;

	mtrxMainCh[0][2][0] = 2;
	mtrxMainCh[0][2][1] = 2;
	mtrxMainCh[1][2][0] = 1;
	mtrxMainCh[1][2][1] = 1;
	mtrxMainCh[2][2][0] = 0;
	mtrxMainCh[2][2][1] = 2;


	iSize = iSize;//4096;

	prefixFN = prefixFN;
	//idFN = 1;
	bFN_gen = bFN_gen;
		
	txtrCellPoints = (9 + (rand() % 5 ))*10;

	};


	void gen_new_opt_bluegreen(void)
	{
	invMainCh0 = -1.0f;
	negMainCh0 =   -1.0f;//(rand() % 1000) / 1000.0f - .5f;
	shftMainCh0 =  0.0f;
	if ((rand() % 100) > 33)
	{
		//shftMainCh0 =  0.0f;
		//negMainCh0 =   0.0f;
	}
	pwrMainCh0 = -1.0f / 3.0f+0.0f*((float)((rand() % 100))/100.0f);


	invMainCh1 = 1.0f;
	negMainCh1 =   1.0f;//(rand() % 1000) / 1000.0f - .5f;
	shftMainCh1 =  0.0f;
	if ((rand() % 100) > 33)
	{
		//shftMainCh1 =  0.0f;
		//negMainCh1 =   0.0f;
	}
	pwrMainCh1 = -1.0f / 3.0f+0.0f*((float)((rand() % 100))/100.0f);


	invMainCh2 = 1.0f;
	negMainCh2 =   1.0f;//(rand() % 1000) / 1000.0f - .5f;
	shftMainCh2 =  0.0f;
	if ((rand() % 100) > 33)
	{
		//shftMainCh2 =  0.0f;
		//negMainCh2 =   0.0f;
	}
	pwrMainCh2 = -1.0f / 3.0f+0.0f*((float)((rand() % 100))/100.0f);


	/*//1
	invMainCh1 = (rand() % 100) > 19 ? 1.0f : -1.0f;
	negMainCh1 = -1.0f;//(rand() % 1000) / 1000.0f - .5f;
	shftMainCh1 = 1.0f - (rand() % 1000) / 1000.0f;
	pwrMainCh1 = -1.0f / 3.0f;
	*/



	//                x  y  a,b  0-txtr, 1-txtr1, 2-txtr2
	mtrxMainCh[0][0][0] = 2;
	mtrxMainCh[0][0][1] = 0;
	mtrxMainCh[1][0][0] = 1;
	mtrxMainCh[1][0][1] = 1;
	mtrxMainCh[2][0][0] = 2;
	mtrxMainCh[2][0][1] = 2;

	mtrxMainCh[0][1][0] = 1;
	mtrxMainCh[0][1][1] = 1;
	mtrxMainCh[1][1][0] = 0;
	mtrxMainCh[1][1][1] = 1;
	mtrxMainCh[2][1][0] = 1;
	mtrxMainCh[2][1][1] = 1;

	mtrxMainCh[0][2][0] = 2;
	mtrxMainCh[0][2][1] = 2;
	mtrxMainCh[1][2][0] = 1;
	mtrxMainCh[1][2][1] = 1;
	mtrxMainCh[2][2][0] = 0;
	mtrxMainCh[2][2][1] = 2;


	iSize = iSize;//4096;

	prefixFN = prefixFN;
	//idFN = 1;
	bFN_gen = bFN_gen;
		
	txtrCellPoints = (6 + (rand() % 5 ))*100;

	};

	void gen_new_opt_kolokao(void)//on Hetz
	{
	itrBlur=4;
	amountBlur=8;

	invMainCh0 = -1.0f;
	negMainCh2 =  0.6f;//*(rand() % 1000) / 1000.0f;//0.6f;//-1.0f;//(rand() % 1000) / 1000.0f - .5f;
	shftMainCh0 =  0.0f;
	if ((rand() % 100) > 33)
	{
		//shftMainCh0 =  0.0f;
		//negMainCh0 =   0.0f;
	}
	pwrMainCh0 = -1.0f / 3.0f+0.0f*((float)((rand() % 100))/100.0f);


	invMainCh1 = -1.0f;
	negMainCh1 =   (rand() % (1000-(int)(negMainCh0*1000))) / 1000.0f;//0.3f;//(rand() % 1000) / 1000.0f - .5f;
	shftMainCh1 =  0.0f;
	if ((rand() % 100) > 33)
	{
		//shftMainCh1 =  0.0f;
		//negMainCh1 =   0.0f;
	}
	pwrMainCh1 = -1.0f / 3.0f+0.0f*((float)((rand() % 100))/100.0f);


	invMainCh2 = -1.0f;
	negMainCh0 =  1.0f - negMainCh0 - negMainCh1; //0.1f;//(rand() % 1000) / 1000.0f - .5f;

	negMainCh0 /= 10.0f;
	negMainCh1  /= 10.3330f;
	negMainCh2  /= 10.0f;

	shftMainCh2 =  0.0f;
	if ((rand() % 100) > 33)
	{
		//shftMainCh2 =  0.0f;
		//negMainCh2 =   0.0f;
	}
	pwrMainCh2 = -1.0f / 3.0f+0.0f*((float)((rand() % 100))/100.0f);


	/*//1
	invMainCh1 = (rand() % 100) > 19 ? 1.0f : -1.0f;
	negMainCh1 = -1.0f;//(rand() % 1000) / 1000.0f - .5f;
	shftMainCh1 = 1.0f - (rand() % 1000) / 1000.0f;
	pwrMainCh1 = -1.0f / 3.0f;
	*/



	//                x  y  a,b  0-txtr, 1-txtr1, 2-txtr2
	mtrxMainCh[0][0][0] = 2;
	mtrxMainCh[0][0][1] = 0;
	mtrxMainCh[1][0][0] = 1;
	mtrxMainCh[1][0][1] = 1;
	mtrxMainCh[2][0][0] = 2;
	mtrxMainCh[2][0][1] = 2;

	mtrxMainCh[0][1][0] = 1;
	mtrxMainCh[0][1][1] = 1;
	mtrxMainCh[1][1][0] = 0;
	mtrxMainCh[1][1][1] = 1;
	mtrxMainCh[2][1][0] = 1;
	mtrxMainCh[2][1][1] = 1;

	mtrxMainCh[0][2][0] = 2;
	mtrxMainCh[0][2][1] = 2;
	mtrxMainCh[1][2][0] = 1;
	mtrxMainCh[1][2][1] = 1;
	mtrxMainCh[2][2][0] = 0;
	mtrxMainCh[2][2][1] = 2;


	iSize = iSize;//4096;

	prefixFN = prefixFN;
	//idFN = 1;
	bFN_gen = bFN_gen;
		
	txtrCellPoints = (9 + (rand() % 5 ))*10;

	};

	void gen_new_opt_clsl(void)
	{
	invMainCh0 = -1.0f;
	negMainCh0 =   -1.0f;//(rand() % 1000) / 1000.0f - .5f;
	shftMainCh0 =  0.0f;
	if ((rand() % 100) > 33)
	{
		//shftMainCh0 =  0.0f;
		//negMainCh0 =   0.0f;
	}
	pwrMainCh0 = -1.0f / 3.0f+0.0f*((float)((rand() % 100))/100.0f);


	invMainCh1 = -1.0f;
	negMainCh1 =   0.03f;//(rand() % 1000) / 1000.0f - .5f;
	shftMainCh1 =  0.0f;
	if ((rand() % 100) > 33)
	{
		//shftMainCh1 =  0.0f;
		//negMainCh1 =   0.0f;
	}
	pwrMainCh1 = -1.0f / 3.0f+0.0f*((float)((rand() % 100))/100.0f);


	invMainCh2 = -1.0f;
	negMainCh2 =   0.01f;//(rand() % 1000) / 1000.0f - .5f;
	shftMainCh2 =  0.0f;
	if ((rand() % 100) > 33)
	{
		//shftMainCh2 =  0.0f;
		//negMainCh2 =   0.0f;
	}
	pwrMainCh2 = -1.0f / 3.0f+0.0f*((float)((rand() % 100))/100.0f);


	/*//1
	invMainCh1 = (rand() % 100) > 19 ? 1.0f : -1.0f;
	negMainCh1 = -1.0f;//(rand() % 1000) / 1000.0f - .5f;
	shftMainCh1 = 1.0f - (rand() % 1000) / 1000.0f;
	pwrMainCh1 = -1.0f / 3.0f;
	*/



	//                x  y  a,b  0-txtr, 1-txtr1, 2-txtr2
	mtrxMainCh[0][0][0] = 2;
	mtrxMainCh[0][0][1] = 0;
	mtrxMainCh[1][0][0] = 1;
	mtrxMainCh[1][0][1] = 1;
	mtrxMainCh[2][0][0] = 2;
	mtrxMainCh[2][0][1] = 2;

	mtrxMainCh[0][1][0] = 1;
	mtrxMainCh[0][1][1] = 1;
	mtrxMainCh[1][1][0] = 0;
	mtrxMainCh[1][1][1] = 1;
	mtrxMainCh[2][1][0] = 1;
	mtrxMainCh[2][1][1] = 1;

	mtrxMainCh[0][2][0] = 2;
	mtrxMainCh[0][2][1] = 2;
	mtrxMainCh[1][2][0] = 1;
	mtrxMainCh[1][2][1] = 1;
	mtrxMainCh[2][2][0] = 0;
	mtrxMainCh[2][2][1] = 2;


	iSize = iSize;//4096;

	prefixFN = prefixFN;
	//idFN = 1;
	bFN_gen = bFN_gen;
		
	txtrCellPoints = (6 + (rand() % 5 ))*100;

	};
	

	void reconstrFN(void) {
		wsprintf(wFN,L"%S%d.png",prefixFN,idFN);
		return;
							}

	void generateFN(void) {
		if (bFN_gen)
		wsprintf(wFN,L"%S%d.png",prefixFN, idFN); //if incremment used
		else
		wsprintf(wFN,L"%S.png",prefixFN); //if not (for static cadre)
		idFN++; //generate new id all the time (need for animatio)
		return;

							}

	void generateLogFile(void)
	{
	char buff[1024];
	sprintf_s(buff,"%s%d.txt",prefixFN,idFN);
	FILE *flog;
	if (fopen_s( &flog, buff, "w" )==0) {
			
	
	fprintf( flog, "prefixFN = %s; idFN = %d;\n\n", prefixFN, idFN);
	fprintf( flog, "Image size (X*Y) = (%d x %d)\n", iSize*pixelate_x, iSize*pixelate_y);
	fprintf( flog, "///////////////////////////////////////////\n");
	fprintf( flog, "iSize = %d;\n", iSize);
	fprintf( flog, "itrBlur = %d;	amountBlur = %d;	beforeBlur = %d;\n ", itrBlur,	amountBlur,	beforeBlur);
	fprintf( flog, "itrBlur0 = %d;	amountBlur0 = %d;	beforeBlur0 = %d;\n ", itrBlur0,	amountBlur0,	beforeBlur0);
	fprintf( flog, "itrBlur1 = %d;	amountBlur1 = %d;	beforeBlur1 = %d;\n ", itrBlur1,	amountBlur1,	beforeBlur1);
	fprintf( flog, "itrBlur2 = %d;	amountBlur2 = %d;	beforeBlur2 = %d;\n\n ", itrBlur2,	amountBlur2,	beforeBlur2);
	
	fprintf( flog, "deltaTxtrSize = %f;	deltaTxtr1Size = %f;	deltaTxtr2Size = %f;\n\n ", deltaTxtrSize,	deltaTxtr1Size,	deltaTxtr2Size);


		fprintf( flog, "invMainCh0 = %f; negMainCh0 = %f; shftMainCh0 = %f; pwrMainCh0 = %f;\n", invMainCh0, negMainCh0, shftMainCh0, pwrMainCh0);
		fprintf( flog, "invMainCh1 = %f; negMainCh1 = %f; shftMainCh1 = %f; pwrMainCh1 = %f;\n", invMainCh1, negMainCh1, shftMainCh1, pwrMainCh1);
		fprintf( flog, "invMainCh2 = %f; negMainCh2 = %f; shftMainCh2 = %f; pwrMainCh2 = %f;\n", invMainCh2, negMainCh2, shftMainCh2, pwrMainCh2);
	
		fprintf( flog, "txtrCellPoints = %d;\n", txtrCellPoints);

		//////prints the Cellular techniq
		 char* ts;		
		 switch(txtrCellTechniq)	{
			case ct_nearest: ts="ct_nearest"; break;
			case ct_diffntl_nrst: ts="ct_diffntl_nrst"; break;
			case ct_diffntl2_nrst: ts="ct_diffntl2_nrst"; break;
			case ct_diffntl5_nrst:	ts="ct_diffntl5_nrst"; break;
			case ct_diffntl2_nrst_quad:	ts="ct_diffntl2_nrst_quad"; break;
			case ct_folliage: ts="ct_folliage"; break;	
			default: ts="unknown";	};
		fprintf( flog, "txtrCellTechniq = %s;\n", ts);
		//////prints the Cellular techniq


		//////prints the Material 1 Techniq
		 char* mt;		
		 switch(mtrl1MatTecniq)	{
			case mtrl_1_witrage: mt="mtrl_1_witrage //first tehnick - witrage, stucco, color smoke plams"; break;
			case mtrl_1_crstll_marble_sky: mt="mtrl_1_crstll_marble_sky //second tehnick - cristalls with inner light, marble, sky"; break;
			default: mt="unknown";	};
		fprintf( flog, "mtrl1MatTecniq = %s;\n", mt);
		//////prints the Material 1 Techniq


		fprintf( flog, "randSeed = %d;\n", randSeed);
		fprintf( flog, "brandSeed = %s;\n", brandSeed ? "True" : "False");
		fprintf( flog, "bFN_gen	= %s; ", bFN_gen ? "True" : "False");
		fprintf( flog, "//if true generate number for filename incremmentally\n");
		fprintf( flog, "WCHAR wFN[200]= \"%S\";\n",wFN);

		fprintf( flog, "\n\n//int mtrxMainCh[3][3][2];//matrix of showinng textures.. X,Y, a and b. where 0-txtr, 1-txtr1, 2-txtr2;\n");
		for (int i=0; i<3;i++) {
					for (int j=0; j<3;j++)
						fprintf( flog, "mtrxMainCh[%d][%d][0] = %d; mtrxMainCh[%d][%d][1] = %d; ",j,i,mtrxMainCh[j][i][0],j,i,mtrxMainCh[j][i][1]);
					fprintf( flog, "\n");
		}
		//[2][0] [1][1] [2][2]
		//[2][2] [0][1] [2][2]
		//[2][2] [1][1] [0][2]


		fprintf( flog, "\npixelate_x=%d; pixelate_y=%d;//x- and y-integer magnifickator by zoom  the outgoing picture \n\n",pixelate_x,pixelate_y);
		
		fprintf( flog, " /////////////////////////////////////////////////////////////////////\n");
		fprintf( flog, " //is animation mode on? Where the texture generators\n");
		fprintf( flog, " //turns only one time by the first reneder, then change it parameters\n");
		fprintf( flog, "bAnimOn	= %s; ", bAnimOn ? "True" : "False");
		/*
		for(int k=0; k<322;k++) 
			fprintf( flog, "animated_%d.png\n",k);

		for(int k=322; k>=0;k--) 
			fprintf( flog, "animated_%d.png\n",k);
			*/
					fclose( flog );
					}
	}



		enum iflCommander {iflPlain,iflPingPong};//command and generator for animation lists
		void generateIFLFile(int num, char* listfname, char* bitmapfname, iflCommander pCmnd=iflPlain)
		{
	char buff[1024];
	sprintf_s(buff,"%s.ifl",listfname);
	FILE *flog;
	if (fopen_s( &flog, buff, "w" )==0) {
		
		for(int k=0; k<num;k++) 
			fprintf( flog, "%s%d.png\n",bitmapfname,k);

		if (pCmnd==iflPingPong)
		for(int k=num; k>=0;k--) 
			fprintf( flog, "%s%d.png\n",bitmapfname,k);
			
					fclose( flog );
					}
		}
};

extern amk_options mw_opt;



class procedural
{
	int seed; //initial random value;
	float* arr;
	int size_x, size_y;

public:
	float get_arr(int x, int y) 
	{
		if ( (arr==NULL) ||
			 (x>=size_x) ||
			 (x< 0     ) ||
			 (y>=size_y) ||
			 (y< 0     ) ) return 0.0f;


		return arr[x*size_y+y];
	};

	float set_arr(int x, int y, float val) 
	{
		if ( (arr==NULL) ||
			 (x>=size_x) ||
			 (x< 0     ) ||
			 (y>=size_y) ||
			 (y< 0     ) ) return 0.0f;

		       arr[x*size_y+y] = val;
		return arr[x*size_y+y];
	};


	float DistToNearestPoint(float x, float y, float* xcoords, float* ycoords, int numPoints, float& retMindist2, float& retMindist3, float& retMindist4, float& retMindist5)
	{
    float mindist = 3.4E+38;
	float mindist2 = 3.4E+38;
	float mindist3 = 3.4E+38;
	float mindist4 = 3.4E+38;
	float mindist5 = 3.4E+38;
    float dist;

	for (int i=0; i<numPoints; i++) {
        dist = sqrt( pow((xcoords[i]-x),2) + pow( (ycoords[i]-y),2) );
		if (dist < mindist) {mindist5=mindist4;mindist4=mindist3;mindist3=mindist2;mindist2=mindist; mindist= dist;}	
	}

	retMindist2=mindist2;
	retMindist3=mindist3;
	retMindist4=mindist4;
	retMindist5=mindist5;
    return mindist;
	};


	
	enum flipcmd {flipnone, flipx, flipy, flipxy};
	enum addcmd  {addnone,  addadd,  addmult, addxdivy, addydivx, addxpowy, addypowx, addy2addx2};

	void flip(flipcmd fc = flipxy, addcmd cadd=addnone, float fKoeff1 = 1.0f, float fKoeff2 = 1.0f, int p_size_x=0, int p_size_y=0 )
	{
		float* distBuffer = new float[size_x*size_y];

		int ix,iy;
		float fz;

		if ((size_x==0)||(size_y==0)) return;

		if (p_size_x==0) p_size_x=size_x; else if (p_size_x>size_x) p_size_x=  p_size_x - (p_size_x / size_x)*size_x;
		if (p_size_y==0) p_size_y=size_y; else if (p_size_y>size_y) p_size_y=  p_size_y - (p_size_y / size_y)*size_y;

for( int x=0; x<size_x; x++)
 for( int y=0; y<size_y; y++)
 {
	 ix = (p_size_x - x)-1;
	 iy = (p_size_y - y)-1;

	 //2distBuffer[x*size_y+y] = (arr[x*size_y+y]+arr[ix*size_y+y]+arr[x*size_y+iy]+arr[x*size_y+y])/3.0f;
	 
	 switch(fc)	{
	 case flipxy:	 distBuffer[x*size_y+y] = arr[ix*size_y+iy]; break;
	 case flipx:	 distBuffer[x*size_y+y] = arr[ix*size_y+ y]; break;
	 case flipy:	 distBuffer[x*size_y+y] = arr[ x*size_y+iy]; break;
	 case flipnone:
	 default: distBuffer[x*size_y+y] = arr[ x*size_y+y];
						}

	 //Add or not source and flipped?
	 switch(cadd)	{
		case addadd:
		distBuffer[x*size_y+y] = fKoeff1*arr[ x*size_y+y]+fKoeff2*distBuffer[ x*size_y+y];
		break;

		case addmult:
		distBuffer[x*size_y+y] = fKoeff1*arr[ x*size_y+y]*fKoeff2*distBuffer[ x*size_y+y];
		break;

		case addxdivy:
		distBuffer[x*size_y+y] = fKoeff1*arr[ x*size_y+y] / ((fKoeff2*distBuffer[ x*size_y+y]==0.0f) ? 1.0f : fKoeff2*distBuffer[ x*size_y+y]);
		break;

		case addydivx:
		distBuffer[x*size_y+y] = fKoeff2*distBuffer[ x*size_y+y] / ((fKoeff1*arr[ x*size_y+y]==0.0f) ? 1.0f : fKoeff1*arr[ x*size_y+y]);
		break;

		case addxpowy:
		distBuffer[x*size_y+y] = pow( fKoeff1*arr[ x*size_y+y], fKoeff2*distBuffer[ x*size_y+y] );
		break;

		case addypowx:
		distBuffer[x*size_y+y] = pow( fKoeff2*distBuffer[ x*size_y+y], fKoeff1*arr[ x*size_y+y] );
		break;

		case addy2addx2:
		distBuffer[x*size_y+y] = fKoeff2*distBuffer[ x*size_y+y]*distBuffer[ x*size_y+y]+ fKoeff1*arr[ x*size_y+y]*arr[ x*size_y+y];
		break;

		case addnone:
		default:
			distBuffer[x*size_y+y] *= fKoeff1;
					}


 }

 memcpy(arr,distBuffer, sizeof(float)*size_x*size_y);
	 delete [] distBuffer; distBuffer=NULL;
		return;
	};	//void flip(



	void move(int p_size_x,int p_size_y)
	{
		float* distBuffer = new float[size_x*size_y];

		int ix,iy;


for( int x=0; x<size_x; x++)	{
 for( int y=0; y<size_y; y++)
 {
	 ix = (x+p_size_x);
	 iy = (y+p_size_y);

	 if (ix<0) ix = size_x+ix;
	 if (ix>=size_x) ix = ix-size_x;

	 if (iy<0) iy = size_y+iy;
	 if (iy>=size_y) iy = iy-size_y;

	 distBuffer[ix*size_y+iy] = arr[x*size_y+y];
	 //1distBuffer[x*size_y+y] = arr[((x+p_size_x) % size_x)*size_y+((y+p_size_y) % size_y)];
 }

								}



 memcpy(arr,distBuffer, sizeof(float)*size_x*size_y);
	 delete [] distBuffer; distBuffer=NULL;
		return;
	};


	void move_dsslv_blur(int p_size_x,int p_size_y)
	{

		float* distBuffer = new float[size_x*size_y];
		bool bUp = false;
		bool bDown = false;
		bool bLeft = false;
		bool bRight = false;


		if (p_size_y!=0) { if (p_size_y<0) bUp = true; else bDown = true; }
		if (p_size_x!=0) { if (p_size_x<0) bLeft = true; else bRight = true; }

		int ix,iy;


for( int x=0; x<size_x; x++)	{
 for( int y=0; y<size_y; y++)
 {
	 ix = (x+p_size_x);
	 iy = (y+p_size_y);

	 if (ix<0) ix = size_x+ix;
	 if (ix>=size_x) ix = ix-size_x;

	 if (iy<0) iy = size_y+iy;
	 if (iy>=size_y) iy = iy-size_y;

	 distBuffer[ix*size_y+iy] = arr[x*size_y+y];
	 //1distBuffer[x*size_y+y] = arr[((x+p_size_x) % size_x)*size_y+((y+p_size_y) % size_y)];
 }

								}

int iA; float fA;
int iB; float fB;
float fC;float fMin;float fDelta;

/*
for( int x=0; x<size_x; x++) 
for( int k=0; k<p_size_y; k++)	{
	iA = x*size_y+(size_y-k)-1;
//	iB = x*size_y+(size_y-k)-1;
distBuffer[iA] = 1.0f;
								}
*/

if (bLeft) for( int y=0; y<size_y; y++) 
	for( int k=-p_size_x; k>0; k--)
{
	iA = (size_x-k-1)*size_y+y;//levee
	iB = (size_x-k  )*size_y+y;//pravee
	fA = distBuffer[iA];	fB = distBuffer[iB];
	fC = (fB==0.0f) ? 1.0f : fA/fB;
	fMin = min(fA,fB); fDelta = (fA-fB);

distBuffer[iA] = fMin + 3.0f*fDelta/18.0f;
distBuffer[iB] = fMin + 1.0f*fDelta/18.0f;
}


if (bRight) for( int y=0; y<size_y; y++) 
	for( int k=p_size_x; k>0; k--)
{
	iA = (k)*size_y+y;//pravee
	iB = (k-1)*size_y+y;//levee
	fA = distBuffer[iA];	fB = distBuffer[iB];
	fC = (fB==0.0f) ? 1.0f : fA/fB;
	fMin = min(fA,fB); fDelta = (fA-fB);

distBuffer[iA] = fMin + 3.0f*fDelta/18.0f;
distBuffer[iB] = fMin + 1.0f*fDelta/18.0f;
}




if (bDown) for( int x=0; x<size_x; x++) 
	for( int k=p_size_y; k>0; k--)
{
	iA = x*size_y+k;//nige
	iB = x*size_y+k-1;//vishe
	fA = distBuffer[iA];	fB = distBuffer[iB];
	fC = (fB==0.0f) ? 1.0f : fA/fB;
	fMin = min(fA,fB); fDelta = (fA-fB);

distBuffer[iA] = fMin + 3.0f*fDelta/18.0f;
distBuffer[iB] = fMin + 1.0f*fDelta/18.0f;
}



if (bUp) for( int x=0; x<size_x; x++)
	for( int k=-p_size_y; k>0; k--)
{
	iA = x*size_y+(size_y-k)-1;//vishe
	iB = x*size_y+(size_y-k)-1+1;//nige
	fA = distBuffer[iA];	fB = distBuffer[iB];
	fC = (fB==0.0f) ? 0.50f : fA/fB;
	fMin = min(fA,fB); fDelta = (fA-fB);

distBuffer[iA] = fMin + 3.0f*fDelta/18.0f;
distBuffer[iB] = fMin + 1.0f*fDelta/18.0f;
}


 memcpy(arr,distBuffer, sizeof(float)*size_x*size_y);
	 delete [] distBuffer; distBuffer=NULL;
		return;
	};




	void generate_cellular(cellul_tech mytech=ct_diffntl_nrst)
	{

		int numPoints =mw_opt.txtrCellPoints;
float* distBuffer = new float[size_x*size_y];
float*    xcoords = new float[numPoints];
float*    ycoords = new float[numPoints];
float mindist = size_x*size_x+size_y*size_y;//infinity;
float maxdist = 0;

for( int i=0; i<numPoints; i++) {
    xcoords[i] = rand() % size_x;//random number between 0 and width
    ycoords[i] = rand() % size_y;//random number between 0 and height
}


float distance = 0.0f;
float distance2 = 0.0f;
float distance3 = 0.0f;
float distance4 = 0.0f;
float distance5 = 0.0f;

for( int x=0; x<size_x; x++)
 for( int y=0; y<size_y; y++)
 {
	distance =DistToNearestPoint((float) x, (float) y,  xcoords,  ycoords, numPoints, distance2, distance3, distance4, distance5);
	
	
	switch(mytech) {

	case ct_nearest:
	////////////////output2 png
	 distBuffer[x*size_y+y] = distance;
	////////////////output2 png
		break;

	case ct_diffntl_nrst:
	////////////////output3 png
	 distBuffer[x*size_y+y] = distance2-distance;
	////////////////output3 png
	 break;


	

	case ct_diffntl2_nrst:
	////////////////output4 png
    distBuffer[x*size_y+y] = pow((distance3-distance2),14)/pow((distance2-distance),14);//distance to nearest point
	////////////////output4 png
	break;



	




	case ct_diffntl5_nrst:
	////////////////output1 png
	distBuffer[x*size_y+y] = 0.0f;

	for (int i=1; i<4; i++)
	distBuffer[x*size_y+y] += 
	(
	    pow((distance2-distance)*(distance3-distance2)*(distance4-distance3)*(distance5-distance4)/pow((distance5-distance4),4),1.0f/i) +
		pow((distance2-distance)*(distance3-distance2)*(distance4-distance3)*(distance5-distance4)/pow((distance4-distance3),4),1.0f/i) +
		pow((distance2-distance)*(distance3-distance2)*(distance4-distance3)*(distance5-distance4)/pow((distance3-distance2),4),1.0f/i) +
		pow((distance2-distance)*(distance3-distance2)*(distance4-distance3)*(distance5-distance4)/pow((distance2-distance ),4),1.0f/i)
		)/pow(4.0f,1.0f/i);	

	distBuffer[x*size_y+y] = distBuffer[x*size_y+y] / 3.0f;  
	////////////////output1 png
	break;

	case ct_diffntl2_nrst_quad:
	////////////////output5 png glowing edges
	distBuffer[x*size_y+y] =  
		pow((distance3-distance),2)/pow((distance2-distance),2) -
		pow((distance3-distance2),2)/pow((distance2-distance),2);
	////////////////output5 png
	break;



	case ct_folliage:
	////////////////output6 png folliage
		distBuffer[x*size_y+y] =  
		 ((distance4-distance )/3.0f + (distance3-distance )/2.0f+(distance2-distance))/(1.0f/3.0f+1.0f/2.0f+1.0f);
	////////////////output6 png folliage
	break;

	////////////////output6 png white folliage
	//distBuffer[x*size_y+y] =  (distance4+distance3+distance2+distance)/4;
	////////////////output6 png white folliage

	////////////////output7 png peggy
	//distBuffer[x*size_y+y] =  distance/(2.0f) - distance2/(2.0f*3.0f) + distance3/(2.0f*3.0f*4.0f) - distance4/(2.0f*3.0f*4.0f*5.0f) + distance5/(2.0f*3.0f*4.0f*5.0f*6.0f);
	////////////////output7 png

	////////////////output8 black blurred dotts on white
	//float k8 = 1.5f; //(2.5f(lenta metaparticles)..5.0f(kapli)..25.0f(dots)...50.0f(microdot))
	//distBuffer[x*size_y+y] =   min(k8*distance, (distance/(2.0f) + distance2/(2.0f*3.0f) + distance3/(2.0f*3.0f*4.0f) + distance4/(2.0f*3.0f*4.0f*5.0f) + distance5*(2.0f*3.0f*4.0f*5.0f*6.0f)) /(distance/(2.0f*3.0f*4.0f*5.0f*6.0f)*5.0f) );
	////////////////output8 black blurred dotts on white


	////////////////output9 zwezdnaja klaksa
	//float k9 =3.0f; //1-brightest 6.0 darkest
	//distBuffer[x*size_y+y] =  (distance*(2.0f) + distance2*(2.0f*3.0f) + distance3*(2.0f*3.0f*4.0f) + distance4*(2.0f*3.0f*4.0f*5.0f) + distance5*(2.0f*3.0f*4.0f*5.0f*6.0f)) /(distance*(2.0f*3.0f*4.0f*5.0f*6.0f)*k9);
	////////////////output9  zwezdnaja klaksa

	////////////////output10 chemistry framewaork
	//float k10 =12.5f; //5-brightest 25.0 darkest
	//distBuffer[x*size_y+y] =  (distance + distance2 + distance3 + distance4 + distance5) / k10;
	////////////////output10 chemistry framewaork

	////////////////output11 chemistry framewaork 2
	//distBuffer[x*size_y+y] = (distance2-distance)+(distance4-distance3);
	////////////////output11 chemistry framewaork 2

	////////////////output12 soap
	//float k12 =1.0f/0.52f;
	//float ik12 =0.5;
	//distBuffer[x*size_y+y] = pow( ( pow(distance2/(distance2-distance),ik12) + pow(distance3/(distance3-distance2),ik12)+pow(distance4/(distance4-distance3),ik12)+pow(distance5/(distance5-distance4),ik12) ),k12 );
	////////////////output12 soap



	////////////////output13
	//distBuffer[x*size_y+y] = (distance+distance2+distance3+distance4)/abs(distance-distance2+distance3-distance4);
	////////////////output13

	//distBuffer[x*size_y+y] =	(distance2/(distance5-distance4) + distance3/(distance5-distance4) + distance4/(distance5-distance4) +distance5/(distance5-distance4) + distance/(distance5-distance4));
	//distBuffer[x*size_y+y] = (distance5-distance3)*(distance3-distance)*(distance4-distance2)/pow((distance2-distance),3.0f);

	////////////////output
	//	 distBuffer[x*size_y+y] +=  
	//     pow((distance2-0.25f*distance),4)/pow((distance5-2.25f*distance),2) -
   	//	 pow((distance2-2.25f*distance),4)/pow((distance3-2.0f*distance),2)/(4.0f);
	////////////////output



	////////////////output14
	////	float k=32.0f;	float pwr=6.00f; float stp=2.0f;//zrapana powerhnost
	//	float k=32.0f;	float pwr=6.00f; float stp=13.0f;//zrapana powerhnost
//		distBuffer[x*size_y+y] +=  
//		pow(( pow((distance2-2.0f*distance),stp)/pow((distance3-2.0f*distance),stp) +
//			  pow((distance2-2.0f*distance),stp)/pow((distance3-2.0f*distance),stp)/k),pwr);
	////////////////output14


};

    if (distance < mindist) mindist = distance;
    if (distance > maxdist) maxdist = distance;
 }      


for( int x=0; x<size_x; x++)
 for( int y=0; y<size_y; y++)
	set_arr(x,y, (distBuffer[x*size_y+y]-mindist)/(maxdist-mindist));


delete[] distBuffer; distBuffer=NULL;
delete[] xcoords; xcoords=NULL;
delete[] ycoords; ycoords=NULL;

	};

	


	float CloudExpCurve(float v)
	{
		const float CloudCover = 22000.0f;//0 .. 255 give total cloud cover and empty sky respecitvely.

		//0 .. 1.0f  controls how fuzzy or sharp the clouds are.
		//Lower values give sharper, denser clouds, and values closer to 1.0 give fuzzier, thinner clouds.
		//Do not use values any greater than 1.0.
		const float CloudSharpness = 0.99992095f;

      int c = v - CloudCover;
      if (c < 0)  c=0;
 
      float CloudDensity = 65535.0f - (pow(CloudSharpness,c) * 65535.0f) ;

      return CloudDensity;
	}


	void generate_()//Perlin
	{
        //const int PSize = 512; // ����� ������� ���� ������//256
        //float Noise[PSize][PSize];// = new float[PSize][PSize];
		float* distBuffer = new float[size_x*size_y];

				//srand(19150103);//134
		            for (int y = 0; y < size_y; y++)
                    for (int x = 0; x < size_x; x++) distBuffer[x*size_y+y]=0.0f;//1-(rand() % 1000) *0.001f;
					
					int PSize = min(size_x,size_y);

		            int d = PSize >> 1;
            while (true)
            {
                for (int y = 0; y < PSize; y += (d + d))
                {
                    for (int x = 0; x < PSize; x += (d + d))
                    {
					//distBuffer[x*size_y+y]
                        distBuffer[ ( (x+d)%(PSize-1) )*size_y+y ] = (distBuffer[x*size_y+y ] + distBuffer[((x + d + d) % (PSize - 1))*size_y+y]) * 0.5f + (float)d * (0.001f * (float)( rand() % (1000)) - 0.5f);
                        distBuffer[                   x*size_y+( (y + d) % (PSize - 1))] = (distBuffer[x*size_y+y] + distBuffer[x*size_y+( (y + d + d) % (PSize - 1)) ]) * 0.5f + (float)d * (0.001f * (float)( rand() % (1000)) - 0.5f);
                        distBuffer[ ((x + d) % (PSize - 1))*size_y+((y + d) % (PSize - 1))] = 
							(distBuffer[x*size_y+ y] + distBuffer[((x + d + d) % (PSize - 1))*size_y+( (y + d + d) % (PSize - 1))] +
							 distBuffer[x*size_y+( (y + d + d) % (PSize - 1))] +
							 distBuffer[((x + d + d) % (PSize - 1))*size_y+y]) * 0.25f +
							 (float)d * (0.001f * (float)( rand() % (1000)) - 0.5f);
                    }
                }

                if (d <= 1) break;

                d = d >> 1;
            }


 for( int x=0; x<size_x; x++)
 for( int y=0; y<size_y; y++) //set_arr(x,y, distBuffer[x*size_y+y]);
    //set_arr(x,y, CloudExpCurve( distBuffer[x*size_y+y] * 65535.0f )  / 65535.0f );
	set_arr(x,y, distBuffer[x*size_y+y]);


		delete[] distBuffer; distBuffer=NULL;
		return;
	}



	void generate_Cloud()
	{

float* distBuffer = new float[size_x*size_y];

for( int x=0; x<size_x; x++)
 for( int y=0; y<size_y; y++) {
	distBuffer[x*size_y+y] = CloudExpCurve( ((x*y) % 65535) );
 }

 
 for( int x=0; x<size_x; x++)
 for( int y=0; y<size_y; y++)
	set_arr(x,y, distBuffer[x*size_y+y] / 65535.0f );
 
delete [] distBuffer; distBuffer=NULL;

 generate_cellular();
 for( int x=0; x<size_x; x++)
 for( int y=0; y<size_y; y++)
  set_arr(x,y, CloudExpCurve( (get_arr(x,y) * 65535.0f ))  / 65535.0f );

	}



	void simple_blur_(int depth)
	{
// float px, float py
//float finx = 0;
//float ffrx = modf(px,&finx);

//float finy = 0;
//float ffry = modf(py,&finy);
		float v,w;
		procedural* temp = new procedural(size_x,size_y);


		 for( int x=0; x<size_x; x++)
		 for( int y=0; y<size_y; y++)
			{

		 v=0.0f;//get_arr(x,y);//
		 w=0.0f;
		 for( int jx=0; jx<depth; jx++){
			 
			 for( int jy=0; jy<depth; jy++) {
		       v += get_arr( (x-jx)>0 ? (x-jx) :             x , (y-jy)>0 ? (y-jy) : y );
		  //   v += get_arr( (x-jx)>0 ? (x-jx) : (x-jx)+size_x , (y-jy)>0 ? (y-jy) : (y-jy)+size_y);
											}
			 w += v / (float)depth; v=0.0f;
		 }
			//set_arr(x,y, v /(depth*depth-1) );
			//temp->set_arr(x,y, v /(float)(depth*depth-1.0f) );
			temp->set_arr(x,y, w / ((float)depth) );
			}

		 		 for( int y=0; y<size_y; y++)
				//for( int x=0; x<size_x; x++) set_arr(x,y,abs(temp->get_arr(x,y))>256.0f ? abs(256.0f / temp->get_arr(x,y)) : (temp->get_arr(x,y) ) );
				// for( int x=0; x<size_x; x++) set_arr(x,y,(temp->get_arr(x,y))>1.0f ? 1.0f / temp->get_arr(x,y) : temp->get_arr(x,y) );

				//for( int x=0; x<size_x; x++) set_arr(x,y,temp->get_arr(x,y)>1.0f ? temp->get_arr(x,y) - (float)((int)temp->get_arr(x,y)) : temp->get_arr(x,y));
				
				for( int x=0; x<size_x; x++) {
					
					set_arr(x,y,min(256.0f,mabs(temp->get_arr(x,y))) );

					//2// w = abs(temp->get_arr(x,y));
					//2// set_arr(x,y, (w>1.0f) ? 1.0f / w : w );
				}
				
				 delete temp;
	}

	/* bad idea from mikle flamemaker
	void generate_()
	{

float* distBuffer = new float[size_x*size_y];

int ia1,ia2,ia3,ia4,ia5,ia6,ia7;;

float temp;
float x0, y0;
float x1, y1;
float x2, y2;

float i;
float Sz =128;
float Kf;


int ix, iy;

for( int x=0; x<size_x; x++)
 for( int y=0; y<size_y; y++)
	distBuffer[x*size_y+y] = 0.0f;


  for(i = 7; i>= 1.0f; i-= 1.0f)
  {
    Kf = pow(1.4f, i);
	for( y0 = 0; y0<= size_y; y0+= Sz)
	for( x0 = 0; x0<= size_x; x0+= Sz)
	{

        x1 = x0 + Sz / 2.0f;
        y1 = y0 + Sz / 2.0f;

        //x2 = (x0 + Sz) - 127.0f*floorf((x0 + Sz) / 127.0f);
        //y2 = (y0 + Sz) - 127.0f*floorf((y0 + Sz) / 127.0f);
		x2 = modff((x0 + Sz), &temp);
		y2 = modff((y0 + Sz), &temp);

		ia1 = x1*size_y + y0;
		ia2 = x0*size_y + y1;
		ia3 = x1*size_y + y1;
		ia4 = x0*size_y + y0;
		ia5 = x2*size_y + y0;
		ia6 = x0*size_y + y2;
		ia7 = x2*size_y + y2;

        distBuffer[ia1] = Kf * ((rand()/ (RAND_MAX + 1)) - 0.5f) + (0 + distBuffer[ia4] + distBuffer[ia5]) * 0.5f;
        distBuffer[ia2] = Kf * ((rand()/ (RAND_MAX + 1)) - 0.5f) + (0 + distBuffer[ia4] + distBuffer[ia6]) * 0.5f;
        distBuffer[ia3] = Kf * ((rand()/ (RAND_MAX + 1)) - 0.5f) + (0 + distBuffer[ia4] + distBuffer[ia6] + distBuffer[ia5] + distBuffer[ia7]) * 0.25f;
	}

    Sz = Sz / 2;
  }

  Kf = 0;

	for( y0 = 0; y0<= size_y; y0+= Sz)
	for( x0 = 0; x0<= size_x; x0+= Sz) {
		ia4 = x0*size_y + y0;
      if (Kf < abs(distBuffer[ia4]))  Kf = abs(distBuffer[ia4]);
	}

  Kf = size_x / Kf;


	for( y0 = 0; y0<= size_x; y0+= Sz)
	for( x0 = 0; x0<= size_x; x0+= Sz){
		ia4 = x0*size_y + y0;
		distBuffer[ia4] = (distBuffer[ia4]*Kf + size_x)*0x10000;//21.0f;//0x10101;			
	}

	//float t2,alf;
for( int x=0; x<size_x; x++)
 for( int y=0; y<size_y; y++) {
	 / *
	 t2=hypotf((float)x,(float)y);
	 alf=atanf( (y==0) ? 999999.9f : (float)x / (float)y );

	 set_arr(
		 110.0f+t2*cos(alf+3.14f/4.0f),
		 203.0f+t2*sin(alf+3.14f/4.0f),
		 modff( distBuffer[x*size_y+y]/256.0f, &temp) );* /
	set_arr(0.0f+x,0.0f+y, modff( distBuffer[x*size_y+y]/size_x, &temp) );
 }


delete[] distBuffer; distBuffer=NULL;
return;
	}
 */
/* bad idea from mikle flamemaker*/


	procedural(int psize_x, int psize_y);
	~procedural(void);
};


typedef float amk_rgb[3];
amk_rgb* material_witrag(int psize_x, int psize_y);
amk_rgb* material_crstll_marble_sky(int psize_x, int psize_y);

class cl_mtrl_witrag {
public:
	 cl_mtrl_witrag(int psize_x, int psize_y);
	~cl_mtrl_witrag(void);

	amk_rgb* arr;
	int size_x, size_y;

	procedural* txtr;
	procedural* txtr1;
	procedural* txtr2;
	procedural* txtrCellul;

	amk_rgb* material_witrag(bool pbLegacy=false)
{
	int psize_x = size_x;
	int psize_y = size_y;
	
	int PSize = min(psize_x, psize_y);

	if (!pbLegacy) {

		if (txtr) delete txtr; txtr=NULL; 
		txtr = new procedural(PSize+mw_opt.deltaTxtrSize,PSize+mw_opt.deltaTxtrSize);
		txtr->generate_();
		if (mw_opt.beforeBlur0>0) txtr->simple_blur_(mw_opt.beforeBlur0);//10
		for(int i=0; i<mw_opt.itrBlur0; i++) txtr->simple_blur_(mw_opt.amountBlur0);//8


		if (txtr1) delete txtr1; txtr1=NULL; 
		txtr1 = new procedural(PSize+mw_opt.deltaTxtr1Size,PSize+mw_opt.deltaTxtr1Size);
		txtr1->generate_();
		if (mw_opt.beforeBlur1>0)txtr1->simple_blur_(mw_opt.beforeBlur1);//10
		for(int i=0; i<mw_opt.itrBlur1; i++) txtr1->simple_blur_(mw_opt.amountBlur1);//8

		if (txtr2) delete txtr2; txtr2=NULL; 
		txtr2 = new procedural(PSize+mw_opt.deltaTxtr2Size,PSize+mw_opt.deltaTxtr2Size);
		txtr2->generate_();
		if (mw_opt.beforeBlur2>0) txtr2->simple_blur_(mw_opt.beforeBlur2);//10
		for(int i=0; i<mw_opt.itrBlur2; i++) txtr2->simple_blur_(mw_opt.amountBlur2);//8

		if (txtrCellul) delete txtrCellul; txtrCellul=NULL; 
		txtrCellul = new procedural(PSize,PSize);
		txtrCellul->generate_cellular(mw_opt.txtrCellTechniq);		
		if (mw_opt.beforeBlur>0) txtrCellul->simple_blur_(mw_opt.beforeBlur);		
		for(int i=0; i<mw_opt.itrBlur; i++) txtrCellul->simple_blur_(mw_opt.amountBlur);//8
				} else {
					//here code where txtr,txtr1,txtr2,txtrCellul - Moved, Scaled and Rotated
						}

		//where 0-txtr, 1-txtr1, 2-txtr2, txtrCellul;
		float ttxtr[4];

                for (int y = 0; y < PSize; y += 1)
                    for (int x = 0; x < PSize; x += 1) {

						ttxtr[0] = txtr  ->get_arr(x,y);
						ttxtr[1] = txtr1 ->get_arr(x,y);
						ttxtr[2] = txtr2 ->get_arr(x,y);
						ttxtr[3] = txtrCellul ->get_arr(x,y);
		
						
						switch(mw_opt.mtrl1MatTecniq) {

						case mtrl_1_tech::mtrl_1_witrage:
arr[x*psize_y+y][0] =
//arr[3*(x*psize_y+y)+0] =
//1.0f-
mw_opt.shftMainCh0+mw_opt.negMainCh0*pow(
					(txtrCellul ->get_arr(x,y)*
					0.03f*(txtr2 ->get_arr(x,y)*txtr ->get_arr(x,y)+0.75f*txtr1 ->get_arr(x,y)+0.33f*txtr2 ->get_arr(x,y))/2.0f
					)/2.00f,mw_opt.pwrMainCh0*mw_opt.invMainCh0);

arr[x*psize_y+y][1] =
//arr[3*(x*psize_y+y)+1] =
mw_opt.shftMainCh1+mw_opt.negMainCh1*pow(
					(txtrCellul ->get_arr(x,y)*
					0.010f*(txtr2 ->get_arr(x,y)+0.75f*txtr ->get_arr(x,y)*txtr1 ->get_arr(x,y)+0.33f*txtr2 ->get_arr(x,y))/2.0f
					)/2.00f,mw_opt.pwrMainCh1*mw_opt.invMainCh1);

arr[x*psize_y+y][2] =
//arr[3*(x*psize_y+y)+2] =
mw_opt.shftMainCh2+mw_opt.negMainCh2*pow(
					(txtrCellul ->get_arr(x,y)*
					0.030f*(txtr2 ->get_arr(x,y)+0.75f*txtr1 ->get_arr(x,y)+0.33f*txtr ->get_arr(x,y)*txtr2 ->get_arr(x,y))/2.0f
					)/2.00f,mw_opt.pwrMainCh2*mw_opt.invMainCh2);
								break;




						case mtrl_1_tech::mtrl_1_crstll_marble_sky:

arr[x*psize_y+y][0] =

//0.06f/
//1.0f/ -
//2//1.0f -
mw_opt.shftMainCh0+mw_opt.negMainCh0*
					pow(
					 //( txtrCellul ->get_arr(x,y) < 0.1f ? (rand() % 10)/10.0f : txtrCellul ->get_arr(x,y) ) *
					 // txtrCellul ->get_arr(x,y)*
					 ttxtr[3]*
					(ttxtr[mw_opt.mtrxMainCh[0][0][0]]*ttxtr[mw_opt.mtrxMainCh[0][0][1]]+
					 ttxtr[mw_opt.mtrxMainCh[1][0][0]]*ttxtr[mw_opt.mtrxMainCh[1][0][1]]+
					 ttxtr[mw_opt.mtrxMainCh[2][0][0]]*ttxtr[mw_opt.mtrxMainCh[2][0][1]]
					// txtr2 ->get_arr(x,y)*txtr ->get_arr(x,y)+
					// txtr1 ->get_arr(x,y)*txtr1 ->get_arr(x,y)+
					// txtr2 ->get_arr(x,y)*txtr2 ->get_arr(x,y)
					)/3.0f
					,mw_opt.pwrMainCh0*mw_opt.invMainCh0);////, 0.1f);

arr[x*psize_y+y][1] =
//0.03f/
//2//1.0f -
mw_opt.shftMainCh1+mw_opt.negMainCh1*
					pow(
					 //( txtrCellul ->get_arr(x,y) < 0.1f ? (rand() % 10)/10.0f : txtrCellul ->get_arr(x,y) ) *
					// txtrCellul ->get_arr(x,y)*
					ttxtr[3]*
					(ttxtr[mw_opt.mtrxMainCh[0][1][0]]*ttxtr[mw_opt.mtrxMainCh[0][1][1]]+
					 ttxtr[mw_opt.mtrxMainCh[1][1][0]]*ttxtr[mw_opt.mtrxMainCh[1][1][1]]+
					 ttxtr[mw_opt.mtrxMainCh[2][1][0]]*ttxtr[mw_opt.mtrxMainCh[2][1][1]]

					//txtr2 ->get_arr(x,y)*txtr2 ->get_arr(x,y)+
					// txtr ->get_arr(x,y)*txtr1 ->get_arr(x,y)+
					//txtr2 ->get_arr(x,y)*txtr2 ->get_arr(x,y)
					)/3.0f
					,mw_opt.pwrMainCh1*mw_opt.invMainCh1);////, 0.0001f);

arr[x*psize_y+y][2] =
//0.01f/
mw_opt.shftMainCh2+mw_opt.negMainCh2*
					pow(
					// ( txtrCellul ->get_arr(x,y) < 0.1f ? (rand() % 10)/10.0f : txtrCellul ->get_arr(x,y) ) *
					// txtrCellul ->get_arr(x,y)*
					ttxtr[3]*
					(ttxtr[mw_opt.mtrxMainCh[0][2][0]]*ttxtr[mw_opt.mtrxMainCh[0][2][1]]+
					 ttxtr[mw_opt.mtrxMainCh[1][2][0]]*ttxtr[mw_opt.mtrxMainCh[1][2][1]]+
					 ttxtr[mw_opt.mtrxMainCh[2][2][0]]*ttxtr[mw_opt.mtrxMainCh[2][2][1]]

					//txtr2 ->get_arr(x,y)*txtr2 ->get_arr(x,y)+
					//txtr1 ->get_arr(x,y)*txtr1 ->get_arr(x,y)+
					//txtr ->get_arr(x,y)*txtr2 ->get_arr(x,y)
					)/3.0f
					,mw_opt.pwrMainCh2*mw_opt.invMainCh2);////, 0.0001f);
break;

					}
					}


return arr;
};


};

extern cl_mtrl_witrag* mt1;

/*
typedef struct tagResmakeIni
{

} amk_ResmakeIni;

void resmake()
{
};
*/